/**
 *******************************************************************************
 *
 * @file rc_keycode.c
 *
 * @brief HID remote keycode definitions
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */

#pragma once

#ifdef CFG_FLEX_VKEY_MAP
#define __ATM_VKEY_MAP_CONST
#else
#define __ATM_VKEY_MAP_CONST const
#endif

#define CSM_USAGE 0x000C
#define KBD_USAGE 0x0007

#define CSM(x) ((CSM_USAGE << 16) | x)
#define KBD(x) ((KBD_USAGE << 16) | x)

#define BT_VOLU CSM(0xE9)
#define BT_VOLD CSM(0xEA)
#define BT_PLAY CSM(0xCD)
#define BT_BACK CSM(0x224)
#define BT_BW CSM(0xB6)
#define BT_FW CSM(0xB5)
#define BT_POWER CSM(0x30)
#define BT_HOME CSM(0x223)
#define BT_MENU KBD(0x76)
#define BT_MIC CSM(0x221)
#define BT_OK CSM(0x41)
#define BT_UP CSM(0x42)
#define BT_DOWN CSM(0x43)
#define BT_RIGHT CSM(0x45)
#define BT_LEFT CSM(0x44)

#ifdef CFG_ATVRC_MMI
#define BT_BKMK CSM(0x22A)
#define BT_ALAP CSM(0x1A2)
#define BT_PROF CSM(0x229)
#define BT_ASST CSM(0x221)
#define BT_SETT CSM(0x96)
#define BT_DASHB CSM(0x9F)
#define BT_CENTER CSM(0x41)
#define BT_GUIDE CSM(0x8D)
#define BT_LIVE CSM(0x89)
#define BT_INPUT CSM(0x1BB)
#define BT_MUTE CSM(0xE2)
#define BT_YOUTUBE CSM(0x77)
#define BT_NETFLIX CSM(0x78)
#define BT_APP03 CSM(0x79)
#define BT_APP04 CSM(0x7A)
#define BT_CNLU CSM(0x9C)
#define BT_CNLD CSM(0x9D)
#define BT_FASTR CSM(0xB4)
#define BT_RECORD CSM(0xCE)
#define BT_PLAY CSM(0xCD)
#define BT_FASTF CSM(0xB3)
#define BT_RED CSM(0x69)
#define BT_GREEN CSM(0x6A)
#define BT_YELLOW CSM(0x6C)
#define BT_BLUE CSM(0x6B)
#define BT_NUM1 KBD(0x1E)
#define BT_NUM2 KBD(0x1F)
#define BT_NUM3 KBD(0x20)
#define BT_NUM4 KBD(0x21)
#define BT_NUM5 KBD(0x22)
#define BT_NUM6 KBD(0x23)
#define BT_NUM7 KBD(0x24)
#define BT_NUM8 KBD(0x25)
#define BT_NUM9 KBD(0x26)
#define BT_NUM0 KBD(0x27)
#define BT_INFO CSM(0x1BD)
#define BT_SUBT CSM(0x61)
#define BT_TEXT CSM(0x185)
#endif

#ifdef CFG_ATVRC_UNI_IR
#define ATV_VOLU 0x18
#define ATV_VOLD 0x19
#define ATV_MUTE 0xA4
#define ATV_POWER 0x1A
#define ATV_INPUT 0xB2
#endif

#ifdef CFG_RC_IR
#ifdef CFG_ATVRC_MMI
#define IR_ADDR 0x88
#define IR_POWER 0x21
#define IR_INPUT 0x60
#define IR_MUTE 0x25
#define IR_VOLU 0x23
#define IR_VOLD 0x24
#define IR_YOUTUBE 0x63
#define IR_NETFLIX 0x64
#define IR_APP03 0x67
#define IR_APP04 0x68
#define IR_CNLU 0x33
#define IR_CNLD 0x34
#define IR_BKMK 0x74
#define IR_ALAP 0x57
#define IR_PROF 0x59
#define IR_ASST 0x46
#define IR_SETT 0x0F
#define IR_DASHB 0x10
#define IR_UP 0x15
#define IR_DOWN 0x16
#define IR_LEFT 0x17
#define IR_RIGHT 0x18
#define IR_CENTER 0x19
#define IR_BACK 0x48
#define IR_HOME 0x47
#define IR_GUIDE 0x32
#define IR_LIVE 0x61
#define IR_FASTR 0x51
#define IR_RECORD 0x54
#define IR_PLAY 0x52
#define IR_FASTF 0x53
#define IR_RED 0x4B
#define IR_GREEN 0x4A
#define IR_YELLOW 0x49
#define IR_BLUE 0x4C
#define IR_NUM1 0x01
#define IR_NUM2 0x02
#define IR_NUM3 0x03
#define IR_NUM4 0x04
#define IR_NUM5 0x05
#define IR_NUM6 0x06
#define IR_NUM7 0x07
#define IR_NUM8 0x08
#define IR_NUM9 0x09
#define IR_NUM0 0x0A
#define IR_INFO 0x29
#define IR_SUBT 0x58
#define IR_TEXT 0x75
#define IR_BUGR 0x96
#else
#define IR_ADDR 0x80
#define IR_POWER 0x46
#define IR_UP 0x52
#define IR_LEFT 0x06
#define IR_OK 0x0F
#define IR_RIGHT 0x1A
#define IR_MENU 0x07
#define IR_DOWN 0x13
#define IR_BACK 0x1B
#define IR_VOLD 0x15
#define IR_HOME 0x17
#define IR_VOLU 0x16
#endif
#endif // CFG_RC_IR