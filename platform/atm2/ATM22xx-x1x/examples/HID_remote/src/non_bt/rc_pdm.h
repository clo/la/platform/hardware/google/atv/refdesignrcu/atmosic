/**
 *******************************************************************************
 *
 * @file rc_pdm.h
 *
 * @brief PDM application part
 *
 * Copyright (C) Atmosic 2020-2021
 *
 *******************************************************************************
 */
#pragma once

/// PDM operation type
typedef enum {
    /// Idle. (Not operation)
    RC_PDM_IDLE,
    /// Normal mode. (Sent to peer.)
    RC_PDM_NORMAL,
    /// Local test mode. (Drop data.)
    RC_PDM_LOCAL_TEST_WO_DELAY,
    /// Local test mode. (Interfere by delay and drop data.)
    RC_PDM_LOCAL_TEST_WITH_DELAY,
    /// Local test mode. (No buffer.)
    RC_PDM_LOCAL_TEST_NO_BUF,
    RC_PDM_LOCAL_TEST_MAX,
} rc_pdm_op_t;

/**
 *******************************************************************************
 * @brief PDM initialization.
 *******************************************************************************
 */
void rc_pdm_init(void);

/**
 *******************************************************************************
 * @brief Power on external PDM audio component.
 *******************************************************************************
 */
void rc_pdm_device_pwr_on(void);

/**
 *******************************************************************************
 * @brief Start PDM audio collection.
 * @param op[in] PDM operation type.
 * @param is8K[in] True if 8Khz. Otherwise is 16Khz.
 *******************************************************************************
 */
void rc_pdm_start(rc_pdm_op_t op, bool is8K);

/**
 *******************************************************************************
 * @brief Stop current PDM audio collection.
 *******************************************************************************
 */
void rc_pdm_stop(void);

/**
 *******************************************************************************
 * @brief PDM gain value adjust.
 * @param[in] add True for increasing. Otherwise is decreasing.
 *******************************************************************************
 */
void rc_pdm_gain_adjust(bool add);

/**
 *******************************************************************************
 * @brief Resume paused PDM audio collection.
 *******************************************************************************
 */
void rc_pdm_resume(void);

/**
 *******************************************************************************
 * @brief Pause current PDM audio collection.
 *******************************************************************************
 */
void rc_pdm_pause(void);

/**
 *******************************************************************************
 * @brief Get ADPCM index and predicted sample.
 * @param[out] idx Index
 * @return Predicted sample.
 *******************************************************************************
 */
uint16_t rc_pdm_get_adpcm_index(uint8_t *idx);

/**
 *******************************************************************************
 * @brief Check if PDM paused.
 *******************************************************************************
 */
bool rc_pdm_is_paused(void);
