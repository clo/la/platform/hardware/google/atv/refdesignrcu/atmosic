/**
 *******************************************************************************
 *
 * @file rc_ir.h
 *
 * @brief IR application part
 *
 * Copyright (C) Atmosic 2020-2021
 *
 *******************************************************************************
 */
#pragma once

/**
 * @brief IR initialization.
 */
void rc_ir_init(void);

/**
 *******************************************************************************
 * @brief Send IR code.
 * @param[in] address NEC address
 * @param[in] cmd NEC command.
 * @param[in] delay_ds Delay time to send.
 *******************************************************************************
 */
void rc_ir_send(uint32_t address, uint32_t cmd, uint32_t delay_cs);

/**
 * @brief Stop repeat
 */
void rc_ir_repeat_end(void);

#ifdef CFG_ATVRC_UNI_IR
/**
 *******************************************************************************
 * @brief Check universal IR key
 * @param[in] atv_keycode Android TV keycode
 *******************************************************************************
 */
bool rc_ir_check_uni_key(uint16_t atv_keycode);

/**
 *******************************************************************************
 * @brief Send Universal IR code data
 * @param[in] key Universal IR key id
 * @param[in] code Pointer of IR code data
 * @param[in] size IR code data size
 *******************************************************************************
 */
void rc_ir_send_uni_code(uint16_t key, uint8_t const *code, uint16_t size);

/**
 *******************************************************************************
 * @brief Clear all Universal IR code data
 *******************************************************************************
 */
void rc_ir_clear_uni_codes(void);
#endif
