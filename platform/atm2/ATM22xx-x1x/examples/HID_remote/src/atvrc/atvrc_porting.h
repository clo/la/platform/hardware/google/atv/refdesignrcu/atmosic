/**
 *******************************************************************************
 *
 * @file atvrc_porting.h
 *
 * @brief Android TV Remote Control porting to use refDesignRcu implementations
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */
#pragma once

#include "arch.h"

typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;
typedef int16_t s16;
typedef int8_t s8;

#define APPLICATION_DONGLE 0
#define SYSTEM_TIMER_TICK_1US 16
#define SYSTEM_TIMER_TICK_1S 16000000

void atm_array_printf(uint8_t const *data, uint16_t size);

#define array_printf atm_array_printf