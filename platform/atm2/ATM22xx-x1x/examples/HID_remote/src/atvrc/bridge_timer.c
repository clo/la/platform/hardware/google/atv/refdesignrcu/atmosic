/**
 *******************************************************************************
 *
 * @file bridge_timer.c
 *
 * @brief Bridge timer
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */

#include "arch.h"
#include "atvrc_porting.h"
#include "sw_timer.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-declaration"
#include "vendor/common/blt_soft_timer.h"
#pragma GCC diagnostic pop

#define NUM_BLT_TIMER 3

#undef TIMER_DEBUG

#ifdef TIMER_DEBUG
#define TIMER_TRACE DEBUG_TRACE
#else
#define TIMER_TRACE(fmt, ...) DEBUG_TRACE_COND(0, fmt, ##__VA_ARGS__)
#endif

typedef struct blt_sw_timer_t {
    sw_timer_id_t tid;
    blt_timer_callback_t func;
} blt_sw_timer_t;

static blt_sw_timer_t blt_sw_timer[NUM_BLT_TIMER];

static void blt_sw_timer_timeout(sw_timer_id_t idx, void const *ctx)
{
    TIMER_TRACE("%s idx: %d", __func__, idx);
    blt_timer_callback_t func = (blt_timer_callback_t)(uintptr_t)ctx;
    if (func) {
	func();
    }
}

static uint8_t get_available_blt_sw_timer(void)
{
    for (uint8_t i = 0; i < NUM_BLT_TIMER; i++) {
	if (!blt_sw_timer[i].tid) {
	    return i;
	}
    }
    DEBUG_TRACE("No available blt_sw_timer, increase NUM_BLT_TIMER");
    ASSERT_ERR(0);
    return 0;
}

int blt_soft_timer_add(blt_timer_callback_t func, u32 interval_us)
{
    uint8_t idx = get_available_blt_sw_timer();

    blt_sw_timer[idx].tid = sw_timer_alloc(blt_sw_timer_timeout, func);
    TIMER_TRACE("%s id: %d tid: %d inv: %lu func: %p", __func__, idx,
	blt_sw_timer[idx].tid, interval_us, func);
    blt_sw_timer[idx].func = func;
#define USEC_TO_CSEC 10000
    sw_timer_set(blt_sw_timer[idx].tid, interval_us / USEC_TO_CSEC);
    return idx;
}

int blt_soft_timer_delete(blt_timer_callback_t func)
{
    uint8_t cnt = 0;
    for (uint8_t i = 0; i < NUM_BLT_TIMER; i++) {
	if (blt_sw_timer[i].func == func) {
	    TIMER_TRACE("%s id: %d tid: %d cb: %p", __func__, i,
		blt_sw_timer[i].tid, func);
	    sw_timer_clear(blt_sw_timer[i].tid);
	    sw_timer_free(blt_sw_timer[i].tid);
	    blt_sw_timer[i].tid = 0;
	    blt_sw_timer[i].func = NULL;
	    cnt++;
	}
    }
    TIMER_TRACE("found and deleted %d timer", cnt);
    return cnt;
}
