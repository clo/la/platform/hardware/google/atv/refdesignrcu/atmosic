/**
 *******************************************************************************
 *
 * @file refdesignrcu.h
 *
 * @brief Required header to build refDesignRcu in Atmosic SDK
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */
#pragma once

#include "atvrc_porting.h"

/// Warning supressions for refDesignRcu codes
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wmissing-prototypes"
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#pragma GCC diagnostic ignored "-Wcast-qual"
#pragma GCC diagnostic ignored "-Wunused-const-variable="
#pragma GCC diagnostic ignored "-Wint-conversion"
#pragma GCC diagnostic ignored "-Wformat="
#pragma GCC diagnostic ignored "-Wold-style-declaration"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"
#pragma GCC diagnostic ignored "-Wold-style-definition"
#pragma GCC diagnostic ignored "-Wswitch-enum"

/// Required definitions for refDesignRcu
#define SUCCESS 0x00
#define __PROJECT_8258_BLE_REMOTE__ 0
#define __PROJECT_8258_DRIVER_TEST__ 0
#define __PROJECT_8278_BLE_REMOTE__ 1
#define __PROJECT_8278_DRIVER_TEST__ 0
#define LL_FEATURE_ENABLE_LL_PRIVACY 0
#define LL_FEATURE_ENABLE_LE_DATA_LENGTH_EXTENSION 0
#define LL_MASTER_MULTI_CONNECTION 0
#define _attribute_data_retention_
#define _attribute_aligned_(s) __attribute__((aligned(s)))
#define own_addr_type_t u8

#define U16_HI(a) (((a) >> 8) & 0xFF)
#define U16_LO(a) ((a) & 0xFF)
#define BIT(n) ( 1<<(n) )

#if !PLF_DEBUG
#define printf(fmt, ...) do {} while(0)
#endif