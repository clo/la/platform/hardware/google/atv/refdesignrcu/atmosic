/**
 *******************************************************************************
 *
 * @file atvrc_custom.c
 *
 * @brief Android TV remote customization data management
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */

#include "arch.h"
#include "atvrc_custom.h"
#include "gap.h"
#include "nvds.h"
#include "rc_gap.h"
#include "rc_keycode.h"
#include "timer.h"

#define ATVRC_CUSTOM_WAKE_PACKET_SIZE_MAX (31 - 4)
#define ATVRC_AUTHENTICATION_KEY_SIZE 16
#define ATVRC_ENCRYPTION_KEY_SIZE 16
#define ATVRC_ECDSA_PUBLIC_KEY_SIZE 64

typedef struct
{
    uint8_t dev_type;
    uint8_t pnp_id[ATVRC_PNP_ID_LEN];
    uint8_t dev_name_len;
    uint8_t dev_name[ATVRC_DEV_NAME_LEN_MAX];
    uint8_t dev_ui_layout;
    uint16_t wake_key_1;
    uint16_t wake_key_2;
    uint8_t wake_pkt_len;
    uint8_t key_id_oft;
    uint8_t key_cnt_oft;
    uint8_t peer_addr_oft;
    uint8_t wake_pkt_data[ATVRC_CUSTOM_WAKE_PACKET_SIZE_MAX];
    uint16_t period_wake_interval;
    uint8_t period_wake;
    uint8_t rpa_switch;
    uint8_t disable_ble;
    uint8_t wake_pkt_send_mode;
    uint8_t cache_pwr;
    uint8_t auth_key[ATVRC_AUTHENTICATION_KEY_SIZE];
    uint8_t encrypt_key[ATVRC_ENCRYPTION_KEY_SIZE];
    uint8_t ecdsa_pub_key[ATVRC_ECDSA_PUBLIC_KEY_SIZE];
} __PACKED atvrc_custom_data_t;

static atvrc_custom_data_t custom_data;
static uint8_t fw_rev[ATVRC_FW_REV_STR_LEN] = {'V', '1', '.', '0', '0'};

#define WAKE_KEY_NUM 12
static uint32_t wake_key_bt_id[WAKE_KEY_NUM] = {
    BT_ASST,
    BT_HOME,
    BT_POWER,
    BT_INPUT,
    BT_YOUTUBE,
    BT_NETFLIX,
    BT_APP03,
    BT_APP04,
    BT_BLUE,
    BT_GREEN,
    BT_RED,
    BT_YELLOW,
};

#define NVDS_TAG_ATVRC_CUSTOM_DATA 0xC1
void atvrc_custom_init(void)
{
    nvds_tag_len_t len = sizeof(atvrc_custom_data_t);
    nvds_get(NVDS_TAG_ATVRC_CUSTOM_DATA, &len, (uint8_t*)&custom_data);
#define FW_REV_MINOR_TEN_DIGIT 3
#define FW_REV_MINOR_UNIT_DIGIT 4
#define ASCII_BASE 0x30
    fw_rev[FW_REV_MINOR_TEN_DIGIT] = custom_data.dev_type + ASCII_BASE;
    fw_rev[FW_REV_MINOR_UNIT_DIGIT] = custom_data.dev_ui_layout + ASCII_BASE;
    DEBUG_TRACE("Device type: %d", custom_data.dev_type);
    DEBUG_TRACE("Vendor source: %d", custom_data.pnp_id[0]);
    DEBUG_TRACE("VID: %02X%02X", custom_data.pnp_id[2], custom_data.pnp_id[1]);
    DEBUG_TRACE("PID: %02X%02X", custom_data.pnp_id[4], custom_data.pnp_id[3]);
    DEBUG_TRACE("PVer: %02X%02X", custom_data.pnp_id[6], custom_data.pnp_id[5]);
    DEBUG_TRACE("Device name length: %d", custom_data.dev_name_len);
    DEBUG_TRACE("Device name :");
#if PLF_DEBUG
    for (uint8_t i = 0; i < ATVRC_DEV_NAME_LEN_MAX; i++) {
	printf("%c", custom_data.dev_name[i]);
    }
    printf("\n");
#endif
    DEBUG_TRACE("UI layout: %d", custom_data.dev_ui_layout);
    DEBUG_TRACE("Wake key 1: %04X", custom_data.wake_key_1);
    DEBUG_TRACE("Wake key 2: %04X", custom_data.wake_key_2);
    DEBUG_TRACE("Wake packet len: %d", custom_data.wake_pkt_len);
    DEBUG_TRACE("Key ID offset: %d", custom_data.key_id_oft);
    DEBUG_TRACE("Key press count offset: %d", custom_data.key_cnt_oft);
    DEBUG_TRACE("Peer Addr. offset: %d", custom_data.peer_addr_oft);
    DEBUG_TRACE("Customized wakeup packet :");
#if PLF_DEBUG
    for (uint8_t i = 0; i < ATVRC_CUSTOM_WAKE_PACKET_SIZE_MAX; i++) {
	printf("%02X ", custom_data.wake_pkt_data[i]);
    }
    printf("\n");
#endif
#define PERIOD_WAKE_INTERVEL_MINIMAL_MIN 5
#define PERIOD_WAKE_INTERVEL_MAXIMAL_MIN 0x5A0
#define PERIOD_WAKE_INTERVEL_DEFAULT_MIN 30
    if ((custom_data.period_wake_interval < PERIOD_WAKE_INTERVEL_MINIMAL_MIN) ||
	(custom_data.period_wake_interval > PERIOD_WAKE_INTERVEL_MAXIMAL_MIN)) {
	custom_data.period_wake_interval = PERIOD_WAKE_INTERVEL_DEFAULT_MIN;
    }
    DEBUG_TRACE("Periodic wakeup interval: %d", custom_data.period_wake_interval);
    DEBUG_TRACE("Periodic wakeup: %02X", custom_data.period_wake);
    DEBUG_TRACE("RCU RPA switch: %02X", custom_data.rpa_switch);
    DEBUG_TRACE("Disable BLE: %02X", custom_data.disable_ble);
    DEBUG_TRACE("Wakeup sending mode: %02X", custom_data.wake_pkt_send_mode);
    DEBUG_TRACE("Catch power switch: %02X", custom_data.cache_pwr);
}

uint8_t atvrc_custom_get_device_type(void)
{
    return custom_data.dev_type;
}

uint8_t *atvrc_custom_get_pnp(void)
{
    return custom_data.pnp_id;
}

uint8_t atvrc_custom_get_dev_name(nvds_tag_len_t *len, uint8_t *name)
{
    DEBUG_TRACE("%s: %s", __func__, custom_data.dev_name);
    if (!custom_data.dev_name_len) {
	DEBUG_TRACE("%s: FAIL", __func__);
	return NVDS_FAIL;
    }

    if (custom_data.dev_name_len > *len) {
	DEBUG_TRACE("%s: LENGTH_OUT_OF_RANGE", __func__);
	return NVDS_LENGTH_OUT_OF_RANGE;
    }

    *len = custom_data.dev_name_len;
    memcpy(name, custom_data.dev_name, *len);
    return NVDS_OK;
}

uint8_t atvrc_custom_get_ui_layout(void)
{
    return custom_data.dev_ui_layout;
}

uint8_t *atvrc_custom_get_fw_rev(void)
{
    return fw_rev;
}

static bool is_mtk_wake(void)
{
    uint16_t product_ver = (custom_data.pnp_id[6] << 8) + custom_data.pnp_id[5];
#define PRODUCT_VER_MTK_FORMAT 0x0101
    return product_ver == PRODUCT_VER_MTK_FORMAT;
}

#define FEATURE_DISABLED 0xFFFF
uint8_t atvrc_custom_check_wake_key(uint32_t bt_key)
{
    if (is_mtk_wake()) {
	if (bt_key == BT_POWER) {
	    return ATVRC_WAKE_MTK_POWER;
	}
	if (bt_key == BT_NETFLIX) {
	    return ATVRC_WAKE_MTK_NETFLIX;
	}
	return ATVRC_WAKE_NOT_WAKE_KEY;
    }
    if (!custom_data.wake_key_1 && !custom_data.wake_key_2 &&
	(custom_data.wake_key_1 == FEATURE_DISABLED) &&
	(custom_data.wake_key_2 == FEATURE_DISABLED)) {
	return ATVRC_WAKE_NOT_WAKE_KEY;
    }
    for (uint8_t i = 0; i < WAKE_KEY_NUM; i++) {
	if (wake_key_bt_id[i] == bt_key) {
	    if (custom_data.wake_key_1 & (1 << i)) {
		return ATVRC_WAKE_KEY1;
	    }
	    if (custom_data.wake_key_2 & (1 << i)) {
		return ATVRC_WAKE_KEY2;
	    }
	}
    }
    return ATVRC_WAKE_NOT_WAKE_KEY;
}

bool atvrc_custom_is_cwake(void)
{
    return custom_data.wake_pkt_len;
}

bool atvrc_custom_is_cwake_only(void)
{
    return !custom_data.wake_pkt_send_mode;
}

#define AD_FLAG_LEN 3
#define ATVRC_DISABLED 0xFF
uint8_t atvrc_custom_set_cwake_pkt(uint8_t *data, uint8_t key_id,
    uint8_t key_cnt, uint8_t const *addr)
{
    ASSERT_ERR(custom_data.wake_pkt_len > AD_FLAG_LEN);

    memcpy(data, custom_data.wake_pkt_data + AD_FLAG_LEN,
	custom_data.wake_pkt_len - AD_FLAG_LEN);
    if (custom_data.key_id_oft != ATVRC_DISABLED) {
	data[custom_data.key_id_oft - AD_FLAG_LEN] = key_id;
    }
    if (custom_data.key_cnt_oft != ATVRC_DISABLED) {
	data[custom_data.key_cnt_oft - AD_FLAG_LEN] = key_cnt;
    }
    if (custom_data.peer_addr_oft != ATVRC_DISABLED) {
	memcpy(data + custom_data.peer_addr_oft - AD_FLAG_LEN, addr,
	    GAP_BD_ADDR_LEN);
    }
    return custom_data.wake_pkt_len - AD_FLAG_LEN;
}

#define ATVRC_ENABLED 1
bool atvrc_custom_is_period_wake(void)
{
    return custom_data.period_wake == ATVRC_ENABLED;
}

uint16_t atvrc_custom_get_wake_interval(void)
{
    return custom_data.period_wake_interval;
}

bool atvrc_custom_is_rpa_enabled(void)
{
    return custom_data.rpa_switch == ATVRC_ENABLED;
}

bool atvrc_custom_is_ble_adv_enabled(void)
{
    return custom_data.disable_ble;
}

bool atvrc_custom_is_cache_pwr(void)
{
    return custom_data.cache_pwr;
}
