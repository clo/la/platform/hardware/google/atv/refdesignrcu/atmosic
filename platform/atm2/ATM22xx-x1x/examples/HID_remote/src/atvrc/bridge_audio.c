/**
 *******************************************************************************
 *
 * @file bridge_audio.c
 *
 * @brief Bridge audio service
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */

#include "arch.h"
#include "atvrc_porting.h"
#include "bridge_audio.h"
#include "rc_atvv.h"
#include "rc_pdm.h"
#include "nvds.h"
#include "sw_event.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include "application/audio/gl_audio.h"
#pragma GCC diagnostic pop
#include "stack/ble/ble_format.h"
#include "vendor/827x_ble_remote/app_audio.h"
#include "vendor/827x_ble_remote/app_config.h"

extern void google_voice_data_notify_proc(void);

uint8_t app_mtu_size;
static uint16_t app_pkt_size;
typedef struct {
    uint16_t wr_oft;
    uint16_t rd_oft;
    uint16_t unsend;
    bool mic_status;
} audio_ctl_t;

static audio_ctl_t au_ctl;
static audio_sync_t au_sync;
static sw_event_id_t sync_evt;
extern uint16_t atv_char_ctl_ccc, atv_char_rx_ccc;

__FAST
audio_sync_t *bridge_audio_get_sync_info(void)
{
    return &au_sync;
}

__FAST
static void audio_sync(sw_event_id_t event_id, void const *ctx)
{
    sw_event_clear(sync_evt);
    au_sync.codec = google_voice_codec_used;
    au_sync.frame_no += 1;
    au_sync.pred_val = rc_atvv_get_pred_val(&(au_sync.step_idx));
}

#include "ke_mem.h"

#define AUDIO_FRAME_SIZE 120
#define NUM_AUDIO_BUF 20
#define AUDIO_BUF_LEN AUDIO_FRAME_SIZE * NUM_AUDIO_BUF
static uint8_t *audio_buf;

typedef struct {
    uint16_t rx_ccc;
    uint16_t ctl_ccc;
} atvv_ccc_t;

static atvv_ccc_t atvv_ccc;

#define NVDS_TAG_ATVRC_VOICE_CCC_CFG 0xC3

extern void google_cmd_proc(void);
static rep_vec_err_t bridge_audio_plf_schedule(void)
{
    google_cmd_proc();
    return RV_NEXT;
}

void bridge_audio_init(void)
{
    audio_buf = ke_malloc(AUDIO_BUF_LEN, KE_MEM_ENV);
    sync_evt = sw_event_alloc(audio_sync, NULL);
    RV_PLF_SCHEDULE_ADD(bridge_audio_plf_schedule);
    nvds_tag_len_t len = sizeof(atvv_ccc_t);
    if (nvds_get(NVDS_TAG_ATVRC_VOICE_CCC_CFG, &len, (uint8_t*)&atvv_ccc)
	!= NVDS_OK) {
	DEBUG_TRACE("%s: Fail to read ATVRC VOICE CCC NVDS tag", __func__);
    }
    atv_char_rx_ccc = atvv_ccc.rx_ccc;
    atv_char_ctl_ccc = atvv_ccc.ctl_ccc;
    active_mic_open();
}

__FAST
void bridge_audio_write_voice_data(uint8_t data)
{
    audio_buf[au_ctl.wr_oft++] = data;
    if (!(au_ctl.wr_oft % AUDIO_FRAME_SIZE)) {
	au_ctl.unsend += AUDIO_FRAME_SIZE;
	if (au_ctl.unsend >= AUDIO_BUF_LEN) {
	    DEBUG_TRACE("Audio buffer overflow: pause PDM");
	    rc_pdm_pause();
	    return;
	}
	google_voice_data_notify_proc();
	sw_event_set(sync_evt);
    }

    if (au_ctl.wr_oft >= AUDIO_BUF_LEN) {
	au_ctl.wr_oft = 0;
    }
}

__FAST
void bridge_audio_resend_voice(void)
{
    if (au_ctl.unsend > AUDIO_FRAME_SIZE) {
	google_voice_data_notify_proc();
    }
}

extern uint8_t read_assistant_model(void);
bool bridge_audio_is_legacy_model(void)
{
    return read_assistant_model() == REASON_MICOPEN;
}

bool bridge_audio_is_htt_model(void)
{
    return read_assistant_model() == REASON_HTT;
}

void bridge_audio_start_search(void)
{
    google_voice_start();
    sw_event_set(sync_evt);
}

void bridge_audio_dpad_select(void)
{
    google_voice_dpad_select();
}

extern uint8_t app_audio_key_stop(uint8_t reason);
void bridge_audio_stop(void)
{
    app_audio_key_stop(bridge_audio_is_htt_model() ? REASON_RELEASE_HTT :
	REASON_OTHERS);
}

void bridge_audio_set_mtu(uint8_t mtu)
{
    app_mtu_size = mtu;
    extern void set_audio_frame_size(uint8_t);
    set_audio_frame_size(mtu);
}

void bridge_audio_set_pkt_size(uint16_t pkt_size)
{
    DEBUG_TRACE("%s: %d", __func__, pkt_size);
    app_pkt_size = pkt_size;
}

void bridge_audio_ready_ind(bool ready)
{
    rc_atvv_ready(ready);
}

static void write_atvv_ccc(atvv_ccc_t *ccc)
{
    nvds_tag_len_t len = sizeof(atvv_ccc_t);
    nvds_put(NVDS_TAG_ATVRC_VOICE_CCC_CFG, len, (uint8_t*)ccc);
}

/// Porting refDesignRcu audio related functions
int bridge_audio_ccc_write(void* msg)
{
    rf_packet_att_data_t *pw = (rf_packet_att_data_t *)msg;
    uint16_t handle = pw->handle;
    uint16_t data = (pw->dat[1] << 8) + pw->dat[0];
    if (handle == AUDIO_GOOGLE_RX_CCC_H) {
	DEBUG_TRACE("ATVV RX Notify: %s", data ? "Enabled" : "Disabled");
	atvv_ccc.rx_ccc = atv_char_rx_ccc = data;
	write_atvv_ccc(&atvv_ccc);
    }

    if (handle == AUDIO_GOOGLE_CTL_CCC_H) {
	DEBUG_TRACE("ATVV CTL Notify: %s", data ? "Enabled" : "Disabled");
	atvv_ccc.ctl_ccc = atv_char_ctl_ccc = data;
	write_atvv_ccc(&atvv_ccc);
    }

    rc_atvv_ready(atv_char_ctl_ccc && atv_char_rx_ccc);
    return 0;
}

__FAST
uint8_t *bridge_audio_read_frame(void)
{
    if (!au_ctl.unsend) {
	return NULL;
    }
    return audio_buf + au_ctl.rd_oft;
}

__FAST
void bridge_audio_sent_frame_ind(void)
{
    au_ctl.unsend -= AUDIO_FRAME_SIZE;
    if (rc_pdm_is_paused() && au_ctl.unsend < (AUDIO_BUF_LEN / 2)) {
	DEBUG_TRACE("Audio buffer available: resume PDM");
	rc_pdm_resume();
    }
    au_ctl.rd_oft += AUDIO_FRAME_SIZE;
    if (au_ctl.rd_oft >= AUDIO_BUF_LEN) {
	au_ctl.rd_oft = 0;
    }

}

void ui_enable_mic(int en)
{
    if (!en) {
	if (au_ctl.mic_status) {
	    rc_atvv_mic_close();
	    memset(&au_ctl, 0, sizeof(audio_ctl_t));
	}
	return;
    }
    au_ctl.mic_status = true;
#ifdef CFG_FORCE_8K_NO_DLE
    bool is8k = (app_pkt_size >= app_mtu_size) ?
	!(google_voice_codec_used & CODEC_SUPPORTED_16K) : true;
    if (is8k) {
	google_voice_codec_used = CODEC_USED_8K;
	google_voice_pcm_sample_packet = VOICE_V1P0_ADPCM_UNIT_SIZE * 2;
    }
#else
    bool is8k = !(google_voice_codec_used & CODEC_SUPPORTED_16K);
#endif
    rc_atvv_mic_open(is8k);
}
