/**
 *******************************************************************************
 *
 * @file rc_ota.h
 *
 * @brief OTA application part
 *
 * Copyright (C) Atmosic 2020-2022
 *
 *******************************************************************************
 */
#pragma once
#include "ble_otaps.h"

/**
 *******************************************************************************
 * @brief Get OTAP profile parameter.
 * This is used for passing parameters to @ref atm_gap_prf_reg.
 * @return Parameters.
 *******************************************************************************
 */
ble_otaps_param_t const *rc_otps_param(void);
