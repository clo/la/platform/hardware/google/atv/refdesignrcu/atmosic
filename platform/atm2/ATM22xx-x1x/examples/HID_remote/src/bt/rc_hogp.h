/**
 *******************************************************************************
 *
 * @file rc_hogp.h
 *
 * @brief HOGP application part
 *
 * Copyright (C) Atmosic 2020-2022
 *
 *******************************************************************************
 */
#pragma once

#include "ble_hogpd.h"

/**
 *******************************************************************************
 * @brief Get HOGP profile parameter.
 * This is used for passing parameters to @ref atm_gap_prf_reg.
 * @return Parameters.
 *******************************************************************************
 */
ble_hogpd_param_t const *rc_hogp_param(void);

/**
 *******************************************************************************
 * @brief Send key to peer.
 *
 * @param[in] keycode Key code.
 * @note High 16 bits is usage, low 16 bits is key code.
 *******************************************************************************
 */
void rc_hogp_send_single_key(uint32_t keycode);

/**
 *******************************************************************************
 * @brief Get HOGP state
 *
 * @return HOGP state.
 *******************************************************************************
 */
ble_hogpd_state_t rc_hogp_state(void);

#ifdef CFG_VOHID
/**
 *******************************************************************************
 * @brief Get audio report buffer index
 *
 * @param[in] hogp_cb Callback while sending complete.
 * @param[in] ctx Application context.
 * @return HID audio buffer context
 *******************************************************************************
 */
void *rc_hogp_get_audio_buf(ble_hogpd_report_cmp_cb_t hogp_cb, void const *ctx);
#endif
