/**
 *******************************************************************************
 *
 * @file rc_test_mode.h
 *
 * @brief Test mode for fast connection
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */

#pragma once

#include <stdint.h>
#include <stdbool.h>
#include "atm_adv.h"

// Two continuous test input would cause system reset and
// enter test mode from index 0 to 15. The direct ADV can be generated right
// after reset through rc_create_test_mode_adv API. Here is the mapping of
// input combination and index.
// |-------+--------|
// | Index | Inputs |
// |=======+========|
// | 0     | U U    |
// | 1     | U D    |
// | 2     | U L    |
// | 3     | U R    |
// | 4     | D U    |
// | 5     | D D    |
// | 6     | D L    |
// | 7     | D R    |
// | 8     | L U    |
// | 9     | L D    |
// | 10    | L L    |
// | 11    | L R    |
// | 12    | R U    |
// | 13    | R D    |
// | 14    | R L    |
// | 15    | R R    |
// |-------+--------|

/// Test input definition.
typedef enum {
    // Cancel test
    RC_TEST_CANCEL,
    // Up get
    RC_TEST_U_GOT,
    // Down get
    RC_TEST_D_GOT,
    // Left get
    RC_TEST_L_GOT,
    // Right get
    RC_TEST_R_GOT,
} rc_test_input_t;

/// NVDS base tag for fasting connection bt address
#define FAST_CONN_NVDS 0xD0

/// Default address
#define FAST_CONN_D4ADDR 0x11, 0x18, 0x12, 0x23, 0x01, 0x24

/**
 *******************************************************************************
 * @brief Test mode control
 * @param[in] input Input to trigger state machine. Two continue inputs which
 * are not RC_TEST_CANCEL will enable test mode with specific index. If the I1 and I2
 * are first and second input, then the index of test mode is (I1 - 1) * 4 +
 * (I2 -1).
 *******************************************************************************
 */
void rc_test_mode_control(rc_test_input_t input);

/**
 *******************************************************************************
 * @brief Generate test mode direct ADV structure for fast connection.
 * The target address of direct ADV would get from NVDS tag which tag number is
 * FAT_CONN_NVDS + test mode index.
 * @return Data which is used to create direct ADV instance.
 *******************************************************************************
 */
atm_adv_create_t *rc_create_test_mode_adv(void);

/**
 *******************************************************************************
 * @brief Query for direct ADV retry left
 * @param[in] minus_one True if substract the value of retry time by 1.
 * @return Current value of retry times.
 *******************************************************************************
 */
uint8_t rc_test_mode_adv_times_left(bool minus_one);
