/**
 *******************************************************************************
 *
 * @file uuid.h
 *
 * @brief Android TV Service UUIDs
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */
#pragma once

#define GOOGLE_UUID_POSTFIX 0x5A, 0x21, 0x4F, 0x05, 0xBC, 0x7D, 0xAF, 0x01, \
    0xF6, 0x17, 0xB6, 0x64

/* Google IR Configuration over BLE Service
 * UUID : D343BFC0-5A21-4F05-BC7D-AF01F617B664
 */
#define SVC_UUID_IR_SERVICE 0xD3, 0x43, 0xBF, 0xC0, GOOGLE_UUID_POSTFIX

/* Control Characteristic
 * UUID : D343BFC1-5A21-4F05-BC7D-AF01F617B664
 */
#define CHAR_UUID_IR_PROG_CONTROL 0xD3, 0x43, 0xBF, 0xC1, GOOGLE_UUID_POSTFIX

/* Key Code Characteristic
 * UUID : D343BFC2-5A21-4F05-BC7D-AF01F617B664
 */
#define CHAR_UUID_IR_KEY_ID 0xD3, 0x43, 0xBF, 0xC2, GOOGLE_UUID_POSTFIX

/* IR Code Characteristic
 * UUID : D343BFC3-5A21-4F05-BC7D-AF01F617B664
 */
#define CHAR_UUID_IR_CODE 0xD3, 0x43, 0xBF, 0xC3, GOOGLE_UUID_POSTFIX

/* IR Suppress Characteristic
 * UUID : D343BFC4-5A21-4F05-BC7D-AF01F617B664
 */
#define CHAR_UUID_IR_SUPPRESS 0xD3, 0x43, 0xBF, 0xC4, GOOGLE_UUID_POSTFIX

/* Key Down Characteristic
 * UUID : D343BFC5-5A21-4F05-BC7D-AF01F617B664
 */
#define CHAR_UUID_IR_KEY_EVENT 0xD3, 0x43, 0xBF, 0xC5, GOOGLE_UUID_POSTFIX

/* Google Voice over BLE Service
 * UUID : AB5E0001-5A21-4F05-BC7D-AF01F617B664
 */
#define SVC_UUID_ATVV_SERVICE 0xAB, 0x5E, 0x00, 0x01, GOOGLE_UUID_POSTFIX

/* ATVV TX Characteristic
 * UUID : AB5E0002-5A21-4F05-BC7D-AF01F617B664
 */
#define CHAR_UUID_ATVV_TX 0xAB, 0x5E, 0x00, 0x02, GOOGLE_UUID_POSTFIX

/* ATVV Audio Characteristic
 * UUID : AB5E0003-5A21-4F05-BC7D-AF01F617B664
 */
#define CHAR_UUID_ATVV_AUDIO 0xAB, 0x5E, 0x00, 0x03, GOOGLE_UUID_POSTFIX

/* ATVV CTL Characteristic
 * UUID : AB5E0003-5A21-4F05-BC7D-AF01F617B664
 */
#define CHAR_UUID_ATVV_CTL 0xAB, 0x5E, 0x00, 0x04, GOOGLE_UUID_POSTFIX