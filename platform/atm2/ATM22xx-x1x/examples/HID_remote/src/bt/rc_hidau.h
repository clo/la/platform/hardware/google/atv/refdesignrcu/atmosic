/**
 *******************************************************************************
 *
 * @file rc_hidau.h
 *
 * @brief Voice over HID application part
 *
 * Copyright (C) Atmosic 2021
 *
 *******************************************************************************
 */

#pragma once

#ifdef CFG_PDM_MSBC
#include "sbc_enc_wrapper.h"
#define HID_AU_REPORT_SIZE (MSBC_ENC_SIZE * 2)
#else
#define HID_AU_REPORT_SIZE 128
#endif

/**
 *******************************************************************************
 * @brief Start Audio search.
 *******************************************************************************
 */
void rc_hidau_start_search(void);

/**
 *******************************************************************************
 * @brief Stop Audio search.
 *******************************************************************************
 */
void rc_hidau_stop_search(void);

/**
 *******************************************************************************
 * @brief Get audio frame
 *
 * @param[out] frame Byte point of audio frame
 * @return Residual number from pointer.
 *******************************************************************************
 */
uint16_t rc_hidau_frame_get(uint8_t **frame);

/**
 *******************************************************************************
 * @brief Move audio frame pointer forward
 *
 * @param[in] count Byte count to move
 * @return Actual moved byte count.
 * @note If reach the end, rc_bta will send it out. If current frame is not
 * enough, the actual moving count would be the residual number.
 *******************************************************************************
 */
uint16_t rc_hidau_frame_ptr_move(uint16_t count);

/**
 *******************************************************************************
 * @brief Fill data to audio frame.
 *
 * @param[in] pcm PCM data.
 * @note If reach the end of frame, rc_bta will send it out.
 *******************************************************************************
 */
void rc_hidau_fill_pcm(uint8_t pcm);

/**
 *******************************************************************************
 * @brief Initialize ATVV environment.
 *******************************************************************************
 */
void rc_hidau_init(void);

#ifdef CFG_PDM_LOCAL_TEST
/**
 *******************************************************************************
 * @brief ATVV test start
 *
 * @note This is used for ATVV flow unit test locally.
 *******************************************************************************
 */
void rc_hidau_test_start(void);
/**
 *******************************************************************************
 * @brief Pretend the bad link situation without allocating buffers.
 *
 * @note This is used for ATVV flow unit test locally.
 *******************************************************************************
 */
void rc_hidau_fake_bad_link(void);
#endif // CFG_PDM_LOCAL_TEST

