/**
 *******************************************************************************
 *
 * @file rc_gap.h
 *
 * @brief GAP application part
 *
 * Copyright (C) Atmosic 2020-2021
 *
 *******************************************************************************
 */
#pragma once

/// RF test type for @ref rc_gap_rf_test_adjust API.
typedef enum {
    /// PHY.
    RC_RFTEST_PHY,
    /// Channel.
    RC_RFTEST_CH,
    /// Power.
    RC_RFTEST_PWR,
} rc_rftest_type_t;

/**
 *******************************************************************************
 * @brief Bluetooth initialization.
 *******************************************************************************
 */
void rc_gap_init(void);

/**
 *******************************************************************************
 * @brief Disconnect current link if have.
 *******************************************************************************
 */
void rc_gap_disconnect(void);

/**
 *******************************************************************************
 * @brief Start or stop device advertisement.
 * @param[in] en True for enabling, otherwise for disabling.
 *******************************************************************************
 */
void rc_gap_discoverable(bool en);

/**
 *******************************************************************************
 * @brief Negotiate current link parameter.
 *******************************************************************************
 */
void rc_gap_nego_parameter(void);

/**
 *******************************************************************************
 * @brief Stop all the activities and links and back to idle.
 *******************************************************************************
 */
void rc_gap_stop(void);

/**
 *******************************************************************************
 * @brief Enter RF testing mode.
 *******************************************************************************
 */
void rc_gap_enter_rf_test(void);

/**
 *******************************************************************************
 * @brief Adjust RF testing parameter.
 *
 * @param[in] is_up True for increase parameter value.
 * @param[in] type Parameter type.
 *******************************************************************************
 */
void rc_gap_rf_test_adjust(bool is_up, rc_rftest_type_t type);

/**
 *******************************************************************************
 * @brief Disable slave latency locally
 * @param[in] disable True for disabling slave latency. False for enabling slave
 * latency;
 *******************************************************************************
 */
void rc_gap_local_slave_latency(bool disable);

/**
 *******************************************************************************
 * @brief Enter pairing after next boot
 * @param[in] enable True for enable pairing. False disable
 * @note This switch is across hibernation.
 *******************************************************************************
 */
void rc_gap_enter_pairing_next_boot(bool enable);

/**
 *******************************************************************************
 * @brief Remove all bond information
 *******************************************************************************
 */
void rc_gap_remove_all_bond(void);

#ifdef CFG_ATVRC_WAKEUP
typedef enum {
    ATVRC_WAKE_NOT_WAKE_KEY,
    ATVRC_WAKE_KEY1,
    ATVRC_WAKE_KEY2,
    ATVRC_WAKE_MTK_POWER,
    ATVRC_WAKE_MTK_NETFLIX,
} atvrc_wake_key_t;

/**
 *******************************************************************************
 * @brief Start Wake up host
 * @param[in] id Wake up key ID
 *******************************************************************************
 */
void rc_gap_set_wake(atvrc_wake_key_t key);
#endif

#ifdef CFG_OTA_SPEEDUP
/**
 *******************************************************************************
 * @brief Negotiate current link parameter for OTA speed up.
 *******************************************************************************
 */
void rc_gap_nego_ota_parameter(void);
#endif
