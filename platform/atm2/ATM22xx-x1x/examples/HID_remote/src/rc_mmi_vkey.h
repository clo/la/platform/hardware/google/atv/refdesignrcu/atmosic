/*
 *******************************************************************************
 *
 * @file rc_mmi_vkey.h
 *
 * @brief VKEY application part.
 *
 * Copyright (C) Atmosic 2020-2021
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @brief Notify MMI state has been changed.
 *******************************************************************************
 */
void rc_mmi_vkey_state_change_notify(uint8_t next_state);

/**
 * @brief Get saved key.
 *
 * @return Key saved. O is invalid.
 */
uint32_t rc_mmi_vkey_get_saved(void);

/**
 * @brief Clear saved key.
 */
void rc_mmi_vkey_clear_saved(void);

#ifdef CFG_ATVRC_CUSTOM
/**
 * @brief Update UI layout configurable keys
 */
void rc_mmi_vkey_update_ui(uint8_t ui);
#endif

#ifdef CFG_VKEY_BUF
/**
 * @brief Flush buffered vkey events
 */
void rc_mmi_vkey_flush_buf(void);

/**
 * @brief Free buffered vkeys
 */
void rc_mmi_vkey_free_buf(void);
#endif