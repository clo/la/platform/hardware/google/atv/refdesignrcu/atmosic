/*
 *******************************************************************************
 *
 * @file rc_mmi.c
 *
 * @brief MMI of Atmosic remote controller reference design.
 *
 * Copyright (C) Atmosic 2020-2021
 *
 *******************************************************************************
 */
#pragma once

/// MMI states
typedef enum {
    /// System booted.
    MMI_S_BOOTED,
    /// Initialing.
    MMI_S_INITING,
    /// Initialized.
    MMI_S_IDLE,
    /// Pairing.
    MMI_S_PAIRING,
    /// Reconnecting.
    MMI_S_RECONNING,
    /// Connected but no profile ready.
    MMI_S_CONNECTED,
#ifdef CFG_VOHID
    /// HID is ready
    MMI_S_HID_READY,
    /// Audio streaming
    MMI_S_HID_STREAMING,
#else
    /// ATVV is ready but HID isn't.
    MMI_S_ATVV_ONLY,
    /// HID is ready but ATVV isn't.
    MMI_S_HID_ONLY,
    /// HID and ATVV are ready.
    MMI_S_HID_ATVV,
    /// ATVV is transferring audio.
    MMI_S_ATVVING,
#endif
    /// Disconnecting.
    MMI_S_DISCONNING,
    /// Under RF testing mode.
    MMI_S_RF_TEST,
    /// Under PDM local test
    MMI_S_PDM_TEST,
    /// Dummy state for marking.
    MMI_S_END
} mmi_state_t;

/// MMI operations for MMI state transition
typedef enum {
    /// Started initialing.
    MMI_OP_INITING,
    /// Initialized.
    MMI_OP_INIT_DONE,
    /// Started reconnecting.
    MMI_OP_RECONNING,
    /// Started pairing.
    MMI_OP_PAIRING,
    /// Pairing failed.
    MMI_OP_PAIR_FAIL,
    /// Reconnection timeout.
    MMI_OP_RECONN_TOUT,
    /// Pairing timeout.
    MMI_OP_PAIR_TOUT,
    /// Pairing success.
    MMI_OP_PAIR_SUCCESS,
    /// Connected.
    MMI_OP_CONNECTED,
    /// Disconnected.
    MMI_OP_DISCONNED,
    /// HOGP became ready.
    MMI_OP_HID_READY,
#ifndef CFG_VOHID
    /// ATVV became ready.
    MMI_OP_ATVV_READY,
#endif
    /// HOGP became unready.
    MMI_OP_HID_UNREADY,
#ifndef CFG_VOHID
    /// ATVV became unready.
    MMI_OP_ATVV_UNREADY,
#endif
    /// Started transferring audio.
    MMI_OP_OPEN_MIC,
    /// Stopped transferring audio.
    MMI_OP_CLOSE_MIC,
    /// Started disconnecting.
    MMI_OP_DISCONNING,
    /// ADV became stopped.
    MMI_OP_ADV_STOPPED,
    /// Number of MMI operations
    MMI_OP_NUM
} mmi_op_t;

/// MMI timeout setting for @ref rc_mmi_idle_timer API
typedef enum mmi_tout_s {
    /// Enable and reset MMI timeout timer.
    MMI_TOUT_START,
    /// Disable and clear MMI timeout timer.
    MMI_TOUT_STOP,
    /// Force MMI timeout in a short time.
    MMI_TOUT_FORCE,
} mmi_tout_t;

/// MMI test mode type for @ref rc_mmi_enter_test API
typedef enum mmi_test_s {
#ifndef CFG_VOHID
    /// ATVV auto test.
    MMI_TEST_ATVV,
#endif
    /// RF test.
    MMI_TEST_RF,
    /// KEY auto test.
    MMI_TEST_KEY,
#ifdef CFG_PDM_LOCAL_TEST
    /// PDM local test.
    MMI_TEST_PDM,
#endif
} mmi_test_t;

/**
 * @brief MMI state machine initialization.
 */
void rc_mmi_init(void);

/**
 * @brief MMI state transition.
 * @param[in] op Current operation.
 */
void rc_mmi_transition(mmi_op_t op);

/**
 * @brief MMI idle timer setting.
 * @param[in] op Operation.
 */
void rc_mmi_idle_timer(mmi_tout_t op);

/**
 * @brief Enter test mode.
 * @param[in] op Test mode.
 */
void rc_mmi_enter_test(mmi_test_t op);
