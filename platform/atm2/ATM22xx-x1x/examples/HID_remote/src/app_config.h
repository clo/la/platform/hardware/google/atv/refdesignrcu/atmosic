/**
 *******************************************************************************
 *
 * @file app_config.h
 *
 * @brief Application configuration.
 *
 * Copyright (C) Atmosic 2021-2022
 *
 *******************************************************************************
 */
#pragma once

#include "ble_att.h"
#include "sw_timer.h"

// Connection Parameters Setting
// Preferred connection parameter negotiation retry times.
#ifndef RC_PARAM_NEGO_TIMES
#define RC_PARAM_NEGO_TIMES 1
#endif
// Preferred connection parameter negotiation timeout time.
#ifndef RC_PARAM_NEGO_TOUT_CS
#define RC_PARAM_NEGO_TOUT_CS 300
#endif

// Preferred connection parameter negotiation delay time.
#ifndef RC_PARAM_NEGO_DELAY_CS
#define RC_PARAM_NEGO_DELAY_CS 100
#endif

// Timeout Settings

// Timeout time after HID ready.
// A timer with this timeout value will be set when link is connected and HID is
// ready. When this timer occurs, system will disconnect link and go to hibernate.
// The timer would be reset on any of user input.
// If this value is zero, the link will always be maintained.
#ifndef RC_CONN_READY_TOUT_CS
#define RC_CONN_READY_TOUT_CS 0
#endif

// Timeout time after connected.
// A timer with this timeout value will be set when link is connected but HID is
// not ready. When this timer occurs, system will disconnect link and go to hibernate.
// This timer will be clear after HID is ready.
// If this value is zero, the link will always be maintained.
#ifndef RC_CONN_NOT_READY_TOUT_CS
#define RC_CONN_NOT_READY_TOUT_CS 0
#endif

// DISS configuration

// Length of APP_DIS_MANUFACTURER_NAME
#define APP_DIS_MANUFACTURER_NAME_LEN (sizeof(APP_DIS_MANUFACTURER_NAME) - 1)

// Length of APP_DIS_MANUFACTURER_NAME
#define APP_DIS_MODEL_NB_STR_LEN (sizeof(APP_DIS_MODEL_NB_STR) - 1)

// Length of APP_DIS_MANUFACTURER_NAME
#define APP_DIS_SERIAL_NB_STR_LEN (sizeof(APP_DIS_SERIAL_NB_STR) - 1)

// Length of APP_DIS_MANUFACTURER_NAME
#define APP_DIS_FIRM_REV_STR_LEN (sizeof(APP_DIS_FIRM_REV_STR) - 1)

// Length of APP_DIS_MANUFACTURER_NAME
#define APP_DIS_SYSTEM_ID_LEN (sizeof(APP_DIS_SYSTEM_ID) - 1)

// Length of APP_DIS_MANUFACTURER_NAME
#define APP_DIS_HARD_REV_STR_LEN (sizeof(APP_DIS_HARD_REV_STR) - 1)

// Length of APP_DIS_MANUFACTURER_NAME
#define APP_DIS_SW_REV_STR_LEN (sizeof(APP_DIS_SW_REV_STR) - 1)

// Length of APP_DIS_MANUFACTURER_NAME
#define APP_DIS_IEEE_LEN (sizeof(APP_DIS_IEEE) - 1)

// Length of APP_DIS_MANUFACTURER_NAME
#define APP_DIS_PNP_ID_LEN (sizeof(APP_DIS_PNP_ID) - 1)

// Manufacturer Name Value
#define APP_DIS_MANUFACTURER_NAME "Atmosic Tech."

// Model Number String Value
#define APP_DIS_MODEL_NB_STR "ATV-RC-01"

// Serial Number
#define APP_DIS_SERIAL_NB_STR "1.0.0.0"

#ifdef CFG_NONRF_HARV
// Firmware Revision
#define APP_DIS_FIRM_REV_STR APP_VERSION"PV"
// Software Revision String
#define APP_DIS_SW_REV_STR APP_VERSION"PV"
#else
// Firmware Revision
#define APP_DIS_FIRM_REV_STR APP_VERSION
// Software Revision String
#define APP_DIS_SW_REV_STR APP_VERSION
#endif

// Hardware Revision String
#define APP_DIS_HARD_REV_STR "1.0.0"

// System ID Value
#define APP_DIS_SYSTEM_ID "\x12\x34\x56\xFF\xFE\x9A\xBC\xDE"

// IEEE
#define APP_DIS_IEEE "\xFF\xEE\xDD\xCC\xBB\xAA"

/**
 * PNP ID Value - LSB -> MSB
 * Vendor ID Source : 0x01 (bluetooth SIG)
 * Vendor ID : 0x7545
 * Product ID : 0x0021
 * Product Version : 0x0110
 */
#define APP_DIS_PNP_ID "\x01\x45\x75\x21\x00\x10\x01"

// Feature enabled
#define APP_DIS_FEATURES DIS_MANUFACTURER_NAME_CHAR_SUP | \
    DIS_MODEL_NB_STR_CHAR_SUP | DIS_SERIAL_NB_STR_CHAR_SUP | \
    DIS_HARD_REV_STR_CHAR_SUP | DIS_FIRM_REV_STR_CHAR_SUP | \
    DIS_SW_REV_STR_CHAR_SUP | DIS_SYSTEM_ID_CHAR_SUP | \
    DIS_IEEE_CHAR_SUP | DIS_PNP_ID_CHAR_SUP

// Security level
#define APP_DIS_SEC_PROPERTY BLE_SEC_PROP_NO_SECURITY

// BASS configuration

// Security level
#define APP_BASS_SEC_PROPERTY SEC_PROFLE_LEVEL

// OTAP configuration

// OTA board identifier
#define RC_OTA_BOARD_ID 02

// Atm_gap configuration

// Maximal support BT profiles
#ifdef CFG_ATVRC_ATT
#define CFG_GAP_MAX_MODULES 6
#else
#define CFG_GAP_MAX_MODULES 5
#endif

// Appearance Icon
#define CFG_GAP_APPEARANCE 0x180

// Pairing Mode
#define CFG_GAP_PAIRING_MODE  (BLE_GAP_PAIRING_LEGACY)

// Minimal connection interval
#define CFG_GAP_CONN_INT_MIN 8

// Maximal connection interval
#define CFG_GAP_CONN_INT_MAX 8

// Peripheral latency
#define CFG_GAP_PERIPH_LATENCY 99

// Connection timeout
#define CFG_GAP_CONN_TIMEOUT 300

// GAP attribute connfiguration
#ifdef CFG_RC_BIG_MTU
#define CFG_GAP_ATT_CFG BLE_GAP_ATT_PERIPH_PREF_CON_PAR_EN_MASK \
    | BLE_GAP_ATT_CLI_AUTO_MTU_EXCH_EN_MASK
#else
#define CFG_GAP_ATT_CFG BLE_GAP_ATT_PERIPH_PREF_CON_PAR_EN_MASK
// Maximal transmit octets
#define CFG_GAP_MAX_TX_OCTETS BLE_MIN_OCTETS
// Maximal transmit time
#define CFG_GAP_MAX_TX_TIME BLE_MIN_TIME
// Maximize MTU (comply with 4.2)
#define CFG_GAP_MAX_LL_MTU 247
#endif

// Atm_adv configuration

// ADV payload - UUID
#define RC_HID_ADV_DAT_UUID 0x03, 0x03, 0x12, 0x18

// ADV payload - Appearance
#define RC_HID_ADV_DAT_APPEARANCE 0x03, 0x19, 0x80, 0x01

// ADV payload - Atmosic Vendor Application ID info
#define RC_HID_ADV_DAT_MANU 0x06, 0xFF, 0x0A, 0x24, 0x01, 0x03, 0x00

// Non constant ADV creation parameter
#define CFG_ADV_CREATE_PARAM_CONST 0

// Maximal ADV instances
#ifdef CFG_ATVRC_WAKEUP
#define CFG_GAP_ADV_MAX_INST 5
#else
#define CFG_GAP_ADV_MAX_INST 2
#endif

// ADV0 configuration (for reconnection)

// Owner address
#define CFG_ADV0_OWNER_ADDR_TYPE BLE_OWN_STATIC_ADDR

// parameters for creation
#ifdef CFG_ATVRC_WAKEUP
#define CFG_ADV0_CREATE_PROPERTY ADV_LEGACY_DIR_CONN_MASK
#else
#define CFG_ADV0_CREATE_PROPERTY ADV_LEGACY_DIR_CONN_HDC_MASK
#endif
#define CFG_ADV0_CREATE_CHNL_MAP ADV_ALL_CHNLS
#define CFG_ADV0_CREATE_MAX_TX_POWER 0
#define CFG_ADV0_CREATE_DISCOVERY_MODE ADV_MODE_NON_DISC

// ADV timeout duration
#ifdef CFG_ATVRC_WAKEUP
#define CFG_ADV0_START_DURATION 300
#else
#define CFG_ADV0_START_DURATION 12000
#endif

// ADV data payload
#define CFG_ADV0_DATA_ADV_PAYLOAD RC_HID_ADV_DAT_UUID, \
    RC_HID_ADV_DAT_APPEARANCE, RC_HID_ADV_DAT_MANU

// Scan response enabling
#define CFG_ADV0_DATA_SCANRSP_ENABLE 0

// ADV1 (for pairing)

// Owner address
#define CFG_ADV1_OWNER_ADDR_TYPE BLE_OWN_STATIC_ADDR

// parameters for creation
#define CFG_ADV1_CREATE_PROPERTY ADV_LEGACY_UNDIR_CONN_MASK
#define CFG_ADV1_CREATE_CHNL_MAP ADV_ALL_CHNLS
#define CFG_ADV1_CREATE_MAX_TX_POWER 0
#define CFG_ADV1_CREATE_FILTER_POLICY FILTER_SCAN_ANY_CON_ANY

// ADV timeout duration
#ifdef CFG_ATVRC_MMI
#define CFG_ADV1_START_DURATION 18000
#else
#define CFG_ADV1_START_DURATION 6000
#endif

#ifdef CFG_ATVRC_WAKEUP
#define CFG_ADV1_CREATE_DISCOVERY_MODE ADV_MODE_LIM_DISC
#define UUID_HID 0x12, 0x18
#define UUID_BATTERY 0x0F, 0x18
#define RC_SRV_HID_BAT 0x05, 0x02, UUID_HID, UUID_BATTERY
// ADV data payload
#define CFG_ADV1_DATA_ADV_PAYLOAD RC_HID_ADV_DAT_APPEARANCE, RC_SRV_HID_BAT

// Scan response enabling
#define CFG_ADV1_DATA_SCANRSP_ENABLE 0

// Google wake up packet configuration
#define CFG_ADV2_OWNER_ADDR_TYPE BLE_OWN_STATIC_ADDR
#define CFG_ADV2_CREATE_TYPE ADV_TYPE_LEGACY
#define CFG_ADV2_CREATE_PROPERTY ADV_LEGACY_UNDIR_CONN_MASK
#define CFG_ADV2_CREATE_FILTER_POLICY FILTER_SCAN_ANY_CON_ANY
#define CFG_ADV2_CREATE_DISCOVERY_MODE ADV_MODE_BEACON
#define CFG_ADV2_CREATE_CHNL_MAP ADV_ALL_CHNLS
#define CFG_ADV2_CREATE_PRIM_PHY BLE_GAP_PHY_LE_1MBPS
#define CFG_ADV2_CREATE_INTERVAL_MIN (ADV_INTERVAL_MIN * 2)
#define CFG_ADV2_CREATE_INTERVAL_MAX (ADV_INTERVAL_MIN * 2)
#define CFG_ADV2_CREATE_MAX_TX_POWER 0
#define CFG_ADV2_CREATE_SEC_MAX_SKIP 0
#define CFG_ADV2_CREATE_SEC_PHY BLE_GAP_PHY_LE_1MBPS
#define CFG_ADV2_CREATE_SEC_ADV_SID 0
#define CFG_ADV2_CREATE_PERI_INTERVAL_MIN 80
#define CFG_ADV2_CREATE_PERI_INTERVAL_MAX 80
#define CFG_ADV2_CREATE_PEER_ADDR_TYPE 0
#define CFG_ADV2_CREATE_PEER_ADDR 0

#define CFG_ADV2_START_DURATION 300
#define CFG_ADV2_START_MAX_ADV_EVENT 0

#define CFG_ADV2_DATA_ADV_ENABLE 1
#define CFG_ADV2_DATA_SCANRSP_ENABLE 0

#define UUID_WAKEUP_SRV 0x36, 0xFD
#define PACKET_FORMAT_VER 0x01
#define DUMMY_KEY_ID 0xFF
#define DUMMY_KEY_PRESS_CNT 0xFF
#define DUMMY_PEER_BD_ADDR 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

#define RC_SRV_DATA_WAKEUP_SRV 0x0C, 0x16, UUID_WAKEUP_SRV, PACKET_FORMAT_VER, \
    DUMMY_KEY_ID, DUMMY_KEY_PRESS_CNT, DUMMY_PEER_BD_ADDR
#define CFG_ADV2_DATA_ADV_PAYLOAD RC_SRV_DATA_WAKEUP_SRV

// Customized wake up packet configuration
#define CFG_ADV3_OWNER_ADDR_TYPE BLE_OWN_STATIC_ADDR
#define CFG_ADV3_CREATE_TYPE ADV_TYPE_LEGACY
#define CFG_ADV3_CREATE_PROPERTY ADV_LEGACY_UNDIR_CONN_MASK
#define CFG_ADV3_CREATE_FILTER_POLICY FILTER_SCAN_ANY_CON_ANY
#define CFG_ADV3_CREATE_DISCOVERY_MODE ADV_MODE_NON_DISC
#define CFG_ADV3_CREATE_CHNL_MAP ADV_ALL_CHNLS
#define CFG_ADV3_CREATE_PRIM_PHY BLE_GAP_PHY_LE_1MBPS
#define CFG_ADV3_CREATE_INTERVAL_MIN (ADV_INTERVAL_MIN * 2)
#define CFG_ADV3_CREATE_INTERVAL_MAX (ADV_INTERVAL_MIN * 2)
#define CFG_ADV3_CREATE_MAX_TX_POWER 0
#define CFG_ADV3_CREATE_SEC_MAX_SKIP 0
#define CFG_ADV3_CREATE_SEC_PHY BLE_GAP_PHY_LE_1MBPS
#define CFG_ADV3_CREATE_SEC_ADV_SID 0
#define CFG_ADV3_CREATE_PERI_INTERVAL_MIN 80
#define CFG_ADV3_CREATE_PERI_INTERVAL_MAX 80
#define CFG_ADV3_CREATE_PEER_ADDR_TYPE 0
#define CFG_ADV3_CREATE_PEER_ADDR 0

#define CFG_ADV3_START_DURATION 300
#define CFG_ADV3_START_MAX_ADV_EVENT 0

#define CFG_ADV3_DATA_ADV_ENABLE 1
#define CFG_ADV3_DATA_SCANRSP_ENABLE 0
#define CFG_ADV3_DATA_ADV_PAYLOAD 0

// MTK wake up packet configuration
#define CFG_ADV4_OWNER_ADDR_TYPE BLE_OWN_STATIC_ADDR
#define CFG_ADV4_CREATE_TYPE ADV_TYPE_LEGACY
#define CFG_ADV4_CREATE_PROPERTY ADV_LEGACY_UNDIR_CONN_MASK
#define CFG_ADV4_CREATE_FILTER_POLICY FILTER_SCAN_ANY_CON_ANY
#define CFG_ADV4_CREATE_DISCOVERY_MODE ADV_MODE_NON_DISC
#define CFG_ADV4_CREATE_CHNL_MAP ADV_ALL_CHNLS
#define CFG_ADV4_CREATE_PRIM_PHY BLE_GAP_PHY_LE_1MBPS
#define CFG_ADV4_CREATE_INTERVAL_MIN (ADV_INTERVAL_MIN * 2)
#define CFG_ADV4_CREATE_INTERVAL_MAX (ADV_INTERVAL_MIN * 2)
#define CFG_ADV4_CREATE_MAX_TX_POWER 0
#define CFG_ADV4_CREATE_SEC_MAX_SKIP 0
#define CFG_ADV4_CREATE_SEC_PHY BLE_GAP_PHY_LE_1MBPS
#define CFG_ADV4_CREATE_SEC_ADV_SID 0
#define CFG_ADV4_CREATE_PERI_INTERVAL_MIN 80
#define CFG_ADV4_CREATE_PERI_INTERVAL_MAX 80
#define CFG_ADV4_CREATE_PEER_ADDR_TYPE 0
#define CFG_ADV4_CREATE_PEER_ADDR 0

#define CFG_ADV4_START_DURATION 1000
#define CFG_ADV4_START_MAX_ADV_EVENT 0

#define CFG_ADV4_DATA_ADV_ENABLE 1
#define RC_SRV_HID 0x03, 0x02, UUID_HID
#define KEY_ID_POWER 0xFF, 0xFF
#define RC_MTK_MANUF_DATA 0x13, 0xFF, 0x46, 0x00, 0x00, DUMMY_PEER_BD_ADDR, \
    KEY_ID_POWER, 0x00, 0x00, 'C', 'R', 'K', 'T', 'M'
#define CFG_ADV4_DATA_ADV_PAYLOAD RC_HID_ADV_DAT_APPEARANCE, RC_SRV_HID, \
    RC_MTK_MANUF_DATA

#define CFG_ADV4_DATA_SCANRSP_ENABLE 1
#define RC_LOCAL_NAME_MTK_RC 0x0F, 0x09, 'M', 'T', 'K', ' ', 'B', 'L', 'E', \
    ' ', 'R', 'e', 'm', 'o', 't', 'e'
#define CFG_ADV4_DATA_SCANRSP_PAYLOAD RC_LOCAL_NAME_MTK_RC

#else // CFG_ATVRC_WAKEUP
// ADV data payload
#define CFG_ADV1_DATA_ADV_PAYLOAD RC_HID_ADV_DAT_UUID, \
    RC_HID_ADV_DAT_APPEARANCE, RC_HID_ADV_DAT_MANU

// Scan response enabling
#define CFG_ADV1_DATA_SCANRSP_ENABLE 1

// Scan response payload
#define CFG_ADV1_DATA_SCANRSP_PAYLOAD RC_HID_ADV_DAT_UUID, RC_HID_ADV_DAT_APPEARANCE
#endif // CFG_ATVRC_WAKEUP
