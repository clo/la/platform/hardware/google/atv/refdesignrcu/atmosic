/**
 *******************************************************************************
 *
 * @file keyboard.h
 *
 * @brief keyboard driver
 *
 * Copyright (C) Atmosic 2018-2021
 *
 *******************************************************************************
 */

#pragma once

/**
 * @defgroup KEYBOARD KEYBOARD
 * @ingroup DRIVERS
 * @brief User driver for KSM module.
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

/// Key matrix event
typedef enum ksm_event_s {
    /// A key was released.
    KSM_RELEASE,
    /// A Key was pressed.
    KSM_PRESS,
    /// Hardware overflow error.
    KSM_ERR_HW_OVF
} ksm_event_t;

/**
 * @brief Callback function prototype
 * @param[in] event True if the key is pressed
 * @param[in] idx Key index which is used in KSM_RELEASE and KSM_PRESS event.
 * The index is Ri*Cn + Ci where Ci, Cn and Ri are column index, number of
 * columns and row index respectively.
 * @param[in] ctx Context from keyboard_run()
 */
typedef void (*keyboard_cb)(ksm_event_t event, uint32_t idx, void const *ctx);

/**
 * @brief Register callbacks and activate device.
 * The callback is called from SW event.
 * @param[in] callback Callback function. Called when a new keyboard
 * event occurred.
 * @param[in] ctx Context passed to callback
 */
__NONNULL(1)
void keyboard_run(keyboard_cb callback, void const *ctx);

#ifdef __cplusplus
}
#endif

/// @} KEYBOARD
