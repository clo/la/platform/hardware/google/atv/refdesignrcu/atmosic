/*******************************************************************************
 *
 * @file atm_pm.h
 *
 * @brief Power wakelock manager
 *
 * Copyright (C) Atmosic 2020
 *
 *******************************************************************************
 */

#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_DEV_PM Power managerment
 * @ingroup DRIVERS
 * @brief ATM power managerment driver
 *
 * This module contains the necessary functions to control the power modes
 *
 * @{
 *******************************************************************************
*/

#ifdef __cplusplus
extern "C" {
#endif

/// Lock type of low power mode
typedef enum {
    /// Sleep mode
    PM_LOCK_SLEEP,
    /// Retention mode
    PM_LOCK_RETENTION,
    /// Hibernation mode
    PM_LOCK_HIBERNATE,
    PM_LOCK_TYPE_MAX,
} pm_lock_type_e;

/// Lock identifier
typedef struct {
    /// Lock id
    uint32_t id;
    /// Lock type
    pm_lock_type_e type;
} pm_lock_id_t;

/**
 *******************************************************************************
 * @brief Allocate lock identifier with specified id
 * @param[in] id Identifier of specific lock and id.
 * @param[in] type Type of low power to be manipulated.
 *******************************************************************************
 */
bool atm_pm_realloc(pm_lock_id_t id, pm_lock_type_e type);

/**
 *******************************************************************************
 * @brief Allocate lock identifier.
 * @param[in] type Type of low power to be manipulated.
 * @return Identifier of specific lock.
 *******************************************************************************
 */
pm_lock_id_t atm_pm_alloc(pm_lock_type_e type);

/**
 *******************************************************************************
 * @brief Free allocated lock
 * @param[in] id Identifier of specific lock and id.
 *******************************************************************************
 */
void atm_pm_free(pm_lock_id_t id);

/**
 *******************************************************************************
 * @brief Lock specific power mode
 * @param[in] index Lock index
 *******************************************************************************
 */
void atm_pm_lock(pm_lock_id_t index);

/**
 *******************************************************************************
 * @brief Unlock specific power mode
 * @param[in] index Lock index
 *******************************************************************************
 */
void atm_pm_unlock(pm_lock_id_t index);

/**
 *******************************************************************************
 * @brief Print wakelocks
 *******************************************************************************
 */
void atm_pm_lock_info(void);

/**
 *******************************************************************************
 * @brief Set restart time from hibernate
 * @param[in] restart_time Hibernation time in centisec.
 *******************************************************************************
 */
void atm_pm_set_hib_restart_time(uint32_t restart_time);

/**
 *******************************************************************************
 * @brief Set function of atm_pm replacement vector of hibernate.
 * @param[in] cb Function of replacement vector.
 * @note The cb should be placed in RAM using __FAST.
 *******************************************************************************
 */
void atm_pm_set_hibernate_cb(rep_vec_fn__ret_bool__int32_t__uint32_t__t cb);

#ifdef __cplusplus
}
#endif

///@}

