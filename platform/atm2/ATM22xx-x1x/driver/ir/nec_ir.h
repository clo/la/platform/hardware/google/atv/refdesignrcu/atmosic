/**
 *******************************************************************************
 *
 * @file nec_ir.h
 *
 * @brief NEC IR driver interface
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */

#pragma once

/**
 * @defgroup NEC_IR NEC_IR
 * @ingroup DRIVERS
 * @brief Driver for NEC IR module
 * @{
 */

#include "ir_ctl.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Configure NEC IR instance.
 * @param[in] cb_end Callback to receive IR sequence completion event.
 */
__NONNULL_ALL
void nec_ir_init(void(*cb_end)(void));

/**
 * @brief Send IR address and cmd using NEC protocol.
 * @param[in] data IR data: address, inverse address, command, inverse command
 * @note If repeat is set, need to call @ref nec_ir_repeat_end to stop repeat
 * code.
 */
void nec_ir_send(ir_data_t *data, bool repeat);

#ifdef __cplusplus
}
#endif

/// @} NEC_IR