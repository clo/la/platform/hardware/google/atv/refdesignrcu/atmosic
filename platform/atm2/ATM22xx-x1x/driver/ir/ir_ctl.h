/**
 *******************************************************************************
 *
 * @file ir_ctl.h
 *
 * @brief IR control interface
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */

#pragma once

/**
 * @defgroup IR_CTL IR_CTL
 * @ingroup DRIVERS
 * @brief Driver for IR control
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    IR_MSG_TYPE_IR_CMD,
    IR_MSG_TYPE_IR_SEQ,
} ir_msg_type_t;

/// IR protocol definition
typedef struct {
    /// Name of IR protocol
    const char *name;
    /// IR Protocol Header ON duration in us
    uint32_t header_on_dur;
    /// IR Protocol Header OFF duration in us
    uint32_t header_off_dur;
    /// IR Protocol address bits
    uint8_t  addr_len;
    /// IR Protocol command bits
    uint8_t  cmd_len;
    /// IR Protocol logical one ON duration in us
    uint32_t one_on_dur;
    /// IR Protocol logical one OFF duration in us
    uint32_t one_off_dur;
    /// IR Protocol logical zero ON duration in us
    uint32_t zero_on_dur;
    /// IR Protocol logical zero OFF duration in us
    uint32_t zero_off_dur;
    /// IR Protocol message end duration in us
    uint32_t msg_end_dur;
    /// IR Protocol message end space in cs
    uint32_t msg_end_space;
    /// IR Protocol repeat ON duration in us
    uint32_t rept_on_dur;
    /// IR Protocol repeat OFF duration in us
    uint32_t rept_off_dur;
    /// IR Protocol repeat end ON duration in us
    uint32_t rept_end_on_dur;
    /// IR Protocol repeat delay in us
    uint32_t rept_delay;
} __PACKED ir_prot_t;

typedef struct {
    uint32_t addr;
    uint32_t inv_addr;
    uint32_t cmd;
    uint32_t inv_cmd;
} ir_data_t;

typedef struct {
    ir_prot_t const *prot;
    ir_data_t const *data;
    bool repeat;
} __PACKED ir_cmd_t;

typedef struct {
    uint16_t us_per_seq;
    uint16_t seq_len;
    uint16_t const *seq;
    uint16_t rept_len;
    uint16_t const *rept_seq;
} ir_seq_t;

typedef struct {
    ir_msg_type_t type;
    uint8_t duty;
    uint32_t freq;
    union {
	ir_cmd_t ir_cmd;
	ir_seq_t ir_seq;
    };
} ir_msg_t;

/**
 * @brief Initial IR control instance.
 * @param[in] cb_end Callback to receive IR sequence completion event.
 */
__NONNULL_ALL
void ir_ctl_init(void(*cb_end)(void));

/**
 * @brief Start transmiting IR signals
 * @param[in] msg IR message
 */
__NONNULL_ALL
void ir_ctl_start(ir_msg_t *msg);

/**
 * @brief Stop repeat IR sequence
 */
void ir_ctl_stop_rept(void);

/**
 * @brief Send IR data using NEC protocol.
 * @param[in] data IR address, inverse address, IR command, inverse command
 * @param[in] repeat true for repeating IR until ir_ctl_stop_rept() is invoked
 * @note If repeat is set, need to call @ref ir_ctl_stop_rept to stop repeat
 * code.
 */
__NONNULL(1)
void nec_ir_send(ir_data_t const *data, bool repeat);

#ifdef CFG_ATVRC_UNI_IR
/**
 * @brief Send Universal IR message.
 * @param[in] ir_code Universal IR code data
 * @param[in] size IR code data size
 */
__NONNULL(1)
void uni_ir_send(uint8_t const *code, uint16_t size);

/**
 * @brief Get repeat delay from Universal IR code data
 * @param[in] uni_ir_code Universal IR code data
 * @return repeat delay in us. return 0 if IR code type not match
 */
__NONNULL_ALL
uint32_t uni_ir_get_rept_delay(uint8_t const *uni_ir_code);
#endif

#ifdef __cplusplus
}
#endif

/// @} IR_CTL
