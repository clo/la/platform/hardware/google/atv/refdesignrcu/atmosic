/**
 *******************************************************************************
 *
 * @file ble_gattc.h
 *
 * @brief Header File - BLE GATTC API
 *
 * Copyright (C) Atmosic 2020-2023
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_GATTC GATTC API
 * @ingroup ATM_BTFM_API
 * @brief ATM bluetooth framework GATTC API
 *
 * This module contains the necessary API to deal with the RW GATTC messages.
 *
 * @{
 *******************************************************************************
 */

/*
 * INCLUDE
 *******************************************************************************
 */
#include "co_bt.h"
#include "gattc_task.h"
#include "ble_att.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * STRUCTURE DEFINITION
 *******************************************************************************
 */

/// The characteristic information of GATTC_DISC_ALL_CHAR
typedef struct {
    /// Element handle
    uint16_t attr_hdl;
    /// Pointer attribute handle to UUID
    uint16_t pointer_hdl;
    /// Properties
    uint8_t prop;
    /// UUID length
    uint8_t uuid_len;
    /// Characteristic UUID
    uint8_t uuid[ATT_UUID_128_LEN];
} ble_gattc_disc_all_char_ind_t;

/// The data of GATTC_DISC_ALL_CHAR
typedef struct {
    /// The maximum entries
    uint16_t max_num;
    /// Number of entries
    uint16_t num;
    /// Entry of one characteristic
    ble_gattc_disc_all_char_ind_t chars[__ARRAY_EMPTY];
} ble_gattc_disc_chars_t;

/// The characteristic descriptor information of GATTC_DISC_DESC_CHAR
typedef struct {
    /// Element handle
    uint16_t attr_hdl;
    /// UUID length
    uint8_t uuid_len;
    /// Descriptor UUID
    uint8_t uuid[ATT_UUID_128_LEN];
} ble_gattc_disc_char_desc_ind_t;

/// Data for GATTC_DISC_DESC_CHAR
typedef struct {
    /// The maximum entries
    uint16_t max_num;
    /// Number of entries
    uint16_t num;
    /// Entry of one characteristic descriptor
    ble_gattc_disc_char_desc_ind_t descs[__ARRAY_EMPTY];
} ble_gattc_disc_char_descs_t;

/// The service information of GATTC_DISC_ALL_SVC
typedef struct {
    /// Start handle
    uint16_t start_hdl;
    /// End handle
    uint16_t end_hdl;
    /// UUID length
    uint8_t  uuid_len;
    /// Service UUID
    uint8_t  uuid[ATT_UUID_128_LEN];
} ble_gattc_disc_all_svc_ind_t;

/// Data of GATTC_DISC_ALL_SVC
typedef struct {
    /// The maximum entries
    uint16_t max_num;
    /// Number of entries
    uint8_t num;
    /// Entry of one characteristic
    ble_gattc_disc_all_svc_ind_t svcs[__ARRAY_EMPTY];
} ble_gattc_disc_svcs_t;

/// GATT task message
typedef enum gattc_msg_id ble_gattc_msg_id_t;
/// Discovery all characteristics indication structure
typedef struct gattc_disc_char_ind ble_gattc_disc_char_ind_t;
/// Discover service indication structure
typedef struct gattc_disc_svc_ind ble_gattc_disc_svc_ind_t;

typedef struct {
    /// Attribute handle
    uint16_t handle;
    /// Read offset
    uint16_t offset;
    /// Read length
    uint16_t length;
    /// Handle value
    uint8_t value[__ARRAY_EMPTY];
} ble_gattc_read_ind_t;

/// GATT command complete event includes extra information
typedef struct {
    /// GATT request type
    uint8_t operation;
    /// Status of the request
    uint8_t status;
    /// Operation sequence number - provided when operation is started
    uint16_t seq_num;
    /// Extra data
    union {
	/// Data for GATTC_DISC_BY_UUID_CHAR
	ble_gattc_disc_char_ind_t disc_char;
	/// Data for GATTC_DISC_ALL_CHAR
	ble_gattc_disc_chars_t disc_chars;
	/// Data for GATTC_DISC_DESC_CHAR
	ble_gattc_disc_char_descs_t disc_descs;
	/// Data for GATTC_DISC_BY_UUID_SVC
	ble_gattc_disc_svc_ind_t disc_svc;
	/// Data for GATTC_DISC_ALL_SVC
	ble_gattc_disc_svcs_t disc_svcs;
	/// Data for ble_gatt_read API
	ble_gattc_read_ind_t read_ind;
    };
} ble_gattc_cmp_evt_ex_t;

/// The operate code to add or remove callback (@ref ble_gattc_cb_apply)
typedef enum {
    /// Add unsolicited message callback
    BLE_GATTC_CB_ADD,
    /// Remove unsolicited message callback
    BLE_GATTC_CB_REMOVE
} ble_gattc_cb_op_t;

/// The peer device triggers an event (notification)
typedef struct gattc_event_ind ble_gattc_event_ind_t;
/// Indicate that the ATT MTU has been updated (negotiated)
typedef struct gattc_mtu_changed_ind ble_gattc_mtu_changed_int_t;
/// The peer device triggers an event (indication)
typedef struct gattc_event_req_ind ble_gattc_event_req_ind_t;

/// GATTC unsolicited callbacks for application
typedef struct {
    /// MTU exchanged indication
    /// @brief This function will be called after MTU changed.
    /// @param[in] conidx  Connection task index.
    /// @param[in] mtu     MTU exchange information.
    void (*mtu_exchanged_ind)(uint8_t conidx, uint16_t mtu);
    /// GATTC event indication
    /// @brief This function will be called in GATTC event indication.
    /// @param[in] conidx  Connection task index.
    /// @param[in] ind     GATTC event data.
    void (*event_ind)(uint8_t conidx, ble_gattc_event_ind_t const *ind);
    /// GATTC event require indication
    /// @brief This function will be called in GATTC event require indication.
    /// @param[in] conidx  Connection task index.
    /// @param[in] ind     GATTC event require data.
    void (*event_req_ind)(uint8_t conidx, ble_gattc_event_req_ind_t const *ind);
    /// Service changed Configuration indication
    /// @brief This function will be called after CCCD written.
    /// @param[in] conidx   Connection task index.
    /// @param[in] ind_cfg  BLE_ATT_CCCD_IND: service changed indication is
    /// enabled, BLE_ATT_CCCD_STOP_IND: service changed indication is disabled.
    void (*svc_changed_cfg_ind)(uint8_t conidx, ble_att_cccd_val_t ind_cfg);
} ble_gattc_unsolicited_cbs_t;

/// The ID of callback function (@ref ble_gattc_cb_apply)
typedef struct ble_gattc_unsolicited_ctx_s const *ble_gattc_cb_id_t;

/*
 * FUNCTION DECLARATION
 *******************************************************************************
 */

/**
 *******************************************************************************
 * GATTC command complete callback prototype
 *
 * @param[in] conidx  Connection index
 * @param[in] parm    Parameter returned
 * @param[in] ctx     Context which provided from calling API
 *******************************************************************************
 */
typedef void (*ble_gattc_cmpl_cb_t)(uint8_t conidx,
    ble_gattc_cmp_evt_ex_t const *param, void const *ctx);

/**
 *******************************************************************************
 * @brief Retrieve the GATTC message handler
 *
 * @return GATTC message handler
 *******************************************************************************
 */
const struct ble_subtask_handlers *ble_gattc_handler_get(void);

/**
 *******************************************************************************
 * @brief GATTC initialization
 *******************************************************************************
 */
void ble_gattc_init(void);

/**
 *******************************************************************************
 * @brief Add or remove the GATTC unsolicited message callback
 *
 * @param[in] op Operate code (@ref ble_gattc_cb_op_t)
 * @param[in] cbs Callback of GATTC unsolicited message
 * @return the ID of GATTC callback function if op code is ble_gattc_cb_add;
 * otherwise, return NULL.
 *******************************************************************************
 */
__NONNULL(2)
ble_gattc_cb_id_t ble_gattc_cb_apply(ble_gattc_cb_op_t op,
    ble_gattc_unsolicited_cbs_t const *cbs);

/**
 *******************************************************************************
 * @brief Get gattc unsolicited callback
 *
 * @param[in] id The ID of GATTC callback function
 * @return GATTC callback function identify
 *******************************************************************************
 */
__NONNULL_ALL
ble_gattc_unsolicited_cbs_t const *ble_gattc_cb_get(ble_gattc_cb_id_t const id);

/**
 *******************************************************************************
 * @brief MTU exchange
 *
 * @param[in] conidx  Connection index
 * @param[in] cb      Command complete or indication callback
 *******************************************************************************
 */
void ble_gattc_mtu_exchange(uint8_t conidx, ble_gattc_cmpl_cb_t cb);

/**
 *******************************************************************************
 * @brief The specific characteristic discovery
 *
 * @param[in] conidx    Connection index
 * @param[in] uuid      UUID searched - LSB first
 * @param[in] uuid_len  UUID length (2, 4, or 16 bytes)
 * @param[in] cb        Command complete or indication callback
 * @param[in] ctx       Context data
 *******************************************************************************
 */
void ble_gattc_disc_svc(uint8_t conidx, uint8_t const *uuid, uint8_t uuid_len,
    ble_gattc_cmpl_cb_t cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Discover service by handle range
 *
 * @param[in] conidx  Connection index
 * @param[in] shdl     Start handle
 * @param[in] ehdl     End handle
 * @param[in] max_num The maximum services could be discovered
 * @param[in] cb      Command complete or indication callback
 * @param[in] ctx     Context data
 *******************************************************************************
 */
void ble_gattc_disc_svcs(uint8_t conidx, uint16_t shdl, uint16_t ehdl,
    uint16_t max_num, ble_gattc_cmpl_cb_t cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Characteristic discovery
 *
 * @param[in] conidx    Connection index
 * @param[in] uuid      UUID searched - LSB first
 * @param[in] uuid_len  UUID length (2, 4, or 16 bytes)
 * @param[in] cb        Command complete or indication callback
 * @param[in] ctx       Context data
 *******************************************************************************
 */
void ble_gattc_disc_char(uint8_t conidx, uint8_t const *uuid, uint8_t uuid_len,
    ble_gattc_cmpl_cb_t cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Discover all characteristics
 *
 * @param[in] conidx  Connection index
 * @param[in] shdl    Start handle
 * @param[in] ehdl    End handle
 * @param[in] cb      Command complete or indication callback
 * @param[in] max_num The maximum characteristics could be discovered
 * @param[in] ctx     Context data
 *******************************************************************************
 */
void ble_gattc_disc_chars(uint8_t conidx, uint16_t shdl, uint16_t ehdl,
    uint16_t max_num, ble_gattc_cmpl_cb_t cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Characteristic descriptor discovery
 *
 * @param[in] conidx    Connection index
 * @param[in] shdl      Start handle
 * @param[in] ehdl      End handle
 * @param[in] max_num   The maximum descriptors could be discovered
 * @param[in] cb        Command complete or indication callback
 * @param[in] ctx       Context data
 *******************************************************************************
 */
void ble_gattc_disc_desc_char(uint8_t conidx, uint16_t shdl, uint16_t ehdl,
    uint16_t max_num, ble_gattc_cmpl_cb_t cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Characteristic write
 *
 * @param[in] conidx  Connection index
 * @param[in] hdl     ATT handle
 * @param[in] dat     Data
 * @param[in] len     Data length
 * @param[in] cb      Command complete or indication callback
 * @param[in] ctx     Context data
 *******************************************************************************
 */
void ble_gattc_write(uint8_t conidx, uint16_t hdl, uint8_t const *dat,
    uint16_t len, ble_gattc_cmpl_cb_t cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Characteristic read
 *
 * @param[in] conidx  Connection index
 * @param[in] hdl     ATT handle
 * @param[in] offset Start offset in data payload
 * @param[in] len     Data length. (0 = read all)
 * @param[in] cb      Command complete or indication callback
 * @param[in] ctx     Context data
 *******************************************************************************
 */
void ble_gattc_read(uint8_t conidx, uint16_t hdl, uint16_t offset, uint16_t len,
    ble_gattc_cmpl_cb_t cb, void const *ctx);
/**
 *******************************************************************************
 * @brief Characteristic write without response
 *
 * @param[in] conidx  Connection index
 * @param[in] hdl     ATT handle
 * @param[in] dat     Data
 * @param[in] len     Data length
 * @param[in] cb      Command complete or indication callback
 * @param[in] ctx     Context data
 *******************************************************************************
 */
void ble_gattc_write_no_resp(uint8_t conidx, uint16_t hdl, uint8_t const *dat,
    uint16_t len, ble_gattc_cmpl_cb_t cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Characteristic read confirm
 *
 * @param[in] conidx  Connection index
 * @param[in] hdl     ATT handle
 * @param[in] dat     Data
 * @param[in] length  Data length
 *******************************************************************************
 */
void ble_gattc_read_cfm(uint8_t conidx, uint16_t hdl, uint8_t const *dat,
    uint16_t length);

/**
 *******************************************************************************
 * @brief Characteristic write confirm
 *
 * @param[in] conidx  Connection index
 * @param[in] hdl     ATT handle
 * @param[in] status  Status
 *******************************************************************************
 */
void ble_gattc_write_cfm(uint8_t conidx, uint16_t hdl, uint8_t status);

/**
 *******************************************************************************
 * @brief Characteristic notification
 *
 * @param[in] conidx    Connection index
 * @param[in] hdl       ATT handle
 * @param[in] dat       Data
 * @param[in] ntf_size  Data length
 * @param[in] sn        Sequence number
 * @param[in] cb        Command complete or indication callback
 * @param[in] ctx       Context data
 *******************************************************************************
 */
void ble_gattc_send_ntf(uint8_t conidx, uint16_t hdl, uint8_t const *dat,
    uint16_t ntf_size, uint16_t sn, ble_gattc_cmpl_cb_t cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Characteristic indication
 *
 * @param[in] conidx    Connection index
 * @param[in] hdl       ATT handle
 * @param[in] dat       Data
 * @param[in] ind_size  Data length
 * @param[in] sn        Seq number
 * @param[in] cb        Command complete or indication callback
 * @param[in] ctx       Context data
 *******************************************************************************
 */
void ble_gattc_send_ind(uint8_t conidx, uint16_t hdl, uint8_t const *dat,
    uint16_t ind_size, uint16_t sn, ble_gattc_cmpl_cb_t cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Characteristic att info confirm
 *
 * @param[in] conidx  Connection index
 * @param[in] hdl     ATT handle
 * @param[in] length  Length
 *******************************************************************************
 */
void ble_gattc_att_info_cfm(uint8_t conidx, uint16_t hdl, uint16_t length);

/**
 *******************************************************************************
 * @brief Characteristic att event requirement indication confirm
 *
 * @param[in] conidx  Connection index
 * @param[in] hdl     ATT handle
 *******************************************************************************
 */
void ble_gattc_event_req_ind_cfm(uint8_t conidx, uint16_t hdl);

/**
 *******************************************************************************
 * @brief Send service changed notification
 *
 * @param[in] conidx  Connection index
 * @param[in] start_hdl   Staring handle
 * @param[in] end_hdl     Ending handle
 * @param[in] cb          Callback function (NULL for no callback)
 *******************************************************************************
 */
void ble_gattc_svc_changed(uint8_t conidx, uint16_t start_hdl, uint16_t end_hdl,
    ble_gattc_cmpl_cb_t cb);

#ifdef __cplusplus
}
#endif

///@} ATM_BTFM_GATTC

