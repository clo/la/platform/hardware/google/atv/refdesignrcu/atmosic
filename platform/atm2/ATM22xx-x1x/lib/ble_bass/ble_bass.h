/**
 *******************************************************************************
 *
 * @file ble_bass.h
 *
 * @brief BASS API and data structures
 *
 * Copyright (C) Atmosic 2020-2022
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_BASS Battery service procedures
 * @ingroup ATM_BTFM_API
 * @brief BLE bluetooth battery service procedures
 *
 * This module contains the necessary procedure to send the battery level.
 *
 *
 * @{
 *******************************************************************************
 */
/*
 * INCLUDE FILES
 *******************************************************************************
 */
#include "ble_prf_itf.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * DEFINITION
 *******************************************************************************
 */
#define BLE_BASS_MODULE_NAME "kbass"
#define BATT_LVL_HALF 50 // percent
#define BATT_LVL_MAX 100 // percent

/*
 * STRUCTURES DEFINITION
 *******************************************************************************
 */
/// BASS feature
typedef enum {
    /// Support notification
    BLE_BASS_FEATURE_NTF = 1,
    /// Number of instance. MUST BE the final enumerator. Add new values above.
    BLE_BASS_NB_BAS_INSTANCES = 2
} ble_bass_feature_t;

typedef struct {
    /// Service security level
    uint8_t sec_lvl;
    /// Number of BAS to add
    uint8_t bas_nb;
    /// Features of each BAS instance. bit 0: support notification.
    /// bit 1 to 7 reserved.
    ble_bass_feature_t features[BLE_BASS_NB_BAS_INSTANCES];
    /// Callback to request update
    void (*cb_upd_req_ind)(uint8_t bas_instance);
} ble_bass_params_t;

typedef const ble_prf_cbs_t *(ble_bass_cbs_t)(ble_bass_params_t const *init);

/*
 * GLOBAL
 *******************************************************************************
 */

/**
 *******************************************************************************
 * @brief Send battery level information
 * @param[in] bas_instance Connection index
 * @param[in] pct_lvl Battery level percentage
 *******************************************************************************
 */
void ble_bass_send_lvl(uint8_t bas_instance, uint8_t pct_lvl);

#ifdef __cplusplus
}
#endif

/// @} ATM_BTFM_BASS
