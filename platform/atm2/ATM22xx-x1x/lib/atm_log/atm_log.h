/**
 *******************************************************************************
 *
 * @file atm_log.h
 *
 * @brief Atmosic Log Facility
 *
 * Copyright (C) Atmosic 2020
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_LOG LOG utility
 * @ingroup ATM_BTFM_PROC
 * @brief ATM bluetooth framework log utility
 *
 * This module contains the macro for log.
 *
 * @{
 *******************************************************************************
 */

/*
 * INCLUDES
 * *****************************************************************************
 */
#include "arch.h"

#ifdef RTT_DBG
#include "SEGGER_RTT.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*
 * MACRO
 *******************************************************************************
 */

/// string
#define D_(d) #d
#define STR(d) D_(d)

/// set default
#define ATM_LOG_DECOLOR "\x1B[0m"
/// set GG Red color
#define ATM_LOG_GRED "\x1B[91m"
/// set GG Green color
#define ATM_LOG_GGREEN "\x1B[92m"
/// set GG CYAN color
#define ATM_LOG_GCYAN "\x1B[96m"
/// set GG Blue color
#define ATM_LOG_GGBLUE "\x1B[94m"
/// set GG Brown color
#define ATM_LOG_GBROWN "\x1B[93m"
/// set GG White color
#define ATM_LOG_GWHITE "\x1B[97m"
/// set GG MAGENTA color
#define ATM_LOG_GMAGENTA "\x1B[95m"
/// set FG Red color
#define ATM_LOG_FRED "\x1B[31m"
/// set FG Green color
#define ATM_LOG_FGREEN "\x1B[32m"
/// set FG CYAN color
#define ATM_LOG_FCYAN "\x1B[36m"
/// set FG Blue color
#define ATM_LOG_FBLUE "\x1B[34m"
/// set FG Brown color
#define ATM_LOG_FBROWN "\x1B[33m"
/// set FG White color
#define ATM_LOG_FWHITE "\x1B[37m"
/// set FG MAGENTA color
#define ATM_LOG_FMAGENTA "\x1B[35m"

/// LOG ERROR mask
#define ATM_LOG_E_MASK ((1 << 1) - 1)
/// LOG WARNING mask
#define ATM_LOG_W_MASK ((1 << 2) - 1)
/// LOG DEBUG mask
#define ATM_LOG_D_MASK ((1 << 3) - 1)
/// LOG NOTIFY mask
#define ATM_LOG_N_MASK ((1 << 4) - 1)
/// LOG VERBOSE mask
#define ATM_LOG_V_MASK ((1 << 5) - 1)

#ifdef RTT_DBG
/// LOG ERROR color
#define ATM_LOG_E_C RTT_CTRL_TEXT_BRIGHT_RED
/// LOG WARNING color
#define ATM_LOG_W_C RTT_CTRL_TEXT_BRIGHT_MAGENTA
/// LOG NOTIFY color
#define ATM_LOG_N_C RTT_CTRL_TEXT_BRIGHT_BLUE
/// LOG DEBUG color
#define ATM_LOG_D_C RTT_CTRL_TEXT_BRIGHT_GREEN
/// LOG VERBOSE color
#define ATM_LOG_V_C RTT_CTRL_TEXT_BRIGHT_WHITE
#else
/// LOG ERROR color
#define ATM_LOG_E_C ATM_LOG_GRED
/// LOG WARNING color
#define ATM_LOG_W_C ATM_LOG_GMAGENTA
/// LOG NOTIFY color
#define ATM_LOG_N_C ATM_LOG_GGBLUE
/// LOG DEBUG color
#define ATM_LOG_D_C ATM_LOG_GGREEN
/// LOG VERBOSE color
#define ATM_LOG_V_C ATM_LOG_GWHITE
#endif

/// Global debug log level
/// @note user could use -DATMLOG_GLOBAL_LEVEL=\<lvl\> to disable log below the
/// \<lvl\>. The \<lvl\> would be ATM_LOG_[E|W|N|D|V]_MASK.
#ifndef ATM_LOG_GLOBAL_LEVEL
#define ATM_LOG_GLOBAL_LEVEL ATM_LOG_V_MASK
#endif

#if PLF_DEBUG
/// Module debug log setting
#define ATM_LOG_LOCAL_SETTING(name, level)\
static const char M_NAME[] = {name};\
static const uint32_t M_MASK = ATM_LOG_ ## level ## _MASK

/// Module debug log setting dynamic level
#define ATM_LOG_LOCAL_SETTING_DYN_LV(name, level)\
static char M_NAME[] = {name};\
static uint32_t M_MASK = ATM_LOG_ ## level ## _MASK

/// Module debug log
/// @note The module name(M_NAME) and module mask(M_MASK) needs to be defined
/// in source file. For example, #define M_NAME "adv". #define M_MASK V
#define ATM_LOG(MSK, fmt, ...) do {\
    if ((M_MASK & ATM_LOG_ ## MSK ## _MASK & ATM_LOG_GLOBAL_LEVEL) == \
	ATM_LOG_ ## MSK ## _MASK) {\
	DEBUG_TRACE("[%10.10s][" ATM_LOG_ ## MSK ## _C #MSK ATM_LOG_DECOLOR "]: " fmt ,\
	M_NAME,  ##__VA_ARGS__);\
    }} while(false)
#else
#define ATM_LOG_LOCAL_SETTING(name, level)
#define ATM_LOG_LOCAL_SETTING_DYN_LV(name, level)
#define ATM_LOG(MSK, fmt, ...) do {\
    DEBUG_TRACE_COND(0, fmt,  ##__VA_ARGS__);\
} while (false)
#endif //PLF_DEBUG

#ifdef AUTO_TEST
/// Negative unit test
#define ATM_LOG_NEGUT(fmt, ...) do { \
    DEBUG_TRACE("[NEGUT]: " fmt, ##__VA_ARGS__); \
} while (0)
#else
#define ATM_LOG_NEGUT(fmt, ...) do {\
    DEBUG_TRACE_COND(0, fmt,  ##__VA_ARGS__);\
} while (0)
#endif

#ifdef __cplusplus
}
#endif

///@} ATM_BTFM_LOG

