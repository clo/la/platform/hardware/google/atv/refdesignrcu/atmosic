/**
 *******************************************************************************
 *
 * @file atm_gap_param_internal.h
 *
 * @brief Header file - ATM bluetooth framework internal GAP parameters
 *
 * Copyright (C) Atmosic 2020-2022
 *
 *******************************************************************************
 */
#pragma once

#include "gapc_task.h"
#include "gapm.h"
#include "atm_gap.h"
#include "atm_gap_param.h"
#include "l2cc_pdu.h"

#ifdef __cplusplus
extern "C" {
#endif

// -------------- GAP Basic Configure -----------------------

/// device name setting
#ifndef CFG_GAP_DEV_NAME
#define CFG_GAP_DEV_NAME "Atmosic BT"
#endif
#ifndef CFG_GAP_DNAME_MAX_LEN
#define CFG_GAP_DNAME_MAX_LEN 18
#endif
#ifndef CFG_GAP_APP_IRK
#define CFG_GAP_APP_IRK NULL
#endif
/// Central, Peripheral, Observer, Broadcaster or All roles. (@see enum gap_role)
#ifndef CFG_GAP_ROLE
#define CFG_GAP_ROLE BLE_GAP_ROLE_PERIPHERAL
#endif
/// Appearance Icon
#ifndef CFG_GAP_APPEARANCE
// (@see atm_gap_appearance_t)
#define CFG_GAP_APPEARANCE ATM_GAP_APPEARANCE_UNKNOWN
#endif
#ifndef CFG_GAP_CONN_INT_MIN
#define CFG_GAP_CONN_INT_MIN 9 // in 1.25 ms
#endif
#ifndef CFG_GAP_CONN_INT_MAX
#define CFG_GAP_CONN_INT_MAX 9 // in 1.25 ms
#endif
#ifndef CFG_GAP_PERIPH_LATENCY
#define CFG_GAP_PERIPH_LATENCY 29 // in number of connection events
#endif
#ifndef CFG_GAP_CONN_TIMEOUT
#define CFG_GAP_CONN_TIMEOUT 500 // in unit of 10ms. Range: 100ms - 32s
#endif

// -------------- Privacy Config -----------------------
#ifndef CFG_RENEW_DURATION
#define CFG_RENEW_DURATION RPA_TO_DFT
#endif

#ifndef CFG_GAP_PRIVACY_CFG
#define CFG_GAP_PRIVACY_CFG 0
#endif
// -------------- Security Config -----------------------
/// Pairing mode authorized (@see enum gapm_pairing_mode)
#ifndef CFG_GAP_PAIRING_MODE
#define CFG_GAP_PAIRING_MODE BLE_GAP_PAIRING_LEGACY
#endif

// -------------- LE Data Length Extension --------------
#ifndef CFG_GAP_MAX_TX_OCTETS
#define CFG_GAP_MAX_TX_OCTETS BLE_MAX_OCTETS
#endif
#ifndef CFG_GAP_MAX_TX_TIME
#define CFG_GAP_MAX_TX_TIME LE_MAX_TIME
#endif

// --------------- L2CAP Configuration ------------------
#ifndef CFG_GAP_MAX_LL_MTU
#define CFG_GAP_MAX_LL_MTU 160
#endif
#ifndef CFG_GAP_MAX_LL_MPS
#define CFG_GAP_MAX_LL_MPS L2C_MIN_LE_MTUSIG
#endif

// -------------- ATT Database Configure ----------------
/// Attribute database configuration (@see enum gapm_att_cfg_flag)
#ifndef CFG_GAP_ATT_CFG
#define CFG_GAP_ATT_CFG 0
#endif

// --------Application assign static random address ------
// CFG_GAP_OWN_STATIC_RANDOM_ADDR5 must follow the spec. that
// bit 6 and bit 7 must be 1
#ifndef CFG_GAP_OWN_STATIC_RANDOM_ADDR5
#define CFG_GAP_OWN_STATIC_RANDOM_ADDR5 0x00
#endif

#ifndef CFG_GAP_OWN_STATIC_RANDOM_ADDR4
#define CFG_GAP_OWN_STATIC_RANDOM_ADDR4 0x00
#endif

#ifndef CFG_GAP_OWN_STATIC_RANDOM_ADDR3
#define CFG_GAP_OWN_STATIC_RANDOM_ADDR3 0x00
#endif

#ifndef CFG_GAP_OWN_STATIC_RANDOM_ADDR2
#define CFG_GAP_OWN_STATIC_RANDOM_ADDR2 0x00
#endif

#ifndef CFG_GAP_OWN_STATIC_RANDOM_ADDR1
#define CFG_GAP_OWN_STATIC_RANDOM_ADDR1 0x00
#endif

#ifndef CFG_GAP_OWN_STATIC_RANDOM_ADDR0
#define CFG_GAP_OWN_STATIC_RANDOM_ADDR0 0x00
#endif

#ifdef __cplusplus
}
#endif
