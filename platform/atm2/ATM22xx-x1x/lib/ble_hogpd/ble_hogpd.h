/**
 *******************************************************************************
 *
 * @file ble_hogpd.h
 *
 * @brief HID Over GATT Profile Device Middleware
 *
 * Copyright (C) Atmosic 2020-2022
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_HOGPD HID Over GATT Profile Device API
 * @ingroup ATM_BTFM_API
 * @brief ATM bluetooth framework HOGP API
 *
 * This module contains the necessary device API for HID over GATT profile
 *
 * @{
 *******************************************************************************
 */
#include <stdbool.h>
#include "ble_prf_itf.h"

#ifdef __cplusplus
extern "C" {
#endif

/// The module name to register to SDK framework
#define BLE_HOGPD_MODULE_NAME "khogpd"

/// Maximal number of HIDS
#define BLE_HOGPD_NB_HIDS_INST_MAX 2
/// Maximal number of Report Char.
#define BLE_HOGPD_NB_REPORT_INST_MAX 5

/// The states of the HID device
typedef enum {
    /// Device is disabled (Service not added in DB)
    BLE_HOGPD_DISABLED,
    /// Device is idle (Service added but profile not enabled)
    BLE_HOGPD_IDLE,
    /// Device is enabled (Device is connected and the profile is enabled)
    BLE_HOGPD_ENABLED,
    /// Device is ready (Device can sent the report)
    BLE_HOGPD_READY,
    /// Mark for last
    BLE_HOGPD_STATE_MAX,
} ble_hogpd_state_t;

/// Report Char. Configuration Flag Values
typedef enum {
    /// Input Report
    BLE_HOGPD_CFG_REPORT_IN = 0x01,
    /// Output Report
    BLE_HOGPD_CFG_REPORT_OUT = 0x02,
    /// HOGPD_CFG_REPORT_FEAT can be used as a mask to check Report type
    /// Feature Report
    BLE_HOGPD_CFG_REPORT_FEAT = 0x03,
    /// Input report with Write capabilities
    BLE_HOGPD_CFG_REPORT_WR = 0x10,
} ble_hogpd_report_cfg_t;

/// Features Flag Values
typedef enum {
    /// Keyboard Device
    BLE_HOGPD_CFG_KEYBOARD = 0x01,
    /// Mouse Device
    BLE_HOGPD_CFG_MOUSE = 0x02,
    /// Protocol Mode present
    BLE_HOGPD_CFG_PROTO_MODE = 0x04,
    /// Extended Reference Present
    BLE_HOGPD_CFG_MAP_EXT_REF = 0x08,
    /// Boot Keyboard Report write capability
    BLE_HOGPD_CFG_BOOT_KB_WR = 0x10,
    /// Boot Mouse Report write capability
    BLE_HOGPD_CFG_BOOT_MOUSE_WR = 0x20,
    /// Valid Feature mask
    BLE_HOGPD_CFG_MASK = 0x3F,
    /// Report Notification Enabled
    BLE_HOGPD_CFG_REPORT_NTF_EN = 0x40,
} ble_hogpd_cfg_t;

/// Type of reports
typedef enum {
    /// The Report characteristic
    BLE_HOGPD_REPORT,
    /// The Report Map characteristic
    BLE_HOGPD_REPORT_MAP,
    /// Boot Keyboard Input Report
    BLE_HOGPD_BOOT_KEYBOARD_INPUT_REPORT,
    /// Boot Keyboard Output Report
    BLE_HOGPD_BOOT_KEYBOARD_OUTPUT_REPORT,
    /// Boot Mouse Input Report
    BLE_HOGPD_BOOT_MOUSE_INPUT_REPORT,
} ble_hogpd_report_type_t;

/// HID Information bit values
typedef enum {
    /// Device capable of providing wake-up signal to a HID host
    BLE_HIDS_REMOTE_WAKE_CAPABLE = 0x01,
    /// Normally connectable support bit
    BLE_HIDS_NORM_CONNECTABLE = 0x02,
} ble_hogp_info_bit_t;

/// HID Control Point Characteristic value keys
enum ble_hogp_ctnl_pt {
    /// Suspend
    BLE_HOGP_CTNL_PT_SUSPEND,
    /// Exit suspend
    BLE_HOGP_CTNL_PT_EXIT_SUSPEND,
};

/// Protocol Mode Char. value Keys
enum ble_hogp_boot_prot_mode {
    /// Boot Protocol Mode
    BLE_HOGP_BOOT_PROTOCOL_MODE,
    /// Report Protocol Mode
    BLE_HOGP_REPORT_PROTOCOL_MODE,
};

/*
 * STRUCTURE DEFINITION
 *******************************************************************************
 */

/// HID Information structure
typedef struct {
    /// bcdHID
    uint16_t bcdHID;
    /// bCountryCode
    uint8_t bCountryCode;
    /// Flags
    ble_hogp_info_bit_t flags;
} ble_hids_hid_info_t;

/// External Report Reference
typedef struct {
    /// Included Service Handle
    uint16_t inc_svc_hdl;
    /// Characteristic UUID
    uint16_t rep_ref_uuid;
} ble_hogpd_ext_ref_t;

/// Database Creation Service Instance Configuration structure
typedef struct {
    /// Service Features
    ble_hogpd_cfg_t svc_features;
    /// Number of Report Char. instances to add in the database
    uint8_t report_nb;
    /// Report Char. Configuration
    ble_hogpd_report_cfg_t report_char_cfg[BLE_HOGPD_NB_REPORT_INST_MAX];
    /// Report id number
    uint8_t report_id[BLE_HOGPD_NB_REPORT_INST_MAX];
    /// HID Information Char. Values
    ble_hids_hid_info_t hid_info;
    /// External Report Reference
    ble_hogpd_ext_ref_t ext_ref;
} ble_hogpd_hids_cfg_t;

typedef struct {
    /// Number of HIDS to add
    uint8_t hids_nb;
    /// Initial configuration for each HIDS instance
    ble_hogpd_hids_cfg_t cfg[BLE_HOGPD_NB_HIDS_INST_MAX];
} ble_hogpd_db_cfg_t;

/// The ble_hogpd peer information
typedef struct {
    /// State of module
    ble_hogpd_state_t state;
    /// Notification CCC mask
    uint16_t ntf_mask;
} ble_hogpd_peer_info_t;

/// The ble_hogpd report info
typedef struct {
    /// Connection index
    uint8_t conidx;
    /// HIDS Instance
    uint8_t hid_idx;
    /// Report type
    ble_hogpd_report_type_t rpt_type;
    /// Report index
    uint8_t rpt_idx;
} ble_hogpd_report_info_t;

/// The ble_hogpd report
typedef struct {
    /// Report Info
    ble_hogpd_report_info_t info;
    /// Report length
    uint16_t rpt_len;
    /// Report data
    uint8_t const *data;
} ble_hogpd_report_t;

/// The ble_hogpd report read request
typedef struct {
    /// Report info
    ble_hogpd_report_info_t info;
    /// Token value that must be returned in confirmation
    uint16_t token;
    /// Data offset requested for read value
    uint16_t offset;
    /// Maximum data length is response value (starting from offset)
    uint16_t max_length;
} ble_hogpd_report_read_req_t;

/// The ble_hogpd report write request
typedef struct {
    /// Token value that must be returned in confirmation
    uint16_t token;
    /// Report info
    ble_hogpd_report_info_t info;
    /// Report length
    uint16_t rpt_len;
    /// Report data
    uint8_t const *data;
} ble_hogpd_report_write_req_t;

/// The ble_hogpd report read confirm
typedef struct {
    /// Report info
    ble_hogpd_report_info_t info;
    /// Status of read request. Success: BLE_ERR_NO_ERROR
    ble_err_code_t status;
    /// Token value
    uint16_t token;
    /// Report length
    uint16_t rpt_len;
    /// Report data
    uint8_t const *data;
} ble_hogpd_report_read_cfm_t;

/// The ble_hogpd report write confirm
typedef struct {
    /// Token value
    uint16_t token;
    /// Status of read request. Success: BLE_ERR_NO_ERROR
    ble_err_code_t status;
    /// Report Info
    ble_hogpd_report_info_t info;
} ble_hogpd_report_write_cfm_t;

typedef void (*ble_hogpd_ntf_ind_t)(uint8_t conidx, uint16_t rpt_idx_mask);
typedef void (*ble_hogpd_state_ind_t)(uint8_t conidx, ble_hogpd_state_t last);
typedef void (*ble_hogpd_report_read_req_cb_t)(
    ble_hogpd_report_read_req_t const *req);
typedef void (*ble_hogpd_report_write_req_cb_t)(
    ble_hogpd_report_write_req_t const *req);
typedef void (*ble_hogpd_report_cmp_cb_t)(uint8_t conidx, ble_err_code_t status,
    void const *ctx);

typedef struct {
    /// Number of report could be sent
    uint8_t nb_report;
    /// Database config
    ble_hogpd_db_cfg_t db_cfg;
    /// Report map
    uint8_t const *report_map[BLE_HOGPD_NB_HIDS_INST_MAX];
    /// Report map size
    uint16_t report_map_len[BLE_HOGPD_NB_HIDS_INST_MAX];
    /// The mask of cccd enable for HID ready
    uint16_t ccc_rdy_mask;
    /// Notification state changed
    ble_hogpd_state_ind_t state_ind;
    /// Report read request
    ble_hogpd_report_read_req_cb_t report_read_req;
    /// Report write request
    ble_hogpd_report_write_req_cb_t report_write_req;
    /// Service start handle
    /// 0: dynamically allocated in Attribute database
    uint16_t start_hdl;
    /// Number of peers supported
    uint8_t nb_peer;
} ble_hogpd_param_t;

typedef ble_prf_cbs_t const *(ble_hogpd_cbs_t)(ble_hogpd_param_t const *init);

/**
 *******************************************************************************
 * @brief Send HID report
 *
 * @param[in] rpt Report
 * @param[in] cmp_cb Report complete callback. NULL if the user does not want to
 * be notified after report event complete.
 * @param[in] ctx User defined context
 * @return Success: true
 *******************************************************************************
 */
__NONNULL(1)
bool ble_hogpd_send_report(ble_hogpd_report_t const *rpt,
    ble_hogpd_report_cmp_cb_t cmp_cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Send report read confirm
 *
 * @param[in] cfm Report read confirm
 *******************************************************************************
 */
__NONNULL_ALL
void ble_hogpd_send_report_read_cfm(ble_hogpd_report_read_cfm_t const *cfm);

/**
 *******************************************************************************
 * @brief Send report write confirm
 *
 * @param[in] cfm Report write confirm
 *******************************************************************************
 */
__NONNULL_ALL
void ble_hogpd_send_report_write_cfm(ble_hogpd_report_write_cfm_t const *cfm);

/**
 *******************************************************************************
 * @brief Claim report buffer.
 *
 * Application uses this function to claim a buffer from ble_hogpd layer and
 * calls ble_hogpd_report_send to send it out.
 *
 * @param[in] info Report Info
 * @param[in] rpt_len Report Length
 * @param[in] cmp_cb Report complete callback. NULL if the user does not want to
 * be notified after report event complete.
 * @param[in] ctx Application context.
 * @return HOGPD report buffer context.
 *******************************************************************************
 */
__NONNULL(1)
void *ble_hogpd_report_claim(ble_hogpd_report_info_t const *info,
    uint16_t rpt_len, ble_hogpd_report_cmp_cb_t cmp_cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Get report data payload
 *
 * @param[in] rpt_buf_ctx HOGPD report buffer context
 * @return Pointer of the report payload
 *******************************************************************************
 */
__NONNULL_ALL
uint8_t *ble_hogpd_report_get_report_payload(void *rpt_buf_ctx);

/**
 *******************************************************************************
 * @brief Get report length from the report buffer
 *
 * @param[in] rpt_buf_ctx HOGPD report buffer context
 * @return Report length
 *******************************************************************************
 */
__NONNULL_ALL
uint16_t ble_hogpd_report_get_report_length(void const *rpt_buf_ctx);

/**
 *******************************************************************************
 * @brief Set report length to the report buffer
 *
 * @param[in] rpt_buf_ctx HOGPD report buffer context
 * @param[in] new_len Report length. Must not longer than the claimed length
 *******************************************************************************
 */
__NONNULL(1)
void ble_hogpd_report_set_report_length(void *rpt_buf_ctx, uint16_t new_len);

/**
 *******************************************************************************
 * @brief Send report to peer.
 *
 * @param[in] rpt_buf_ctx HOGPD report buffer context
 *******************************************************************************
 */
__NONNULL_ALL
void ble_hogpd_report_send(void const *rpt_buf_ctx);

/**
 *******************************************************************************
 * @brief Get module information
 *
 * @return Current ble_hogpd peer information
 *******************************************************************************
 */
ble_hogpd_peer_info_t const *ble_hogpd_get_peer_info(void);

#ifdef __cplusplus
}
#endif

///@} ATM_BTFM_HOGPD

