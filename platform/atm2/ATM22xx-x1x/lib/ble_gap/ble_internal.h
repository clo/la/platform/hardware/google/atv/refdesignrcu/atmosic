/**
 *******************************************************************************
 *
 * @file ble_internal.h
 *
 * @brief Middleware GAP Internal function
 *
 * Copyright (C) Atmosic 2020-2022
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_BLE_INT GAP internal API
 * @ingroup ATM_BTFM_API
 * @brief ATM bluetooth framework GAP internal API
 *
 * This module contains the necessary API to deal with the GAP messages.
 *
 * @{
 *******************************************************************************
 */

#include "ble_task.h"
#include "ble_gap.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Internal C API
 *******************************************************************************
 */

// BLE GAP Security environment structure
struct ble_gap_sec_env_tag {
    uint16_t bonded_mask;
    int8_t last_bonding;
    uint8_t current_authreq;
    ble_gap_bdaddr_t peer_addr;
    struct gapc_irk peer_irk;
    ble_gap_ltk_t ltk;
    ble_gap_ltk_t peer_ltk;
    bool peer_irk_valid;
    bool loc_irk_valid;
    uint8_t loc_irk[BLE_GAP_KEY_LEN];
};

// BLE gapm profile task add extended parameters
typedef struct {
    // service start handle
    // 0 - Dynamic allocation
    uint16_t start_hdl;
} ble_gapm_task_add_ex_t;

/**
 *******************************************************************************
 * @brief GAP API initialization
 *******************************************************************************
 */
void ble_gap_init(void);
/**
 *******************************************************************************
 * @brief Reset controller
 * After reset controller, user needs to @see ble_gapm_set_device_config again.
 * @param[in] cb Complete callback function.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
void ble_gapm_reset_cmd(ble_gapm_cmd_cb cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Create and add profile task
 *
 * @param[in] taskid Task identifier.
 * @param[in] seclvl Security Level :
 *  7    6    5    4    3    2    1    0
 * +----+----+----+----+----+----+----+----+
 * |   Reserved   |DIS |  AUTH   |EKS | MI |
 * +----+----+----+----+----+----+----+----+
 *
 * - MI: 1 - Application task is a Multi-Instantiated task, 0 - Mono-Instantiated
 * Only applies for service - Ignored by collectors:
 * - EKS: Service needs a 16 bytes encryption key
 * - AUTH: 0 - Disable, 1 - Enable, 2 - Unauth, 3 - Auth
 * - DIS: Disable the service
 * @param[in] size Size of allocated profile configuration data. The storage
 * will be passed to @p cbadd callback.
 * @param[in] cbadd Profile added callback function.
 * @param[in] cb Complete callback function.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
void ble_gapm_profile_task_add(uint16_t taskid, uint8_t seclvl, uint16_t size,
    gapm_profile_add_cb cbadd, ble_gapm_cmd_cb cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Create and add profile task with extended parameters
 *
 * @param[in] taskid Task identifier.
 * @param[in] seclvl Security Level :
 *  7    6    5    4    3    2    1    0
 * +----+----+----+----+----+----+----+----+
 * |   Reserved   |DIS |  AUTH   |EKS | MI |
 * +----+----+----+----+----+----+----+----+
 *
 * - MI: 1 - Application task is a Multi-Instantiated task,
 *   0 - Mono-Instantiated Only applies for service - Ignored by collectors:
 * - EKS: Service needs a 16 bytes encryption key
 * - AUTH: 0 - Disable, 1 - Enable, 2 - Unauth, 3 - Auth
 * - DIS: Disable the service
 * @param[in] size Size of allocated profile configuration data. The storage
 * will be passed to @p cbadd callback.
 * @param[in] cbadd Profile added callback function.
 * @param[in] cb Complete callback function.
 * @param[in] ctx Context data.
 * @param[in] ext Extended parameters
 *******************************************************************************
 */
void ble_gapm_profile_task_add_ex(uint16_t taskid, uint8_t seclvl,
    uint16_t size, gapm_profile_add_cb cbadd, ble_gapm_cmd_cb cb,
    void const *ctx, ble_gapm_task_add_ex_t const *ext);

/**
 *******************************************************************************
 * @brief Register GAPC message callback
 * @param[in] cb Callback of GAPC message
 *******************************************************************************
 */
void ble_gapc_reg_ind(gapc_ind cb);

/**
 *******************************************************************************
 * @brief Register GAPM message callback
 * @param[in] cb Callback of GAPM message
 *******************************************************************************
 */
void ble_gapm_reg_ind(gapm_ind cb);

/**
 *******************************************************************************
 * @brief Register GAPC message callback
 * @param[in] cb Callback of GAPC security messages (GAPC_BOND_REQ_IND,
 * GAPC_BOND_IND and GAPC_ENCRYPT_REQ_IND).
 *******************************************************************************
 */
void ble_gapc_sec_reg_ind(ke_msg_func_t cb);

/**
 *******************************************************************************
 * @brief Initialization
 * @return Handler of messages. (GAPC_BOND_REQ_IND, GAPC_BOND_IND and
 * GAPC_ENCRYPT_REQ_IND).
 *******************************************************************************
 */
ke_msg_func_t ble_gap_sec_init(void);

/**
 *******************************************************************************
 * @brief Get gapm handler
 *******************************************************************************
 */
gapm_ind ble_gapm_cb_get(void);

/**
 *******************************************************************************
 * @brief Get gapc handler
 *******************************************************************************
 */
gapc_ind ble_gapc_cb_get(void);

/**
 *******************************************************************************
 * @brief Add gapc command callback
 * @param[in] cb Command complete callback.
 * @param[in] op  GAPC operation code
 * @param[in] ctx Context data.
 *******************************************************************************
 */
void ble_gapc_add_cb(gapc_cmpl_cb cb, uint8_t op, void const *ctx);

/**
 *******************************************************************************
 * @brief Add gapc command callback
 * @param[in] cb Command complete callback.
 * @param[in] op  GAPC operation code.
 * @param[in] ctx Context data.
 * @param[in] evt Additinal information needed in command complete.
 *******************************************************************************
 */
void ble_gapc_push_cb(ble_gapc_cmd_cb cb, uint8_t op, void const *ctx,
    ble_gapc_cmpl_t *evt);

/**
 *******************************************************************************
 * @brief Allocate a memory of ble_gapc_cmpl_t with extra data length
 * @param[in] extra_data_len The extra data length.
 * @return Allocated memory pointer with extra data length.
 *******************************************************************************
 */
ble_gapc_cmpl_t *ble_gapc_add_cb_evt(uint32_t extra_data_len);

/**
 *******************************************************************************
 * @brief Call a callback event
 * @param[in] id Event id.
 * @param[in] evt Event.
 *******************************************************************************
 */
void ble_gap_evt_cb_call(ble_gap_eid_t id, ble_gap_evt_t *evt);

#ifdef __cplusplus
}
#endif

/// @} ATM_BTFM_BLE_INT
