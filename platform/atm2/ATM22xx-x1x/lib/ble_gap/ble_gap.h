/**
 *******************************************************************************
 *
 * @file ble_gap.h
 *
 * @brief Header file - BLE GAP API.
 *
 * Copyright (C) Atmosic 2020-2023
 *
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_BLE_GAP GAP API
 * @ingroup ATM_BTFM_API
 * @brief ATM bluetooth framework GAP API
 *
 * This module contains the necessary API to deal with the GAP messages.
 *
 * @{
 *******************************************************************************
 */


/*
 * INCLUDE FILES
 *******************************************************************************
 */

#include "co_bt.h"     // Common BT Definitions
#include "gap.h"       // GAP Definition
#include "gapm.h"
#include "gapc.h"
#include "ble_common.h"
#include "ble_err.h"

#ifdef __cplusplus
extern "C" {
#endif

/// Key length
#define BLE_GAP_KEY_LEN 16
/// Random number number
#define BLE_GAP_RAND_NB_LEN 8
/// Maximum test TX power level
#define BLE_GAP_MAX_TEST_TX_PWR_LVL 0x7F
/// Minimum switching pattern length
#define BLE_GAP_MIN_SWITCHING_PATTERN_LEN 2
/// Maximum switching pattern length
#define BLE_GAP_MAX_SWITCHING_PATTERN_LEN 0x4B
/// LE Feature Flags Length
#define BLE_GAP_LE_FEATS_LEN 8
/// LE Channel map length
#define BLE_GAP_LE_CHNL_MAP_LEN 5
/// Scanning window (in 625us slot)
#define BLE_SCAN_WINDOW_DFT 0x0010
/// Scanning interval (in 625us slot)
#define BLE_SCAN_INTERVAL_DFT 0x0010
/// Database hash length
#define BLE_GAP_DATABASE_HASH_LEN 16

/*
 * TYPEDEFINE
 *******************************************************************************
 */

/// Random Address type
typedef enum {
    /// Static random address
    BLE_GAP_STATIC_ADDR = 0xc0,
    /// Private non-resolvable address
    BLE_GAP_NON_RSLV_ADDR = 0x00,
    /// Private resolvable address
    BLE_GAP_RSLV_ADDR = 0x40,
} ble_gapm_rand_addr_type_t;

/// BLE ADV data type
typedef enum {
    /// Advertising data
    BLE_GAPM_ADV_DATA,
    /// Scan response data
    BLE_GAPM_SCAN_RSP_DATA,
    /// Periodic advertising data
    BLE_GAPM_PERIOD_ADV_DATA
} ble_gapm_data_type_t;

typedef enum {
    /// Extended advertising report
    BLE_GAP_REPORT_TYPE_ADV_EXT,
    /// Legacy advertising report
    BLE_GAP_REPORT_TYPE_ADV_LEG,
    /// Extended scan response report
    BLE_GAP_REPORT_TYPE_SCAN_RSP_EXT,
    /// Legacy scan response report
    BLE_GAP_REPORT_TYPE_SCAN_RSP_LEG,
    /// Periodic advertising report
    BLE_GAP_REPORT_TYPE_PER_ADV,
} ble_gap_adv_report_type_t;

typedef enum {
    /// Report Type (see ble_gap_adv_report_type_t)
    BLE_GAP_REPORT_INFO_REPORT_TYPE_MASK = 0x07,
    /// Report is complete
    BLE_GAP_REPORT_INFO_COMPLETE_BIT = (1 << 3),
    /// Connectable advertising
    BLE_GAP_REPORT_INFO_CONN_ADV_BIT = (1 << 4),
    /// Scannable advertising
    BLE_GAP_REPORT_INFO_SCAN_ADV_BIT = (1 << 5),
    /// Directed advertising
    BLE_GAP_REPORT_INFO_DIR_ADV_BIT = (1 << 6),
} ble_gap_adv_report_info_t;

/// Packet payload type for test mode
typedef enum {
    /// PRBS9 sequence "11111111100000111101..." (in transmission order)
    BLE_GAP_PKT_PLD_PRBS9,
    /// Repeated "11110000" (in transmission order)
    BLE_GAP_PKT_PLD_REPEATED_11110000,
    /// Repeated "10101010" (in transmission order)
    BLE_GAP_PKT_PLD_REPEATED_10101010,
    /// PRBS15 sequence
    BLE_GAP_PKT_PLD_PRBS15,
    /// Repeated "11111111" (in transmission order) sequence
    BLE_GAP_PKT_PLD_REPEATED_11111111,
    /// Repeated "00000000" (in transmission order) sequence
    BLE_GAP_PKT_PLD_REPEATED_00000000,
    /// Repeated "00001111" (in transmission order) sequence
    BLE_GAP_PKT_PLD_REPEATED_00001111,
    /// Repeated "01010101" (in transmission order) sequence
    BLE_GAP_PKT_PLD_REPEATED_01010101,
} ble_gap_pkt_pld_type_t;

/// Enumeration of TX PHY values
typedef enum {
    /// LE 1M PHY
    BLE_GAP_TX_PHY_1MBPS = 1,
    /// LE 2M PHY
    BLE_GAP_TX_PHY_2MBPS = 2,
    /// LE Coded PHY with S=8 data coding
    BLE_GAP_TX_PHY_125KBPS = 3,
    /// LE Coded PHY with S=2 data coding
    BLE_GAP_TX_PHY_500KBPS = 4,
} ble_gap_tx_phy_val_t;

/// Enumeration of RX PHY values
typedef enum {
    /// LE 1M PHY
    BLE_GAP_RX_PHY_1MBPS = 1,
    /// LE 2M PHY
    BLE_GAP_RX_PHY_2MBPS = 2,
    /// LE Coded PHY
    BLE_GAP_RX_PHY_CODED = 3,
} ble_gap_rx_phy_val_t;

/// Enumeration of PHY values
typedef enum {
    /// LE 1M PHY
    BLE_GAP_PHY_1MBPS = 1,
    /// LE 2M PHY
    BLE_GAP_PHY_2MBPS = 2,
    /// LE Coded PHY
    BLE_GAP_PHY_CODED = 3,
} ble_gap_phy_val_t;

/// Constant tone extension type
typedef enum {
    /// AoA constant tone extension
    BLE_GAP_CTE_AOA,
    /// AoD constant tone extension with 1 us slots
    BLE_GAP_CTE_AOD_1US,
    /// AoD constant tone extension with 2 us slots
    BLE_GAP_CTE_AOD_2US,
} ble_gap_cte_type_t;

/// LE transmitter test parameters
typedef struct {
    /// TX channel (range: 0x00 to 0x27)
    uint8_t ch;
    /// Length in bytes of payload data in each packet
    uint8_t test_data_len;
    /// Packet payload type
    ble_gap_pkt_pld_type_t pkt_pld_type;
    /// PHY
    ble_gap_tx_phy_val_t phy;
    /// CTE length
    uint8_t cte_len;
    /// CTE type
    ble_gap_cte_type_t cte_type;
    /// Transmit power level in dBm (0x7E: minimum | 0x7F: maximum | range: -127
    /// to +20)
    int8_t tx_pwr_lvl;
    /// The number of antenna IDs in the pattern (range: 0x02 to 0x4B)
    uint8_t switching_pattern_len;
    /// Antenna IDs
    uint8_t antenna_id[__ARRAY_EMPTY];
} ble_gap_test_tx_param_t;

/// Modulation index
typedef enum {
    /// Assume transmitter will have a standard modulation index
    BLE_GAP_MODULATION_STANDARD,
    /// Assume transmitter will have a stable modulation index
    BLE_GAP_MODULATION_STABLE,
} ble_gap_mod_idx_t;

/// Slot durations
typedef enum {
    /// Switching and sampling slots are 1 μs each
    BLE_GAP_SLOT_DUR_1US = 1,
    /// Switching and sampling slots are 2 μs each
    BLE_GAP_SLOT_DUR_2US,
} ble_gap_slot_dur_t;

/// LE receiver test parameters
typedef struct {
    /// RX channel (range: 0x00 to 0x27)
    uint8_t ch;
    /// PHY
    ble_gap_rx_phy_val_t phy;
    /// Modulation index
    ble_gap_mod_idx_t mod_idx;
    /// Expected CTE length
    uint8_t exp_cte_len;
    /// Expected CTE type
    ble_gap_cte_type_t exp_cte_type;
    /// Slot durations
    ble_gap_slot_dur_t slot_dur;
    /// The number of antenna IDs in the pattern (range: 0x02 to 0x4B)
    uint8_t switching_pattern_len;
    /// Antenna IDs
    uint8_t antenna_id[__ARRAY_EMPTY];
} ble_gap_test_rx_param_t;

/// BLE ADV channel selection
typedef enum {
    /// Channel 37
    ADV_37_CHNL = (1 << 0),
    /// Channel 38
    ADV_38_CHNL = (1 << 1),
    /// Channel 39
    ADV_39_CHNL = (1 << 2),
    /// Channel 37, 38 and 39
    ADV_ALL_CHNLS = ADV_37_CHNL | ADV_38_CHNL | ADV_39_CHNL
} ble_adv_chnl_t;

/// Advertising properties bit field bit value
typedef enum {
    /// Indicate that advertising is connectable, reception of
    /// CONNECT_IND or AUX_CONNECT_REQ PDUs is accepted. Not applicable
    /// for periodic advertising.
    ADV_PROP_CONNECTABLE_BIT = (1 << 0),
    /// Indicate that advertising is scannable, reception of SCAN_REQ or
    /// AUX_SCAN_REQ PDUs is accepted
    ADV_PROP_SCANNABLE_BIT = (1 << 1),
    /// Indicate that advertising targets a specific device. Only apply
    /// in
    /// following cases:
    ///   - Legacy advertising: if connectable
    ///   - Extended advertising: connectable or (non connectable and
    /// non discoverable)
    ADV_PROP_DIRECTED_BIT = (1 << 2),
    /// Indicate that High Duty Cycle has to be used for advertising on
    /// primary channel Apply only if created advertising is not an
    /// extended advertising
    ADV_PROP_HDC_BIT = (1 << 3),
    /// Reserved for bit 4
    ADV_PROP_RESERVED_4_BIT = (1 << 4),
    /// Enable anonymous mode. Device address won't appear in send PDUs
    /// Valid only if created advertising is an extended advertising
    ADV_PROP_ANONYMOUS_BIT = (1 << 5),
    /// Include TX Power in the extended header of the advertising PDU.
    /// Valid only if created advertising is not a legacy advertising
    ADV_PROP_TX_PWR_BIT = (1 << 6),
    /// Include TX Power in the periodic advertising PDU.
    /// Valid only if created advertising is a periodic advertising
    ADV_PROP_PER_TX_PWR_BIT = (1 << 7),
    /// Indicate if application must be informed about received scan
    /// requests PDUs
    ADV_PROP_SCAN_REQ_NTF_EN_BIT = (1 << 8),
} ble_adv_prop_t;

/// Leggacy advertising properties. Used when type is ADV_TYPE_LEGACY
enum adv_crt_leg_prop {
    /// Non connectable non scannable advertising
    ADV_LEGACY_NON_CONN_NON_SCAN_MASK,
    /// Broadcast non scannable advertising - Discovery mode must be
    /// Non Discoverable
    ADV_LEGACY_BROADCAST_NON_SCAN_MASK =
	ADV_LEGACY_NON_CONN_NON_SCAN_MASK,
    /// Non connectable scannable advertising
    ADV_LEGACY_NON_CONN_SCAN_MASK = ADV_PROP_SCANNABLE_BIT,
    /// Broadcast non scannable advertising - Discovery mode must be Non
    /// Discoverable
    ADV_LEGACY_BROADCAST_SCAN_MASK = ADV_LEGACY_NON_CONN_SCAN_MASK,
    /// Undirected connectable advertising
    ADV_LEGACY_UNDIR_CONN_MASK = ADV_PROP_CONNECTABLE_BIT |
	ADV_PROP_SCANNABLE_BIT,
    /// Directed connectable advertising
    ADV_LEGACY_DIR_CONN_MASK = ADV_PROP_DIRECTED_BIT |
	ADV_PROP_CONNECTABLE_BIT,
    /// Directed connectable with Low Duty Cycle
    ADV_LEGACY_DIR_CONN_LDC_MASK = ADV_LEGACY_DIR_CONN_MASK,
    /// Directed connectable with High Duty Cycle
    ADV_LEGACY_DIR_CONN_HDC_MASK = ADV_LEGACY_DIR_CONN_MASK |
	ADV_PROP_HDC_BIT,
};

/// Leggacy advertising properties. Used when type is ADV_TYPE_EXTENDED
enum adv_crt_ext_prop {
    /// Non connectable non scannable extended advertising
    ADV_EXT_NON_CONN_NON_SCAN_MASK,
    /// Non connectable scannable extended advertising
    ADV_EXT_NON_CONN_SCAN_MASK = ADV_PROP_SCANNABLE_BIT,
    /// Non connectable scannable directed extended advertising
    ADV_EXT_NON_CONN_SCAN_DIR_MASK = ADV_PROP_SCANNABLE_BIT |
	ADV_PROP_DIRECTED_BIT,
    /// Non connectable anonymous directed extended advertising
    ADV_EXT_ANONYM_DIR_MASK = ADV_PROP_ANONYMOUS_BIT |
	ADV_PROP_DIRECTED_BIT,
    /// Undirected connectable extended advertising
    ADV_EXT_UNDIR_CONN_MASK = ADV_PROP_CONNECTABLE_BIT,
    /// Directed connectable extended advertising
    ADV_EXT_DIR_CONN_MASK = ADV_PROP_CONNECTABLE_BIT |
	ADV_PROP_DIRECTED_BIT,
};

/// Periodic advertising properties. Used when type is ADV_TYPE_PERIODIC
enum adv_crt_per_prop {
    /// Undirected periodic advertising
    ADV_PER_UNDIR_MASK,
    /// Directed periodic advertising
    ADV_PER_DIR_MASK = ADV_PROP_DIRECTED_BIT,
};

/// Advertising parameters for advertising creation
typedef struct {
    /// Advertising type
    enum adv_crt_type {
	/// Legacy advertising
	ADV_TYPE_LEGACY,
	/// Extended advertising
	ADV_TYPE_EXTENDED,
	/// Periodic advertising
	ADV_TYPE_PERIODIC,
    } type;

    /// Advertising discovery mode
    enum adv_crt_mode {
	/// Mode in non-discov rable
	ADV_MODE_NON_DISC,
	/// Mode in general discoverable
	ADV_MODE_GEN_DISC,
	/// Mode in limited discoverable
	ADV_MODE_LIM_DISC,
	/// Broadcast mode without presence of AD_TYPE_FLAG in advertising data
	ADV_MODE_BEACON,
	/// Number of mode
	ADV_MODE_MAX,
    } disc_mode;

    /// Advertising property
    /// This may come from OR operation of values of ble_adv_prop_t
    /// with one of value from @see enum adv_crt_leg_prop,
    /// @see enum adv_crt_ext_prop or @see enum adv_crt_per_prop.
    uint16_t prop;

    /// Maximum power level at which the advertising packets have to be
    /// transmitted (between -127 and 126 dBm)
    int8_t max_tx_pwr;
    /// Advertising filter policy
    enum adv_crt_filter_policy {
	/// Allow both scan and connection requests from anyone
	FILTER_SCAN_ANY_CON_ANY,
	/// Allow both scan req from White List devices only and connection req
	/// from anyone
	FILTER_SCAN_WLST_CON_ANY,
	/// Allow both scan req from anyone and connection req from White List
	/// devices only
	FILTER_SCAN_ANY_CON_WLST,
	/// Allow scan and connection requests from White List devices only
	FILTER_SCAN_WLST_CON_WLST,
    } filter_pol;
    /// Peer address configuration (only used in case of directed advertising)
    ble_gap_bdaddr_t peer_addr;
    /// Configuration for advertising on primary channel
    struct primary_config {
	/// Minimum advertising interval (in unit of 625us). Must be greater
	/// than 20ms
	uint32_t adv_intv_min;
	/// Maximum advertising interval (in unit of 625us). Must be greater
	/// than 20ms
	uint32_t adv_intv_max;
	/// Bit field indicating the channel mapping. Bit number 0/1/2 is
	/// channel number 37/38/39.
	ble_adv_chnl_t chnl_map;
	/// Note that LE 2M PHY is not allowed and that legacy advertising only
	/// support LE 1M PHY
	ble_gap_phy_val_t phy;
    } prim_cfg;
    /// Configuration for secondary advertising (valid only if advertising type
    /// is ADV_TYPE_EXTENDED or ADV_TYPE_PERIODIC)
    struct second_config {
	/// Maximum number of advertising events the controller can skip before
	/// sending the
	/// AUX_ADV_IND packets. 0 means that AUX_ADV_IND PDUs shall be sent
	/// prior each
	/// advertising events
	uint8_t max_skip;
	/// Indicate on which PHY secondary advertising has to be performed
	ble_gap_phy_val_t phy;
	/// Advertising SID
	uint8_t adv_sid;
    } second_cfg;
    /// Configuration for periodic advertising (valid only if advertising type
    /// is ADV_TYPE_PERIODIC)
    struct periodic_config {
	/// Minimum advertising interval (in unit of 1.25ms). Must be greater
	/// than 20ms
	uint16_t adv_intv_min;
	/// Maximum advertising interval (in unit of 1.25ms). Must be greater
	/// than 20ms
	uint16_t adv_intv_max;
    } period_cfg;
} ble_gapm_adv_create_param_t;

/// Generic key structure
typedef struct {
    /// Key value MSB -> LSB
    uint8_t key[BLE_GAP_KEY_LEN];
} ble_gap_key_t;

typedef struct {
    /// Identity Resolving Key
    ble_gap_key_t irk;
    /// Device BD Identity Address
    ble_gap_bdaddr_t addr;
} ble_gap_irk_t;

/// Bit field use to select the preferred TX or RX LE PHY. 0 means no
/// preferences.
typedef enum {
    /// No preferred PHY.
    BLE_GAP_PHY_ANY = 0x00,
    /// LE 1M PHY preferred for an active link.
    BLE_GAP_PHY_LE_1MBPS = (1 << 0),
    /// LE 2M PHY preferred for an active link.
    BLE_GAP_PHY_LE_2MBPS = (1 << 1),
    /// LE Coded PHY preferred for an active link.
    BLE_GAP_PHY_LE_CODED = (1 << 2),
} ble_gap_phy_mode_t;

/// Specify what rate Host prefers to use in transmission on coded PHY. refer to
/// HCI: 7.8.49
typedef enum {
    /// No preferred coding when transmitting on the LE Coded PHY.
    BLE_GAP_PHY_OPT_NO_LE_CODED_TX_PREF,
    /// Prefers that S=2 coding be used when transmitting on the LE Coded PHY.
    BLE_GAP_PHY_OPT_S2_LE_CODED_TX_PREF,
    /// Prefers that S=8 coding be used when transmitting on the LE Coded PHY.
    BLE_GAP_PHY_OPT_S8_LE_CODED_TX_PREF,
} ble_gap_phy_opt_t;

typedef enum {
    /// No role set yet
    BLE_GAP_ROLE_NONE = 0x00,
    /// Observer role
    BLE_GAP_ROLE_OBSERVER = 0x01,
    /// Broadcaster role
    BLE_GAP_ROLE_BROADCASTER = 0x02,
    /// Master/Central role
    BLE_GAP_ROLE_CENTRAL = (0x04 | BLE_GAP_ROLE_OBSERVER),
    /// Peripheral/Slave role
    BLE_GAP_ROLE_PERIPHERAL = (0x08 | BLE_GAP_ROLE_BROADCASTER),
    /// Device has all role, both peripheral and central
    BLE_GAP_ROLE_ALL = (BLE_GAP_ROLE_CENTRAL | BLE_GAP_ROLE_PERIPHERAL),
} ble_gap_role_t;

/// Privacy configuration
typedef enum {
    /// Identity address type. Public address:0, Static private random
    /// address:1
    BLE_GAP_PRIV_CFG_PRIV_ADDR_BIT = (1 << 0),
    /// Reserved
    BLE_GAP_PRIV_CFG_RSVD = (1 << 1),
    /// Enable controller privacy
    BLE_GAP_PRIV_CFG_PRIV_EN_BIT = (1 << 2),
} ble_gap_priv_cfg_t;

/// Pairing Mode
typedef enum {
    /// No pairing authorized
    BLE_GAP_PAIRING_DISABLE = 0,
    /// Legacy pairing Authorized
    BLE_GAP_PAIRING_LEGACY = (1 << 0),
    /// Secure Connection pairing Authorized
    BLE_GAP_PAIRING_SEC_CON = (1 << 1),
} ble_gap_pairing_mode_t;

/// Device Attribute write permission requirement
typedef enum {
    /// Disable write access
    BLE_GAP_WRITE_DISABLE,
    /// Enable write access - no authentication required
    BLE_GAP_WRITE_NO_AUTH,
    /// Write access requires unauthenticated link
    BLE_GAP_WRITE_UNAUTH,
    /// Write access requires authenticated link
    BLE_GAP_WRITE_AUTH,
    /// Write access requires secure connected link
    BLE_GAP_WRITE_SEC_CON,
} ble_gap_write_att_perm_t;

/// Attribute database configuration
///    8    7     6    5    4    3    2    1    0
/// +-----+----+-----+----+----+----+----+----+----+
/// | MTU | SC | PCP |   APP_PERM   |   NAME_PERM  |
/// +-----+----+-----+----+----+----+----+----+----+
///   15   14   13   12    11     10    9
/// +----+----+----+----+------+------+----+
/// |        RFU        | RPAO | EATT | FE |
/// +----+----+----+----+------+------+----+
/// - Bit [0-2]  : Device Name write permission requirements for peer device.
///                (@see ble_gap_write_att_perm_t)
/// - Bit [3-5]  : Device Appearance write permission requirements for peer
///                device (@see ble_gap_write_att_perm_t)
/// - Bit [6]    : Peripheral Preferred Connection Parameters present.
/// - Bit [7]    : Service change feature present in GATT attribute database.
/// - Bit [8]    : Enable automatic MTU exchange at connection establishment.
/// - Bit [9]    : Enable automatic client feature enable setup at connection
///                establishment.
/// - Bit [10]   : Enable automatic establishment of Enhanced ATT bearers.
/// - Bit [11]   : Enable presence of Resolvable private address only.
/// - Bit [12-15]: Reserve for future use.
typedef enum {
    /// Device Name write permission requirements for peer device
    /// (@see ble_gap_write_att_perm_t)
    BLE_GAP_ATT_NAME_PERM_MASK = 0x0007,
    BLE_GAP_ATT_NAME_PERM_POS = 0,
    /// Device Appearance write permission requirements for peer device
    /// (@see ble_gap_write_att_perm_t)
    BLE_GAP_ATT_APPEARENCE_PERM_MASK = 0x0038,
    BLE_GAP_ATT_APPEARENCE_PERM_POS = 3,
    /// Peripheral Preferred Connection Parameters present in GAP attribute
    /// database.
    BLE_GAP_ATT_PERIPH_PREF_CON_PAR_EN_MASK  = 0x0040,
    BLE_GAP_ATT_PERIPH_PREF_CON_PAR_EN_POS  = 6,
    /// Service change feature present in GATT attribute database.
    BLE_GAP_ATT_SVC_CHG_EN_MASK = 0x0080,
    BLE_GAP_ATT_SVC_CHG_EN_POS = 7,
    /// Automatic MTU exchange at connection establishment.
    BLE_GAP_ATT_CLI_AUTO_MTU_EXCH_EN_MASK = 0x0100,
    BLE_GAP_ATT_CLI_AUTO_MTU_EXCH_EN_POS = 8,
    /// Automatic client feature enable
    BLE_GAP_ATT_CLI_AUTO_FEAT_EN_MASK = 0x0200,
    BLE_GAP_ATT_CLI_AUTO_FEAT_EN_POS = 9,
    /// Automatic establishment of Enhanced ATT bearers.
    BLE_GAP_ATT_CLI_AUTO_EATT_EN_MASK = 0x0400,
    BLE_GAP_ATT_CLI_AUTO_EATT_POS = 10,
    /// Presence of Resolvable private address only.
    BLE_GAP_ATT_RSLV_PRIV_ADDR_ONLY_MASK = 0x0800,
    BLE_GAP_ATT_RSLV_PRIV_ADDR_ONLY_POS = 11,
} ble_gap_att_cfg_flag_t;

typedef struct {
    /// Device Role: Central, Peripheral, Observer, Broadcaster or All roles.
    ble_gap_role_t role;

    /// Privacy Config
    /// Duration before regenerate device address when privacy is enabled.
    /// unit:sec
    uint16_t renew_dur;
    /// Provided own static private random address
    ble_bdaddr_t addr;
    /// Device IRK used for resolvable random BD address generation (LSB first)
    ble_gap_key_t irk;
    /// Privacy configuration bit field
    ble_gap_priv_cfg_t privacy_cfg;

    /// Security Config
    /// Pairing mode authorized
    ble_gap_pairing_mode_t pairing_mode;

    /// ATT Database Config
    /// GAP service start handle
    uint16_t gap_start_hdl;
    /// GATT service start handle
    uint16_t gatt_start_hdl;
    /// Attribute database configuration
    ble_gap_att_cfg_flag_t att_cfg;

    /// LE Data Length Extension
    /// Suggested value for the Controller's maximum transmitted number of
    /// payload octets to be used
    uint16_t sugg_max_tx_octets;
    /// Suggested value for the Controller's maximum packet transmission time to
    /// be used (unit: micro sec)
    uint16_t sugg_max_tx_time;

    /// L2CAP Configuration
    /// Maximal MTU size
    uint16_t max_mtu;
    /// Maximal MPS Packet size
    uint16_t max_mps;
    /// Maximum number of LE Credit based connection that can be established
    uint8_t max_nb_lecb;

    /// LE PHY Management
    /// Preferred LE PHY for data transmission
    ble_gap_phy_mode_t tx_pref_phy;
    /// Preferred LE PHY for data reception
    ble_gap_phy_mode_t rx_pref_phy;

    /// Other settings
    /// RF TX Path Compensation value (from -128dB to 128dB, unit is 0.1dB)
    uint16_t tx_path_comp;
    /// RF RX Path Compensation value (from -128dB to 128dB, unit is 0.1dB)
    uint16_t rx_path_comp;
} ble_gap_set_dev_config_t;

/// BLE peer info type
typedef enum {
    /// Retrieve name of peer device.
    BLE_GAP_GET_PEER_NAME,
    /// Retrieve peer device version info.
    BLE_GAP_GET_PEER_VERSION,
    /// Retrieve peer device features.
    BLE_GAP_GET_PEER_FEATURES,
    /// Retrieve connection RSSI.
    BLE_GAP_GET_CON_RSSI,
    /// Retrieve connection channel map.
    BLE_GAP_GET_CON_CHANNEL_MAP,
    /// Get peer device appearance.
    BLE_GAP_GET_PEER_APPEARANCE,
    /// Get peer device peripheral preferred parameters.
    BLE_GAP_GET_PEER_PERIPH_PREF_PARAMS,
    /// Get timer timeout value.
    BLE_GAP_GET_LE_PING_TO,
    /// Central address resolution supported.
    BLE_GAP_GET_ADDR_RESOL_SUPP,
    /// Retrieve PHY configuration of active link.
    BLE_GAP_GET_PHY,
    /// Retrieve channel selection algorithm.
    BLE_GAP_GET_CHAN_SEL_ALGO,
} ble_gap_link_info_type_t;

/// Peer irk structure
typedef struct {
    /// irk information
    ble_gap_irk_t irk;
    /// Bonding Index
    int idx;
} ble_gap_irk_idx_t;

/// Resolving address command complete event data structure
typedef struct {
    /// Index of irk
    uint8_t index;
    /// Number of irk
    uint8_t nb;
    /// Irk
    ble_gap_irk_idx_t irk[__ARRAY_EMPTY];
} resl_addr_t;

/// Connection parameter
typedef struct {
    /// Connection interval minimum
    uint16_t intv_min;
    /// Connection interval maximum
    uint16_t intv_max;
    /// Latency
    uint16_t latency;
    /// Supervision timeout
    uint16_t time_out;
} ble_gap_conn_param_t;

typedef struct {
    /// GAP requested operation
    uint8_t operation;
    /// Status of the request
    uint8_t status;
} gapm_cmp_evt_t;

/// The list of names for registered gap event function.
typedef enum {
    /// Handler in atm_gap.
    GAP_EVT_NAME_ATM_GAP,
    /// Handler in ble_gap_sec.
    GAP_EVT_NAME_BLE_GAP_SEC,
    /// Handler in test.
    GAP_EVT_NAME_TEST,
    /// Max number of this enumerations.
    GAP_EVT_NAME_MAX,
} ble_gap_evt_reg_name_t;

typedef struct {
    /// 8-byte array for random number
    uint8_t nb[BLE_GAP_RAND_NB_LEN];
} ble_rand_nb_t;

typedef struct ble_gap_ext_adv_report_ind {
    /// Bit field info
    ble_gap_adv_report_info_t info;
    /// Transmitter device address
    ble_gap_bdaddr_t trans_addr;
    /// Target device address
    /// Valid only for directed advertising report
    ble_gap_bdaddr_t target_addr;
    /// TX power (in dBm)
    int8_t tx_pwr;
    /// RSSI (between -127 and +20 dBm)
    int8_t rssi;
    /// Primary PHY (1:1M, 3:Coded)
    uint8_t phy_prim;
    /// Secondary PHY (0:No packet on secondary phy, 1:1M, 2:2M, 3:Coded)
    uint8_t phy_second;
    /// Advertising SID
    /// Valid only for periodic advertising report
    uint8_t adv_sid;
    /// Periodic advertising interval (in unit of 1.25ms, min is 7.5ms)
    /// Valid only for periodic advertising report
    uint16_t period_adv_intv;
    /// Report length
    uint16_t length;
    /// Report
    uint8_t data[__ARRAY_EMPTY];
} ble_gap_ind_ext_adv_report_t;

/// Device name
typedef struct {
    /// Name length
    uint16_t value_length;
    /// Name value (starting from offset to maximum length for BLE_GAP_DEV_NAME
    /// request)
    uint8_t value[__ARRAY_EMPTY];
} ble_gap_dev_name_t;

typedef struct {
    uint8_t con_idx;
    ble_gap_dev_name_t param;
} ble_gap_le_dev_name_t;

typedef struct {
    uint8_t con_idx;
    /// Appearance Icon
    uint16_t appearance;
} ble_gap_le_appearance_t;

/// Peripheral preference parameters
typedef struct {
    /// Connection interval minimum
    uint16_t con_intv_min;
    /// Connection interval maximum
    uint16_t con_intv_max;
    /// Peripheral latency
    uint16_t periph_latency;
    /// Connection supervision timeout multiplier
    uint16_t con_timeout;
} ble_gap_periph_pref_t;

typedef struct {
    uint8_t con_idx;
    ble_gap_periph_pref_t param;
} ble_gap_le_periph_pref_t;

typedef struct {
    uint8_t con_idx;
    /// Resolvable Private address only
    uint8_t rslv_priv_addr_only;
} ble_gap_le_rslv_priv_addr_only_t;

/// Peer version info
typedef struct {
    /// Manufacturer name
    uint16_t comp_id;
    /// LMP subversion
    uint16_t lmp_subvers;
    /// LMP version
    uint8_t lmp_vers;
} ble_gap_peer_version_t;

typedef struct {
    uint8_t con_idx;
    ble_gap_peer_version_t param;
} ble_gap_le_peer_version_t;

/// Peer features info
typedef struct {
    /// 8-byte array for LE features
    uint8_t features[BLE_GAP_LE_FEATS_LEN];
} ble_gap_peer_features_t;

typedef struct {
    uint8_t con_idx;
    ble_gap_peer_features_t param;
} ble_gap_le_peer_features_t;

/// Ongoing connection RSSI
typedef struct {
    /// RSSI value
    int8_t rssi;
} ble_gap_con_rssi_t;

typedef struct {
    uint8_t con_idx;
    ble_gap_con_rssi_t param;
} ble_gap_le_con_rssi_t;

/// Channel map structure
typedef struct {
    /// 5-byte channel map array
    uint8_t map[BLE_GAP_LE_CHNL_MAP_LEN];
} ble_chnl_map_t;

/// Ongoing connection Channel Map
typedef struct {
    /// Channel map value
    ble_chnl_map_t ch_map;
} ble_gap_con_channel_map_t;

typedef struct {
    uint8_t con_idx;
    ble_gap_con_channel_map_t param;
} ble_gap_le_con_channel_map_t;

/// LE Ping
typedef struct {
    /// Authenticated payload timeout
    uint16_t timeout;
} ble_gap_le_ping_to_val_t;

typedef struct {
    uint8_t con_idx;
    ble_gap_le_ping_to_val_t param;
} ble_gap_le_le_ping_to_val_t;

/// Active link PHY configuration. Triggered when configuration is read or
/// during an update.
typedef struct {
    /// LE PHY for data transmission.
    ble_gap_phy_val_t tx_phy;
    /// LE PHY for data reception.
    ble_gap_phy_val_t rx_phy;
} ble_gap_le_phy_t;

typedef struct {
    uint8_t con_idx;
    ble_gap_le_phy_t param;
} ble_gap_le_le_phy_t;

/// Channel selection algorithm
typedef struct {
    /// Used channel selection algorithm
    uint8_t chan_sel_algo;
} ble_gap_chan_sel_algo_t;

typedef struct {
    uint8_t con_idx;
    ble_gap_chan_sel_algo_t param;
} ble_gap_le_chan_sel_algo_t;

/// Response data of @ref ble_gapc_get_link_info
typedef struct {
    /// Get information type.
    ble_gap_link_info_type_t type;
    union {
	/// Data for BLE_GAP_GET_PEER_NAME type
	ble_gap_le_dev_name_t name;
	/// Data for BLE_GAP_GET_PEER_APPEARANCE type
	ble_gap_le_appearance_t appearance;
	/// Data for BLE_GAP_GET_PEER_PERIPH_PREF_PARAMS type
	ble_gap_le_periph_pref_t periph_pref;
	/// Data for BLE_GAP_GET_ADDR_RESOL_SUPP type
	ble_gap_le_rslv_priv_addr_only_t rslv_priv_addr_only;
	/// Data for BLE_GAP_GET_PEER_VERSION type
	ble_gap_le_peer_version_t peer_ver;
	/// Data for BLE_GAP_GET_PEER_FEATURES type
	ble_gap_le_peer_features_t peer_feat;
	/// Data for BLE_GAP_GET_CON_RSSI type
	ble_gap_le_con_rssi_t con_rssi;
	/// Data for BLE_GAP_GET_CON_CHANNEL_MAP type
	ble_gap_le_con_channel_map_t con_chnl_map;
	/// Data for BLE_GAP_GET_LE_PING_TO type
	ble_gap_le_le_ping_to_val_t le_ping_to;
	/// Data for BLE_GAP_GET_PHY type
	ble_gap_le_le_phy_t le_phy;
	/// Data for BLE_GAP_GET_CHAN_SEL_ALGO type
	ble_gap_le_chan_sel_algo_t chan_sel_algo;
    };
} ble_gap_link_info_t;

typedef struct {
    /// Activity identifier
    uint8_t actv_idx;
    /// Activity stop reason
    ble_err_code_t reason;
    /// In case of periodic advertising, indicate if periodic advertising
    /// has been stopped
    uint8_t per_adv_stop;
} ble_gap_ind_stop_t;

typedef struct {
    /// Connection handle
    uint16_t conhdl;
    /// Connection interval
    uint16_t con_interval;
    /// Connection latency
    uint16_t con_latency;
    /// Supervision timeout
    uint16_t sup_to;
    /// Clock accuracy
    uint8_t clk_accuracy;
    /// Peer address type
    uint8_t peer_addr_type;
    /// Peer BT address
    ble_bdaddr_t peer_addr;
    /// Role of device in connection (0 = Central / 1 = Peripheral)
    uint8_t role;
} ble_gap_ind_con_est_t;

typedef struct {
    /// Connection index
    uint8_t conidx;
    /// Connection parameter
    ble_gap_ind_con_est_t param;
} ble_gap_ind_le_con_est_t;

/// Long Term Key information
typedef struct {
    /// Long Term Key
    ble_gap_key_t ltk;
    /// Encryption Diversifier
    uint16_t ediv;
    /// Random Number
    ble_rand_nb_t randnb;
    /// Encryption key size (7 to 16)
    uint8_t key_size;
} ble_gap_ltk_t;

typedef struct {
    /// Connection index
    uint8_t conidx;
    /// Long Term Key information
    ble_gap_ltk_t ltk;
} ble_gap_ind_le_ltk_t;

typedef struct {
    /// Connection index
    uint8_t conidx;
} ble_gap_ind_le_repeated_attempt_t;

/// Authentication mask
typedef enum {
    /// No Flag set
    BLE_GAP_AUTH_NONE = 0,
    /// Bond authentication
    BLE_GAP_AUTH_BOND = (1 << 0),
    /// Man In the middle protection
    BLE_GAP_AUTH_MITM = (1 << 2),
    /// Secure Connection
    BLE_GAP_AUTH_SEC_CON = (1 << 3),
    /// Key Notification
    BLE_GAP_AUTH_KEY_NOTIF = (1 << 4),
} ble_gap_auth_mask_t;

/// Authentication Requirements
typedef enum {
    /// No MITM No Bonding
    BLE_GAP_AUTH_REQ_NO_MITM_NO_BOND = BLE_GAP_AUTH_NONE,
    /// No MITM Bonding
    BLE_GAP_AUTH_REQ_NO_MITM_BOND = BLE_GAP_AUTH_BOND,
    /// MITM No Bonding
    BLE_GAP_AUTH_REQ_MITM_NO_BOND = BLE_GAP_AUTH_MITM,
    /// MITM and Bonding
    BLE_GAP_AUTH_REQ_MITM_BOND = BLE_GAP_AUTH_MITM | BLE_GAP_AUTH_BOND,
    /// SEC_CON, No MITM and No Bonding
    BLE_GAP_AUTH_REQ_NO_MITM_SEC_CON_NO_BOND = BLE_GAP_AUTH_SEC_CON,
    /// SEC_CON, No MITM and Bonding
    BLE_GAP_AUTH_REQ_NO_MITM_SEC_CON_BOND = BLE_GAP_AUTH_SEC_CON |
	BLE_GAP_AUTH_BOND,
    /// SEC_CON, MITM and No Bonding
    BLE_GAP_AUTH_REQ_MITM_SEC_CON_NO_BOND = BLE_GAP_AUTH_SEC_CON |
	BLE_GAP_AUTH_MITM,
    /// SEC_CON, MITM and Bonding
    BLE_GAP_AUTH_REQ_MITM_SEC_CON_BOND = BLE_GAP_AUTH_SEC_CON |
	BLE_GAP_AUTH_MITM | BLE_GAP_AUTH_BOND,

    /// Mask of  authentication features without reserved flag
    BLE_GAP_AUTH_REQ_MASK = 0x1F
} ble_gap_auth_t;

typedef enum {
    /// Unauthenticated pairing achieved but without bond data
    /// (meaning-less for connection confirmation)
    BLE_GAP_PAIRING_UNAUTH = BLE_GAP_AUTH_NONE,
    /// Authenticated pairing achieved but without bond data
    /// (meaning-less for connection confirmation)
    BLE_GAP_PAIRING_AUTH = BLE_GAP_AUTH_MITM,
    /// Secure connection pairing achieved but without bond data
    /// (meaning-less for connection confirmation)
    BLE_GAP_PAIRING_SECURE_CON = BLE_GAP_AUTH_SEC_CON | BLE_GAP_AUTH_MITM,

    /// No pairing performed with peer device
    /// (meaning-less for connection confirmation)
    BLE_GAP_PAIRING_NO_BOND = BLE_GAP_AUTH_NONE,
    /// Peer device bonded through an unauthenticated pairing.
    BLE_GAP_PAIRING_BOND_UNAUTH = BLE_GAP_AUTH_BOND,
    /// Peer device bonded through an authenticated pairing.
    BLE_GAP_PAIRING_BOND_AUTH = BLE_GAP_AUTH_MITM | BLE_GAP_AUTH_BOND,
    /// Peer device bonded through a secure connection pairing pairing.
    BLE_GAP_PAIRING_BOND_SECURE_CON = BLE_GAP_AUTH_SEC_CON | BLE_GAP_AUTH_MITM |
	BLE_GAP_AUTH_BOND,

    /// Pairing with bond data present Bit
    BLE_GAP_PAIRING_BOND_PRESENT_BIT = 0x01,
    BLE_GAP_PAIRING_BOND_PRESENT_POS = 0x00,
} ble_gap_pairing_level_t;

typedef struct {
    /// Connection handle
    uint8_t conidx;
    /// Error code
    ble_err_code_t reason;
    /// Pairing security level. (Valid if reason == BLE_ERR_NO_ERROR)
    ble_gap_pairing_level_t pairing_lvl;
    /// LTK is exchanged. (Valid if reason == BLE_ERR_NO_ERROR)
    bool ltk_present;
} ble_gap_ind_le_pair_end_t;

typedef struct {
    /// Connection handle
    uint16_t conhdl;
    /// Reason of disconnection
    ble_err_code_t reason;
} ble_gap_ind_discon_t;

typedef struct {
    uint8_t conidx;
    ble_gap_ind_discon_t param;
} ble_gap_ind_le_discon_t;

/// Privacy mode type
typedef enum {
    /// Network privacy mode
    BLE_GAP_PRIV_TYPE_NETWORK,
    /// Device privacy mode
    BLE_GAP_PRIV_TYPE_DEVICE,
} ble_gap_priv_type_t;

/// Resolving list device information
typedef struct {
    /// Device identity
    ble_gap_bdaddr_t addr;
    /// Privacy Mode
    ble_gap_priv_type_t priv_mode;
    /// Peer IRK
    uint8_t peer_irk[BLE_GAP_KEY_LEN];
    /// Local IRK
    uint8_t local_irk[BLE_GAP_KEY_LEN];
} ble_gap_ral_dev_info_t;

typedef struct {
    /// Connection interval used on this connection, unit:1.25 ms
    uint16_t con_interval;
    /// Peripheral latency for the connection in number of connection events,
    /// range: 0x0000 to 0x01F3
    uint16_t con_latency;
    /// Supervision timeout for this connection, unit: 10ms
    uint16_t sup_to;
} ble_gap_ind_con_update_t;

typedef struct {
    uint8_t conidx;
    ble_gap_ind_con_update_t param;
} ble_gap_ind_le_con_update_t;

typedef enum {
    /// Device name
    BLE_GAP_DEV_NAME,
    /// Device external appearance
    BLE_GAP_DEV_APPEARANCE,
    /// Peripheral preferred connection parameters
    BLE_GAP_DEV_PERIPH_PREF_PARAMS,
    /// Central address resolution supported
    BLE_GAP_DEV_CTL_ADDR_RESOL,
    /// Device database hash value
    BLE_GAP_DEV_DB_HASH,
    /// Resolvable private addresss as local addresss only
    BLE_GAP_DEV_RSLV_PRIV_ADDR_ONLY,
    /// Maximum device info parameter
    BLE_GAP_DEV_INFO_MAX,
} ble_gap_dev_info_t;

typedef union {
    /// Device name
    ble_gap_dev_name_t name;
    /// Appearance Icon
    uint16_t appearance;
} ble_gap_set_dev_info_t;

typedef struct {
    /// Requested device information
    ble_gap_dev_info_t req_type;
    /// Token value
    uint16_t token;
    /// Device information data
    ble_gap_set_dev_info_t info;
} ble_gap_ind_set_dev_req_t;

typedef struct {
    uint8_t conidx;
    ble_gap_ind_set_dev_req_t param;
} ble_gap_ind_le_set_dev_req_t;

typedef struct {
    /// true to accept connection parameters, false else.
    bool accept;
    /// Minimum length of connection event needed for this LE connection in
    /// 0.625 ms.
    uint16_t ce_len_min;
    /// Maximum length of connection event needed for this LE connection in
    /// 0.625 ms.
    uint16_t ce_len_max;
} ble_gapc_param_update_cfm_t;

typedef struct {
    /// Connection interval minimum in 1.25 ms
    uint16_t intv_min;
    /// Connection interval maximum in 1.25 ms
    uint16_t intv_max;
    /// Latency
    uint16_t latency;
    /// Supervision timeout in 10 ms
    uint16_t time_out;
} ble_gap_ind_param_update_req_t;

typedef struct {
    uint8_t conidx;
    ble_gap_ind_param_update_req_t param;
} ble_gap_ind_le_param_update_req_t;

typedef struct {
    /// PHY updated
    ble_gap_le_phy_t phy;
} ble_gap_ind_le_phy_t;

typedef struct {
    uint8_t con_idx;
    ble_gap_ind_le_phy_t param;
} ble_gap_ind_le_le_phy_t;

typedef struct {
    /// Channel selection algorithm updated
    ble_gap_chan_sel_algo_t algo;
} ble_gap_ind_chan_sel_algo_t;

typedef struct {
    uint8_t con_idx;
    ble_gap_ind_chan_sel_algo_t param;
} ble_gap_ind_le_chan_sel_algo_t;

/// Additional advertising parameters
typedef struct {
    /// Advertising duration (in unit of 10ms). 0 means that advertising
    /// continues until the host disable it
    uint16_t duration;
    /// Maximum number of extended advertising events the controller shall
    /// attempt to send prior to terminating the extending advertising Valid
    /// only if extended advertising
    uint8_t max_adv_evt;
} ble_gapm_adv_param_t;

/// Scan window operation parameters
typedef struct {
    /// Scan interval
    uint16_t scan_intv;
    /// Scan window
    uint16_t scan_wd;
} ble_gapm_scan_wd_op_param_t;

/// Scanning types
typedef enum {
    /// General discovery
    BLE_GAPM_SCAN_TYPE_GEN_DISC,
    /// Limited discovery
    BLE_GAPM_SCAN_TYPE_LIM_DISC,
    /// Observer
    BLE_GAPM_SCAN_TYPE_OBSERVER,
    /// Selective observer
    BLE_GAPM_SCAN_TYPE_SEL_OBSERVER,
    /// Connectable discovery
    BLE_GAPM_SCAN_TYPE_CONN_DISC,
    /// Selective connectable discovery
    BLE_GAPM_SCAN_TYPE_SEL_CONN_DISC,
} ble_gapm_scan_type_t;

/// Scanning properties bit field bit value
typedef enum {
    /// Scan advertisement on the LE 1M PHY
    BLE_GAPM_SCAN_PROP_PHY_1M_BIT = (1 << 0),
    /// Scan advertisement on the LE Coded PHY
    BLE_GAPM_SCAN_PROP_PHY_CODED_BIT = (1 << 1),
    /// Active scan on LE 1M PHY (Scan Request PDUs may be sent)
    BLE_GAPM_SCAN_PROP_ACTIVE_1M_BIT = (1 << 2),
    /// Active scan on LE Coded PHY (Scan Request PDUs may be sent)
    BLE_GAPM_SCAN_PROP_ACTIVE_CODED_BIT = (1 << 3),
    /// Accept directed advertising packets if we use a RPA and target address
    /// cannot be solved by the controller
    BLE_GAPM_SCAN_PROP_ACCEPT_RPA_BIT = (1 << 4),
    /// Filter truncated advertising or scan response reports
    BLE_GAPM_SCAN_PROP_FILT_TRUNC_BIT = (1 << 5),
} ble_gapm_scan_prop_t;

/// Filtering policy for duplicated packets
typedef enum {
    /// Disable filtering of duplicated packets
    BLE_GAPM_DUP_FILT_DIS,
    /// Enable filtering of duplicated packets
    BLE_GAPM_DUP_FILT_EN,
    /// Enable filtering of duplicated packets, reset for each scan period
    BLE_GAPM_DUP_FILT_EN_PERIOD,
} ble_gapm_dup_filter_pol_t;

/// Scanning parameters
typedef struct {
    /// Type of scanning to be started
    ble_gapm_scan_type_t type;
    /// Properties for the scan procedure
    ble_gapm_scan_prop_t prop;
    /// Duplicate packet filtering policy
    ble_gapm_dup_filter_pol_t dup_filt_pol;
    /// Reserved for future use
    uint8_t rsvd;
    /// Scan window opening parameters for LE 1M PHY
    ble_gapm_scan_wd_op_param_t scan_param_1m;
    /// Scan window opening parameters for LE Coded PHY
    ble_gapm_scan_wd_op_param_t scan_param_coded;
    /// Scan duration (in unit of 10ms). 0 means that the controller will scan
    /// continuously until reception of a stop command from the application
    uint16_t duration;
    /// Scan period (in unit of 1.28s). Time interval between two consequent
    /// starts of a scan duration by the controller. 0 means that the scan
    /// procedure is not periodic
    uint16_t period;
} ble_gapm_scan_param_t;

/// Initiating types
typedef enum {
    /// Direct connection establishment, establish a connection with an
    /// indicated device
    BLE_GAPM_INIT_TYPE_DIRECT_CONN_EST,
    /// Automatic connection establishment, establish a connection with all
    /// devices whose address is present in the white list
    BLE_GAPM_INIT_TYPE_AUTO_CONN_EST,
    /// Name discovery, Establish a connection with an indicated device in order
    /// to read content of its Device Name characteristic. Connection is closed
    /// once this operation is stopped.
    BLE_GAPM_INIT_TYPE_NAME_DISC,
} ble_gapm_init_type_t;

/// Initiating properties
typedef enum {
    /// Scan connectable advertisements on the LE 1M PHY. Connection parameters
    /// for the LE 1M PHY are provided
    BLE_GAPM_INIT_PROP_1M_BIT = (1 << 0),
    /// Connection parameters for the LE 2M PHY are provided
    BLE_GAPM_INIT_PROP_2M_BIT = (1 << 1),
    /// Scan connectable advertisements on the LE Coded PHY. Connection
    /// parameters for the LE Coded PHY are provided
    BLE_GAPM_INIT_PROP_CODED_BIT = (1 << 2),
} ble_gapm_init_prop_t;

/// Connection parameters
typedef struct {
    /// Minimum value for the connection interval (in unit of 1.25ms). Shall be
    /// less than or equal to conn_intv_max value. Allowed range is 7.5ms to 4s.
    uint16_t conn_intv_min;
    /// Maximum value for the connection interval (in unit of 1.25ms). Shall be
    /// greater than or equal to conn_intv_min value. Allowed range is 7.5ms to
    /// 4s.
    uint16_t conn_intv_max;
    /// Slave latency. Number of events that can be missed by a connected slave
    /// device
    uint16_t conn_latency;
    /// Link supervision timeout (in unit of 10ms). Allowed range is 100ms to
    /// 32s
    uint16_t supervision_to;
    /// Recommended minimum duration of connection events (in unit of 625us)
    uint16_t ce_len_min;
    /// Recommended maximum duration of connection events (in unit of 625us)
    uint16_t ce_len_max;
} ble_gapm_conn_param_t;

/// Initiating parameters
typedef struct {
    /// Initiating type
    ble_gapm_init_type_t type;
    /// Properties for the initiating procedure
    ble_gapm_init_prop_t prop;
    /// Timeout for automatic connection establishment (in unit of 10ms). Cancel
    /// the procedure if not all indicated devices have been connected when the
    /// timeout occurs. 0 means there is no timeout
    uint16_t conn_to;
    /// Scan window opening parameters for LE 1M PHY
    ble_gapm_scan_wd_op_param_t scan_param_1m;
    /// Scan window opening parameters for LE Coded PHY
    ble_gapm_scan_wd_op_param_t scan_param_coded;
    /// Connection parameters for LE 1M PHY
    ble_gapm_conn_param_t conn_param_1m;
    /// Connection parameters for LE 2M PHY
    ble_gapm_conn_param_t conn_param_2m;
    /// Connection parameters for LE Coded PHY
    ble_gapm_conn_param_t conn_param_coded;
    /// Address of peer device in case white list is not used for connection
    ble_gap_bdaddr_t peer_addr;
} ble_gapm_init_param_t;

/// Periodic advertising information
typedef struct {
    /// Advertiser address information
    ble_gap_bdaddr_t addr;
    /// Advertising SID
    uint8_t adv_sid;
} ble_gapm_period_adv_addr_t;

/// Periodic synchronization types
typedef enum {
    /// Do not use periodic advertiser list for synchronization. Use advertiser
    /// information provided in the ble_gapm_start_per_sync().
    BLE_GAPM_PER_SYNC_TYPE_GENERAL,
    /// Use periodic advertiser list for synchronization
    BLE_GAPM_PER_SYNC_TYPE_SELECTIVE,
    /// Use Periodic advertising sync transfer information send through
    /// connection for synchronization
    BLE_GAPM_PER_SYNC_TYPE_PAST,
} ble_gapm_per_sync_type_t;

/// Bit field of enabled advertising reports
typedef enum {
    /// Periodic advertising reports reception enabled
    BLE_GAPM_REPORT_ADV_EN_BIT = 0x01,
    BLE_GAPM_REPORT_ADV_EN_POS = 0,
    /// BIG Info advertising reports reception enabled
    BLE_GAPM_REPORT_BIGINFO_EN_BIT = 0x02,
    BLE_GAPM_REPORT_BIGINFO_EN_POS = 1,
    /// Periodic advertising reports reception enabled with duplicate filtering
    /// enabled.
    BLE_GAPM_REPORT_ADV_DUP_FIL_EN_BIT = 0x04,
    BLE_GAPM_REPORT_ADV_DUP_FIL_EN_POS = 2,
} ble_gapm_report_en_bf_t;

/// Constant Tone Extension sync filtering type
typedef enum {
    /// Do not sync to packets with an AoA Constant Tone Extension
    BLE_GAPM_CTE_NO_SYNC_WITH_AOA = (1 << 0),
    /// Do not sync to packets with an AoD Constant Tone Extension with 1 us
    /// slots
    BLE_GAPM_CTE_NO_SYNC_WITH_AOD_1US_SLOT = (1 << 1),
    /// Do not sync to packets with an AoD Constant Tone Extension with 2 us
    /// slots
    BLE_GAPM_CTE_NO_SYNC_WITH_AOD_2US_SLOT = (1 << 2),
    /// Do not sync to packets with a type 3 Constant Tone Extension (currently
    /// reserved for future use)
    BLE_GAPM_CTE_NO_SYNC_WITH_TYPE_3 = (1 << 3),
    /// Do not sync to packets without a Constant Tone Extension
    BLE_GAPM_CTE_NO_SYNC_WITHOUT_CTE = (1 << 4),
} ble_gapm_sync_cte_type_t;

/// Periodic synchronization parameters
typedef struct {
    /// Number of periodic advertising that can be skipped after a successful
    /// receive. Maximum authorized value is 499.
    uint16_t skip;
    /// Synchronization timeout for the periodic advertising (in unit of 10ms
    /// between 100ms and 163.84s)
    uint16_t sync_to;
    /// Periodic synchronization type
    ble_gapm_per_sync_type_t type;
    /// Connection index used for periodic sync info reception (only valid for
    /// BLE_GAPM_PER_SYNC_TYPE_PAST)
    uint8_t conidx;
    /// Address of advertiser with which synchronization has to be established
    /// (used only if type is BLE_GAPM_PER_SYNC_TYPE_GENERAL)
    ble_gapm_period_adv_addr_t adv_addr;
    /// Bit field that contains list of reports that are enabled or not.
    ble_gapm_report_en_bf_t report_en_bf;
    /// Type of Constant Tone Extension device should sync on.
    ble_gapm_sync_cte_type_t cte_type;
} ble_gapm_per_sync_param_t;

/// Type of activities that can be created
typedef enum {
    /// Advertising activity
    BLE_GAPM_ACTV_TYPE_ADV,
    /// Scanning activity
    BLE_GAPM_ACTV_TYPE_SCAN,
    /// Initiating activity
    BLE_GAPM_ACTV_TYPE_INIT,
    /// Periodic synchronization activity
    BLE_GAPM_ACTV_TYPE_PER_SYNC,
} ble_gapm_actv_type_t;

typedef struct {
    /// The maximum Tx payload size
    uint16_t max_tx_octets;
    /// The maximum Tx time (unit: us)
    uint16_t max_tx_time;
    /// The maximum Rx payload size
    uint16_t max_rx_octets;
    /// The maximum Rx time (unit: us)
    uint16_t max_rx_time;
} ble_gap_ind_pkt_size_t;

typedef struct {
    uint8_t conidx;
    ble_gap_ind_pkt_size_t param;
} ble_gap_ind_le_pkt_size_t;

typedef struct {
    /// Requested information
    ble_gap_dev_info_t req;
    /// Token value that must be returned in confirmation
    uint16_t token;
    /// Device name data offset
    uint16_t name_offset;
    /// Maximum name length (starting from offset)
    uint16_t max_name_length;
} ble_gap_ind_get_dev_info_req_t;

typedef struct {
    uint8_t conidx;
    ble_gap_ind_get_dev_info_req_t param;
} ble_gap_ind_le_get_dev_info_req_t;

typedef struct {
    /// Encryption status
    ble_err_code_t status;
    /// Pairing security level
    ble_gap_pairing_level_t pairing_lvl;
} ble_gap_ind_encrypt_t;

typedef struct {
    uint8_t conidx;
    ble_gap_ind_encrypt_t param;
} ble_gap_ind_le_encrypt_t;

typedef struct {
    /// Pairing security level
    ble_gap_pairing_level_t pairing_lvl;
} ble_gap_encrypt_t;

typedef struct {
    uint8_t conidx;
    ble_gap_encrypt_t param;
} ble_gap_le_encrypt_t;

typedef enum {
    /// Passkey entry started
    BLE_GAP_PASSKEY_ENTRY_STARTED,
    /// Passkey digit entered
    BLE_GAP_PASSKEY_DIGIT_ENTERED,
    /// Passkey digit erased
    BLE_GAP_PASSKEY_DIGIT_ERASED,
    /// Passkey cleared
    BLE_GAP_PASSKEY_CLEARED,
    /// Passkey entry completed
    BLE_GAP_PASSKEY_ENTRY_COMPLETED,
} ble_gap_notif_type_t;

typedef struct {
    /// Keypress notification type
    ble_gap_notif_type_t keypress_notif_type;
} ble_gap_ind_keypress_notif_t;

typedef struct {
    uint8_t conidx;
    ble_gap_ind_keypress_notif_t param;
} ble_gap_ind_le_keypress_notif_t;

typedef struct {
    /// Requested information
    ble_gap_dev_info_t req;
    /// Status of the confirmation
    ble_err_code_t status;
    /// Token value provided in request indication
    uint16_t token;
    /// Complete value length including offset
    uint16_t complete_length;
    union {
	/// Data for BLE_GAP_DEV_NAME request
	ble_gap_dev_name_t name;
	/// Data for BLE_GAP_DEV_APPEARANCE request
	uint16_t appearance;
	/// Data for BLE_GAP_DEV_PERIPH_PREF_PARAMS request
	ble_gap_periph_pref_t periph_pref;
	/// Data for BLE_GAP_DEV_CTL_ADDR_RESOL request
	uint8_t ctl_addr_resol;
	/// Data for BLE_GAP_DEV_DB_HASH request
	uint8_t hash[BLE_GAP_DATABASE_HASH_LEN];
	/// Data for BLE_GAP_DEV_RSLV_PRIV_ADDR_ONLY request
	/// 0: Only Resolvable Private Addresses will be used as local addresses
	/// after bonding; 1-255: Reserved for future use.
	uint8_t rslv_priv_addr_only;
    };
} ble_gap_get_dev_info_cfm_t;

typedef struct {
    /// Internally used
    uint8_t operation;
    /// Status of the request
    ble_err_code_t status;
    /// Connection index
    uint8_t con_idx;
    union {
	/// Data for operation (@see enum ble_gap_link_info_type_t)
	ble_gap_link_info_t info;
	/// Authentication. Data in callback of @ref ble_gap_sec_encrypt_req
	ble_gap_le_encrypt_t encrypt;
    };
} ble_gapc_cmpl_t;

typedef struct {
    /// Internally used
    uint8_t operation;
    /// Status of the request
    ble_err_code_t status;
    /// Extra data
    union {
	/// Random number. Data in callback of @ref ble_gapm_gen_rand.
	ble_rand_nb_t randnb;
	/// Resolved addr
	resl_addr_t resladr;
	/// Activity index. Data in callback of @ref ble_gapm_stop_activity
	uint8_t actv_stop_idx;
	/// Generated address which is returned data of
	/// @ref ble_gapm_gen_rand_addr
	ble_bdaddr_t randadr;
	/// Generated encrypted data which is returned data of
	/// @ref ble_gapm_enc_block
	uint8_t enc_result[BLE_GAP_KEY_LEN];
	/// Number of received packets. Data in callback of
	/// @ref ble_gapm_le_test_stop
	uint16_t nb_packet_received;
	/// Periodic sync activity index which is returned data of
	/// @ref ble_gapm_create_persync_activity
	uint8_t persync_act_idx;
	/// Scan activity index which is returned data of
	/// @ref ble_gapm_create_scan_activity
	uint8_t scan_act_idx;
	/// Initiating activity index which is returned data of
	/// @ref ble_gapm_create_init_activity
	uint8_t init_act_idx;
	/// ADV creation information. Data in callback of
	/// @ref ble_gapm_create_adv_activity.
	struct {
	    /// ADV identifier
	    uint8_t adv_idx;
	    /// Selected TX power for ADV activity
	    int8_t tx_pwr;
	} adv_created;
    };
} ble_gapm_cmpl_t;

/// Invalid connection index
#define BLE_GAP_INVALID_CONIDX 0xFF

/// GAP event
typedef enum {
    /// Indication of ADV stop
    BLE_GAP_EID_IND_ADV_STOP,
    /// Indication of Scan stop
    BLE_GAP_EID_IND_SCAN_STOP,
    /// Indication of initiating stop
    BLE_GAP_EID_IND_INIT_STOP,
    /// Indication of periodic sync stop
    BLE_GAP_EID_IND_PER_SYNC_STOP,
    /// Indication of extended advertising report
    BLE_GAP_EID_IND_EXT_ADV_REPORT,
    /// Indication of connection established
    BLE_GAP_EID_IND_CON_EST,
    /// Indication of connection disconnected
    BLE_GAP_EID_IND_DISCON,
    /// Indication of LTK
    BLE_GAP_EID_IND_LTK,
    /// Indication of connection parameter updated
    BLE_GAP_EID_IND_CON_UPDATE,
    /// Indication of set device information request
    BLE_GAP_EID_IND_SET_DEV_INFO_REQ,
    /// Pairing end
    BLE_GAP_EID_IND_PAIR_END,
    /// Indication of parameter updating request
    BLE_GAP_EID_IND_PARAM_UPDATE_REQ,
    /// Indication of LE PHY
    BLE_GAP_EID_IND_LE_PHY,
    /// Indication of channel selection algorithm
    BLE_GAP_EID_IND_CHAN_SEL_ALGO,
    /// Indication of LE packet size
    BLE_GAP_EID_IND_PKT_SIZE,
    /// Indication of getting device information request
    BLE_GAP_EID_IND_GET_DEV_INFO_REQ,
    /// Indication of encrypt
    BLE_GAP_EID_IND_ENCRYPT,
    /// Indicatin of repeated attempt
    BLE_GAP_EID_IND_REPEATED_ATTEMPT,
    /// Indicatin of keypress notification
    BLE_GAP_EID_IND_KEYPRESS_NOTIFICATION,
} ble_gap_eid_t;

typedef struct {
    /// Event ID
    ble_gap_eid_t eid;
    union {
	/// Data for BLE_GAP_EID_IND_ADV_STOP event.
	ble_gap_ind_stop_t ind_adv_stop;
	/// Data for BLE_GAP_EID_IND_SCAN_STOP event.
	ble_gap_ind_stop_t ind_scan_stop;
	/// Data for BLE_GAP_EID_IND_INIT_STOP event.
	ble_gap_ind_stop_t ind_init_stop;
	/// Data for BLE_GAP_EID_IND_PER_SYNC_STOP event.
	ble_gap_ind_stop_t ind_per_sync_stop;
	/// Data for BLE_GAP_EID_IND_EXT_ADV_REPORT event.
	ble_gap_ind_ext_adv_report_t ind_ext_adv_rpt;
	/// Data for BLE_GAP_EID_IND_CON_EST event.
	ble_gap_ind_le_con_est_t ind_con_est;
	/// Data for BLE_GAP_EID_IND_DISCON event.
	ble_gap_ind_le_discon_t ind_discon;
	/// Data for BLE_GAP_EID_IND_LTK event.
	ble_gap_ind_le_ltk_t ind_ltk;
	/// Data for BLE_GAP_EID_IND_CON_UPDATE event.
	ble_gap_ind_le_con_update_t ind_con_update;
	// Data for BLE_GAP_EID_IND_SET_DEV_INFO_REQ event.
	ble_gap_ind_le_set_dev_req_t ind_set_dev_req;
	/// Data for BLE_GAP_EDI_IND_PAIR_END
	ble_gap_ind_le_pair_end_t ind_pair_end;
	/// Data for BLE_GAP_EID_IND_PARAM_UPDATE_REQ event.
	ble_gap_ind_le_param_update_req_t ind_param_update_req;
	/// Data for BLE_GAP_EID_IND_LE_PHY event.
	ble_gap_ind_le_le_phy_t ind_le_phy;
	/// Data for BLE_GAP_EID_IND_CHAN_SEL_ALGO event.
	ble_gap_ind_le_chan_sel_algo_t ind_sel_algo;
	/// Data for BLE_GAP_EID_IND_PKT_SIZE event.
	ble_gap_ind_le_pkt_size_t ind_pkt_size;
	/// Data for BLE_GAP_EID_IND_GET_DEV_INFO_REQ event.
	ble_gap_ind_le_get_dev_info_req_t ind_get_dev_info_req;
	/// Data for BLE_GAP_EID_IND_ENCRYPT event.
	ble_gap_ind_le_encrypt_t ind_encrypt;
	/// Data for BLE_GAP_EID_IND_REPEATED_ATTEMPT event.
	ble_gap_ind_le_repeated_attempt_t ind_repeated_attempt;
	/// Data for BLE_GAP_EID_IND_KEYPRESS_NOTIFICATION event.
	ble_gap_ind_le_keypress_notif_t ind_keypress_notif;
    };
} ble_gap_evt_t;

/// Own BD address source of the device
typedef enum {
    /// Public or Private Static Address
    BLE_OWN_STATIC_ADDR,
    /// Generated resolvable private random address
    BLE_OWN_GEN_RSLV_ADDR,
    /// Generated non-resolvable private random address
    BLE_OWN_GEN_NON_RSLV_ADDR,
} ble_own_addr_t;

/// GAPC command complete callback prototype
/// @param[in] conidx Connection index.
/// @param[in] parm Parameter returned.
/// @param[in] ctx Context provided from calling API.
typedef void (*gapc_cmpl_cb)(uint8_t conidx, struct gapc_cmp_evt const *parm,
    void const *ctx);

/// GAPC message
/// @param[in] id message id
/// @param[in] conidx The connection index
/// @param[in] is_bcast Specify the message is broadcast or not
/// @param[in] parm message parameter
typedef void (*gapc_ind)(enum gapc_msg_id id, uint8_t conidx, bool is_bcast,
    void const *parm);

/// GAPM message callback
/// @param[in] id message id
/// @param[in] parm message parameter
typedef void (*gapm_ind)(enum gapm_msg_id id, void const *parm);

/// GAPM add profile callback prototype
/// @param[in] parm Parameter returned.
/// @param[in] ctx Context provided from calling API.
typedef void (*gapm_profile_add_cb)(void *parm, void const *ctx);

// FIXME: please remove comment below after coverting process is done.
// Coverting mapping for process of cleaning up violation of abstraction:
//| incument            | succeesor            |
//|=====================+======================|
//| gapc_cmpl_cb        | ble_gapc_cmd_cb      |
//| gapm_cmp_evt_t      | ble_gapm_cmpl_t      |
//| struct gapc_cmp_evt | ble_gapc_cmpl_t      |
//| ble_gapm_reg_ind    | ble_gap_evt_func_reg |
//| ble_gapc_reg_ind    | ble_gap_evt_func_reg |
//| gapc_ind            | ble_gap_evt_fn       |
//| gapm_ind            | ble_gap_evt_fn       |
// The incumbent functions(structs) would be totally replaced finally.

/// GAPC command complete callback prototype
/// @param[in] conidx Connection index.
/// @param[in] parm Parameter returned.
/// @param[in] ctx Context provided from calling API.
typedef void (*ble_gapc_cmd_cb)(uint8_t conidx, ble_gapc_cmpl_t const *parm,
    void const *ctx);

/// GAPM command complete callback prototype
/// @param[in] parm Parameter returned.
/// @param[in] ctx Context provided from calling API.
typedef void (*ble_gapm_cmd_cb)(ble_gapm_cmpl_t const *parm, void const *ctx);

/// GAP event callback function
/// @param[in] event Event parameter
/// @return True if processed. If return value is false, the event will be sent
/// to next registered function.
typedef bool (*ble_gap_evt_fn)(ble_gap_evt_t const *event);

/*
 * MACROS
 *******************************************************************************
 */

/// Invalid bond index
#define BLE_GAP_INVALID_BOND (-1)


/*
 * GLOBAL C API
 *******************************************************************************
 */
/**
 *******************************************************************************
 * @brief Register ble gap event callback
 *
 * @param[in] pri Priority for service order. 0 is the highest.
 * @param[in] name Name of this event func.
 * @param[in] fn Event function
 *******************************************************************************
 */
void ble_gap_evt_func_reg(uint8_t pri, ble_gap_evt_reg_name_t name,
    ble_gap_evt_fn fn);

/**
 *******************************************************************************
 * @brief Get gap event handler
 *
 * @param[in] name Name string for event handler.
 * @return Event function. NULL if not found.
 *******************************************************************************
 */
ble_gap_evt_fn ble_gap_evt_func_get(ble_gap_evt_reg_name_t name);

/**
 *******************************************************************************
 * @brief Set device configuration
 *
 * @param[in] cfg device configuration
 * @param[in] cb Complete callback function.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
__NONNULL(1)
void ble_gapm_set_device_config(ble_gap_set_dev_config_t const *cfg,
    ble_gapm_cmd_cb cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Remove gap event handler
 *
 * @param[in] name Name string for event handler.
 * @return Removed callback function. NULL if not found.
 *******************************************************************************
 */
ble_gap_evt_fn ble_gap_evt_func_rm(ble_gap_evt_reg_name_t name);

/**
 *******************************************************************************
 * @brief Generate random number
 *
 * @param[in] cb Complete callback function. The random number will be return in
 * randnb data field of ble_gapm_cmpl_t which is passed as a argument of cb.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
void ble_gapm_gen_rand(ble_gapm_cmd_cb cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Set content of the white list
 * @param[in] count Number of entries to be added in the list. 0 means that list
 * content has to be cleared.
 * @param[in] addrs List of entries to be added in the list.
 * @param[in] cb Command complete callback. NULL means that the caller does not
 * need to be informed after the command completion.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
__NONNULL(2)
void ble_gapm_set_white_list(uint8_t count, ble_gap_bdaddr_t const addrs[],
    ble_gapm_cmd_cb cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Set content of the resolving list
 * @param[in] count Number of entries to be added in the list. 0 means that list
 * content has to be cleared.
 * @param[in] rals List of entries to be added in the list.
 * @param[in] cb Command complete callback. NULL means that the caller does not
 * need to be informed after the command completion.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
void ble_gapm_set_resolving_list(uint8_t count,
    ble_gap_ral_dev_info_t const rals[], ble_gapm_cmd_cb cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Lower slave latency locally
 * @param[in] conidx Connection index.
 * @param[in] latency
 * @param[in] cb Command complete callback
 * @param[in] ctx Context data.
 *******************************************************************************
 */
void ble_gapc_lower_slave_latency_locally(uint8_t conidx, uint16_t latency,
    ble_gapc_cmd_cb cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Set the preferred slave event duration (for slave role configuration)
 * @param[in] conidx Connection index.
 * @param[in] dur Preferred event duration that the controller should use on a
 * connection (N * 0.625 ms)
 * @param[in] single_tx Slave transmits a single packet per connection event
 * (False/True)
 * @param[in] cb Command complete callback
 * @param[in] ctx Context data.
 *******************************************************************************
 */
void ble_gapc_set_prefer_slave_event_duration(uint8_t conidx, uint16_t dur,
    bool single_tx, ble_gapc_cmd_cb cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Start TX direct test mode
 * @param[in] param Test TX parameters.
 * @param[in] cb Command complete callback. NULL means that the caller does not
 * need to be informed after the command completion.
 *******************************************************************************
 */
__NONNULL(1)
void ble_gapm_le_test_tx(ble_gap_test_tx_param_t const *param,
    ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Start RX direct test mode.
 * @param[in] param Test RX parameters.
 * @param[in] cb Command complete callback. NULL means that the caller does not
 * need to be informed after the command completion.
 *******************************************************************************
 */
__NONNULL(1)
void ble_gapm_le_test_rx(ble_gap_test_rx_param_t const *param,
    ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Stop the direct test mode.
 * @param[in] cb Command complete callback.
 *******************************************************************************
 */
__NONNULL_ALL
void ble_gapm_le_test_stop(ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Resolve provided random address using array of Identity Resolution Key
 * (IRK) exchanged.
 * @param[in] addr Resolvable random address to solve.
 * @param[in] nb Number of provided IRK.
 * @param[in] irkarr peer IRK
 * @param[in] cb Command complete callback.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
void ble_gapm_resolv_addr(ble_bdaddr_t addr, uint8_t nb, ble_gap_irk_idx_t irkarr[],
    ble_gapm_cmd_cb cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Generate random address
 * @param[in] addr_type random address type
 * @param[in] cb Command complete or indication callback.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
void ble_gapm_gen_rand_addr(ble_gapm_rand_addr_type_t addr_type,
    ble_gapm_cmd_cb cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Encrypt data
 * @param[in] key 128 bit operand (key)
 * @param[in] data 128 bit operand (data)
 * @param[in] cb Command complete callback.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
void ble_gapm_enc_block(uint8_t const key[BLE_GAP_KEY_LEN],
    uint8_t const data[BLE_GAP_KEY_LEN], ble_gapm_cmd_cb cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Stop an activity
 * @param[in] idx Activity identifier
 * @param[in] ctx Context data.
 * @param[in] cb Command complete callback.
 *******************************************************************************
 */
void ble_gapm_stop_activity(uint8_t idx, void const *ctx, ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Delete activity
 * @param[in] idx Activity identifier.
 * @param[in] ctx Context data.
 * @param[in] cb Command complete callback.
 *******************************************************************************
 */
void ble_gapm_delete_activity(uint8_t idx, void const *ctx, ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Set advertising data
 * @param[in] data_type  ADV data type
 * @param[in] idx Activity identifier.
 * @param[in] len Length of ADV data.
 * @param[in] data ADV data array.
 * @param[in] ctx Context data.
 * @param[in] cb Command complete callback.
 *****************************************************************************
 */
void ble_gapm_set_adv_data(ble_gapm_data_type_t data_type, uint8_t idx,
    uint16_t len, uint8_t const *data, void const *ctx, ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Get link information
 * @param[in] info Operation correspond to information.
 * @param[in] conidx Connection index.
 * @param[in] cb Complete callback function. NULL means that the caller does not
 * need to be informed after the command completion.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
void ble_gapc_get_link_info(uint8_t conidx, ble_gap_link_info_type_t info,
    ble_gapc_cmd_cb cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Set phy. rate
 * @param[in] conidx Connection index.
 * @param[in] tx_phy Tx phy.
 * @param[in] rx_phy Rx phy.
 * @param[in] phy_opt Coded phy. opt.
 * @param[in] cb Complete callback function. NULL means that the caller does not
 * need to be informed after the command completion.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
void ble_gapc_set_phy(uint8_t conidx, ble_gap_phy_mode_t tx_phy,
    ble_gap_phy_mode_t rx_phy, ble_gap_phy_opt_t phy_opt, ble_gapc_cmd_cb cb,
    void const *ctx);

/**
 *******************************************************************************
 * @brief Update connection parameters
 * @param[in] conidx Connection index.
 * @param[in] param Connection parameters.
 * @param[in] cb Complete callback function.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
__NONNULL(2)
void ble_gapc_update_param(uint8_t conidx, ble_gap_conn_param_t const *param,
    ble_gapc_cmd_cb cb, void const *ctx);

/**
 *******************************************************************************
 * @brief Disconnect specific link
 * @param[in] conidx Connection index.
 * @param[in] reason Reason for disconnection. Only allow BLE_HCI_MODULE and
 * BLE_SMP_MODULE type.
 * @param[in] cb Complete callback function.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
void ble_gapc_disconnect(uint8_t conidx, ble_err_code_t reason, ble_gapc_cmd_cb cb,
    void const *ctx);

/**
 *******************************************************************************
 * @brief Set device IRK
 * @param[in] irk IRK array data. Size is BLE_GAP_KEY_LEN(16).
 * @param[in] cb Complete callback function.
 * @param[in] ctx Context data.
 *******************************************************************************
 */
__NONNULL(1)
void ble_gapm_set_irk(uint8_t const irk[BLE_GAP_KEY_LEN], ble_gapm_cmd_cb cb,
    void const *ctx);

/**
 *******************************************************************************
 * @brief Set device information confirmation (corresponding to
 * BLE_GAP_EID_IND_SET_DEV_INFO_REQ event)
 * @param[in] conidx Connection index.
 * @param[in] req_type Request type
 * @param[in] status Status of the request
 * @param[in] token Token value
 *******************************************************************************
 */
void ble_gapc_set_dev_info_cfm(uint8_t conidx, ble_gap_dev_info_t req_type,
    ble_err_code_t status, uint16_t token);

/**
 *******************************************************************************
 * @brief Retrieve connection address information
 * @param[in] conidx Connection index
 * @param[out] addr Pointer to peer addr.
 * @return True if connection exists.
 *******************************************************************************
 */
__NONNULL(2)
bool ble_gapc_get_peer_addr(uint8_t conidx, ble_gap_bdaddr_t *addr);

/**
 *******************************************************************************
 * @brief Confirm reception of BLE_GAP_EID_IND_PARAM_UPDATE_REQ event
 * @param[in] conidx Connection index.
 * @param[in] param Confirmation parameters.
 *******************************************************************************
 */
__NONNULL(2)
void ble_gapc_param_update_cfm(uint8_t conidx,
    ble_gapc_param_update_cfm_t const *param);

/**
 *******************************************************************************
 * @brief Confirm reception of BLE_GAP_EID_IND_GET_DEV_INFO_REQ event
 * @param[in] conidx Connection index.
 * @param[in] param Confirmation parameters.
 *******************************************************************************
 */
__NONNULL(2)
void ble_gapc_get_dev_info_cfm(uint8_t conidx,
    ble_gap_get_dev_info_cfm_t const *param);

/**
 *******************************************************************************
 * @brief Create periodic sync activity
 * @param[in] own_addr_type Own address type
 * @param[in] ctx Context data.
 * @param[in] cb Command complete callback.
 *******************************************************************************
 */
__NONNULL(3)
void ble_gapm_create_persync_activity(ble_own_addr_t own_addr_type,
    void const *ctx, ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Create scan activity
 * @param[in] own_addr_type Own address type
 * @param[in] ctx Context data.
 * @param[in] cb Command complete callback.
 *******************************************************************************
 */
__NONNULL(3)
void ble_gapm_create_scan_activity(ble_own_addr_t own_addr_type,
    void const *ctx, ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Create initiating activity
 * @param[in] own_addr_type Own address type
 * @param[in] ctx Context data.
 * @param[in] cb Command complete callback.
 *******************************************************************************
 */
__NONNULL(3)
void ble_gapm_create_init_activity(ble_own_addr_t own_addr_type,
    void const *ctx, ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Start advertising
 * @param[in] idx Activity identifier.
 * @param[in] dur Advertising duration (in unit of 10ms). 0 means that
 * advertising continues until the application stop actively.
 * @param[in] max_evt  Maximum number of extended advertising events the
 * controller shall attempt to send prior to terminating the extending
 * advertising. Valid only if extended advertising.
 * @param[in] ctx Context data.
 * @param[in] cb Command complete callback. NULL means that the caller does not
 * need to be informed after the command completion.
 *******************************************************************************
 */
void ble_gapm_start_adv(uint8_t idx, uint16_t dur, uint8_t max_evt,
    void const *ctx, ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Start scanning
 * @param[in] idx Activity identifier.
 * @param[in] param Scanning parameters.
 * @param[in] ctx Context data.
 * @param[in] cb Command complete callback. NULL means that the caller does not
 * need to be informed after the command completion.
 *******************************************************************************
 */
__NONNULL(2)
void ble_gapm_start_scan(uint8_t idx, ble_gapm_scan_param_t const *param,
    void const *ctx, ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Start Initiating
 * @param[in] idx Activity identifier.
 * @param[in] param Initiating parameters.
 * @param[in] ctx Context data.
 * @param[in] cb Command complete callback. NULL means that the caller does not
 * need to be informed after the command completion.
 *******************************************************************************
 */
__NONNULL(2)
void ble_gapm_start_init(uint8_t idx, ble_gapm_init_param_t const *param,
    void const *ctx, ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Start periodic synchronization
 * @param[in] idx Activity identifier.
 * @param[in] param Periodic synchronization parameters.
 * @param[in] ctx Context data.
 * @param[in] cb Command complete callback. NULL means that the caller does not
 * need to be informed after the command completion.
 *******************************************************************************
 */
__NONNULL(2)
void ble_gapm_start_per_sync(uint8_t idx,
    ble_gapm_per_sync_param_t const *param, void const *ctx,
    ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Create ADV activity
 * @param[in] own_addr_type Own address type
 * @param[in] param Advertising parameters for advertising creation.
 * @param[in] ctx Context data
 * @param[in] cb Command complete callback.
 *******************************************************************************
 */
__NONNULL(2)
void ble_gapm_create_adv_activity(ble_own_addr_t own_addr_type,
    ble_gapm_adv_create_param_t const *param, void const *ctx,
    ble_gapm_cmd_cb cb);

/**
 *******************************************************************************
 * @brief Get advertising create parameters from Flash NVDS.
 * @param[in] is_wurx True if the adv is wurx.
 * @param[in,out] own_addr_type Own addr type.
 * @param[in,out] create Create parameter
 * @return true for success
 *******************************************************************************
 */
__NONNULL_ALL
bool ble_gapm_get_adv_create_nvds(bool is_wurx, ble_own_addr_t *own_addr_type,
    ble_gapm_adv_create_param_t *create);

#ifdef __cplusplus
}
#endif

///@} ATM_BTFM_BLE_GAP

