/**
 *******************************************************************************
 *
 * @file atm_adv_param.h
 *
 * @brief Atmosic BLE advertising parameter
 *
 * Copyright (C) Atmosic 2020-2022
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_PADVP ADV parameters
 * @ingroup ATM_BTFM_PROC
 * @brief ATM bluetooth framework advertisement parameters
 *
 * This module contains the necessary function to provide predefined parameters
 * for usage of ADV procedures @see atm_adv.h
 *
 * @{
 *******************************************************************************
 */

#include "atm_log.h"
#include "ble_gap.h"

// Overwrite setting by application
#ifdef GAP_ADV_PARM_NAME
#include STR(GAP_ADV_PARM_NAME)
#endif

// GAP_ADV_PARM_NAME can overwrite default parameter setting
#include "atm_adv_param_internal.h"

#ifdef __cplusplus
extern "C" {
#endif

#if CFG_ADV_CREATE_PARAM_CONST
#define __ATM_ADV_CREATE_PARAM_CONST const
#else
#define __ATM_ADV_CREATE_PARAM_CONST
#endif

#if CFG_ADV_DATA_PARAM_CONST
#define __ATM_ADV_DATA_PARAM_CONST const
#else
#define __ATM_ADV_DATA_PARAM_CONST
#endif

#if CFG_ADV_START_PARAM_CONST
#define __ATM_ADV_START_PARAM_CONST const
#else
#define __ATM_ADV_START_PARAM_CONST
#endif


/// Parameter for advertising data and scan response data
typedef struct {
    /// Length of advertising data or scan response data
    uint16_t len;
    /// The content of advertising data or scan response data
    uint8_t __ATM_ADV_DATA_PARAM_CONST *data;
} atm_adv_data_t;

/// NVDS parameter for advertising data and scan response data
typedef struct {
    /// Length of advertising data or scan response data
    uint16_t len;
    /// The content of advertising data or scan response data
    uint8_t *data;
} atm_adv_data_nvds_t;

/// Create an advertising activity command
typedef struct {
    ble_own_addr_t own_addr_type;
    ble_gapm_adv_create_param_t adv_param;
} atm_adv_create_t;

/// Parameters of Start advertising
typedef ble_gapm_adv_param_t atm_adv_start_t;

#ifndef CFG_GAP_ADV_MAX_INST
#define CFG_GAP_ADV_MAX_INST 1
#endif

/**
 *******************************************************************************
 * @brief Get persistent advertising create parameter.
 * @param[in] idx The index of persistent parameter.
 *******************************************************************************
 */
atm_adv_create_t __ATM_ADV_CREATE_PARAM_CONST *atm_adv_create_param_get(uint8_t idx);

/**
 *******************************************************************************
 * @brief Get persistent advertising start parameter.
 * @param[in] idx The index of persistent parameter.
 *******************************************************************************
 */
atm_adv_start_t __ATM_ADV_START_PARAM_CONST *atm_adv_start_param_get(uint8_t idx);

/**
 *******************************************************************************
 * @brief Get persistent advertising data.
 * @param[in] idx The index of persistent parameter.
 *******************************************************************************
 */
atm_adv_data_t __ATM_ADV_DATA_PARAM_CONST *atm_adv_advdata_param_get(uint8_t idx);

/**
 *******************************************************************************
 * @brief Get persistent scan response data.
 * @param[in] idx The index of persistent parameter.
 *******************************************************************************
 */
atm_adv_data_t __ATM_ADV_DATA_PARAM_CONST *atm_adv_scandata_param_get(uint8_t idx);

/**
 *******************************************************************************
 * @brief Get advertising create parameters from Flash NVDS.
 * @param[in] is_wurx True if wurx ADV.
  * @param[in,out] create Create parameter
  * @return true for success
 *******************************************************************************
 */
bool atm_adv_create_param_nvds(bool is_wurx, atm_adv_create_t *create);

/**
 *******************************************************************************
 * @brief Get advertising start parameters from Flash NVDS.
  * @param[in,out] start Start parameter
  * @return true for success
 *******************************************************************************
 */
bool atm_adv_start_param_nvds(atm_adv_start_t *start);

/**
 *******************************************************************************
 * @brief Get scan response data from Flash NVDS.
  * @param[in,out] scan Scan response data. scan->data must be NULL.
  * @return true for success, scan->data will be allocated dynamically
 *******************************************************************************
 */
bool atm_adv_scandata_param_nvds(atm_adv_data_nvds_t *scan);

/**
 *******************************************************************************
 * @brief Get advertising data from Flash NVDS.
  * @param[in,out] adv Advertising data. adv->data must be NULL.
  * @return true for success, adv->data will be allocated dynamically
 *******************************************************************************
 */
bool atm_adv_advdata_param_nvds(atm_adv_data_nvds_t *adv);

/**
 *******************************************************************************
 * @brief Get advertising data from Flash NVDS.
  * @param[in,out] adv Advertising data/scan response data. adv->data must be NULL.
  * @param[in] tag_id NVDS tag ID
  * @return true for success, adv->data will be allocated dynamically
 *******************************************************************************
 */
__NONNULL(1)
bool atm_adv_data_param_nvds_id(atm_adv_data_nvds_t *adv, uint8_t tag_id);

/**
 *******************************************************************************
 * @brief Free advertising or scan response data reading from Flash NVDS
 * @param[in] nvds_data Advertising or scan response data
 *******************************************************************************
 */
void atm_adv_data_param_nvds_free(atm_adv_data_nvds_t *nvds_data);

#ifdef __cplusplus
}
#endif

///@} ATM_BTFM_PADVP
