/**
 *******************************************************************************
 *
 * @file atm_co_utils.h
 *
 * @brief ATM bluetooth framework common utility functions
 *
 * Copyright (C) Atmosic 2020
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_CO_UTILS Common utilities
 * @ingroup ATM_BTFM
 * @brief ATM bluetooth framework common utility functions
 *
 * This module contains the common utilities functions and macros
 * used by the bluetooth framework API.
 *
 * @{
 *******************************************************************************
 */

#include "gap.h"
#include "co_utils.h"

#ifdef __cplusplus
extern "C" {
#endif

/// Macro to create a for-loop for a co_list
#define FOR_CO_LIST(co_list, type, hdlr) \
    for (type hdlr = (type)co_list_pick(&co_list); hdlr; \
        hdlr = (type)co_list_next(&hdlr->hdr)) \

/// Macro to create a for-loop for pop co_list FIFO
#define FOR_CO_LIST_POP(co_list, type, hdlr) \
    for (type hdlr = (type)co_list_pop_front(&co_list); hdlr; \
	hdlr = (type)co_list_pop_front(&co_list)) \

/// Redefine struct gap_bdaddr to gap_bdaddr_t
typedef struct gap_bdaddr gap_bdaddr_t;

/**
 *******************************************************************************
 * @brief Convert data type from bd_addr_t to struct bd_addr
 * @param[in] addr address to convert
 *
 * @return addr pointer using struct bd_addr data type
 *******************************************************************************
 */
__INLINE struct bd_addr const *atm_co_convert_bd_addr_type(bd_addr_t const *addr)
{
    return (struct bd_addr const *)addr;
}

__INLINE bool atm_co_gap_addr_compare(gap_bdaddr_t const *addr1,
    gap_bdaddr_t const *addr2)
{
    return addr1 && addr2 && (addr1->addr_type == addr2->addr_type) &&
	co_bdaddr_compare(atm_co_convert_bd_addr_type(&addr1->addr),
	atm_co_convert_bd_addr_type(&addr2->addr));
}
/**
 * @brief Covert 16 bits data endian.
 */
#define ATM_CO_BSWAP16(val16) (((val16 << 8) & 0xFF00) | ((val16 >> 8) & 0xFF))

__INLINE int atm_co_popcount(uint32_t x)
{
#ifdef __GNUC__
    return __builtin_popcount(x);
#else
    int i = 0;
    for (; x; x &= x - 1) {
	i++;
    }
    return i;
#endif
}

#ifdef __cplusplus
}
#endif

/// @} ATM_CO_UTILS
