/**
 *******************************************************************************
 *
 * @file ble_err.h
 *
 * @brief Bluetooth framework common error code enums.
 *
 * Copyright (C) Atmosic 2022-2023
 *
 *******************************************************************************
 */

#pragma once

/// Error code size of module
#define BLE_ERR_CODE_BITS 8

/// Module of error codes
typedef enum {
    BLE_HCI_MODULE,
    BLE_L2C_MODULE,
    BLE_ATT_MODULE,
    BLE_SMP_MODULE,
    BLE_GAP_MODULE,
} ble_module_type_t;

/// Macro for module
#define BLE_MODULE(x) BLE_##x##_MODULE

/// Macro for combining errors
#define BLE_ERR(x, y) ((BLE_MODULE(x) << BLE_ERR_CODE_BITS) | y)

/// Macro for getting error type
#define BLE_ERR_TYPE(x) ((x) >> BLE_ERR_CODE_BITS)

/// Error codes
/// @note This error code is mapped from the lower layer. Should not be changed.
typedef enum {
    /// No error.
    BLE_ERR_NO_ERROR = 0,
    /// HCI Specific Error
    /// Unknown HCI Command.
    BLE_HCI_ERR_UNKNOWN_HCI_COMMAND = BLE_ERR(HCI, 0x01),
    /// Unknown Connection Identifier.
    BLE_HCI_ERR_UNKNOWN_CONNECTION_ID = BLE_ERR(HCI, 0x02),
    /// Hardware Failure.
    BLE_HCI_ERR_HARDWARE_FAILURE = BLE_ERR(HCI, 0x03),
    /// Page Timeout.
    BLE_HCI_ERR_PAGE_TIMEOUT = BLE_ERR(HCI, 0x04),
    /// Authentication Failure.
    BLE_HCI_ERR_AUTH_FAILURE = BLE_ERR(HCI, 0x05),
    /// PIN or Key Missing.
    BLE_HCI_ERR_PIN_MISSING = BLE_ERR(HCI, 0x06),
    /// Memory Capacity Exceeded.
    BLE_HCI_ERR_MEMORY_CAPA_EXCEED = BLE_ERR(HCI, 0x07),
    /// Connection Timeout.
    BLE_HCI_ERR_CON_TIMEOUT = BLE_ERR(HCI, 0x08),
    /// Connection Limit Exceeded.
    BLE_HCI_ERR_CON_LIMIT_EXCEED = BLE_ERR(HCI, 0x09),
    /// Synchronous Connection Limit to a Device Exceeded.
    BLE_HCI_ERR_SYNC_CON_LIMIT_DEV_EXCEED = BLE_ERR(HCI, 0x0A),
    /// Connection Already Exists.
    BLE_HCI_ERR_CON_ALREADY_EXISTS = BLE_ERR(HCI, 0x0B),
    /// Command Disallowed.
    BLE_HCI_ERR_COMMAND_DISALLOWED = BLE_ERR(HCI, 0x0C),
    /// Connection Rejected Due To Limited Resources.
    BLE_HCI_ERR_CONN_REJ_LIMITED_RESOURCES = BLE_ERR(HCI, 0x0D),
    /// Connection Rejected Due To Security Reasons.
    BLE_HCI_ERR_CONN_REJ_SECURITY_REASONS = BLE_ERR(HCI, 0x0E),
    /// Connection Rejected due to Unacceptable BD_ADDR.
    BLE_HCI_ERR_CONN_REJ_UNACCEPTABLE_BDADDR = BLE_ERR(HCI, 0x0F),
    /// Connection Accept Timeout Exceeded.
    BLE_HCI_ERR_CONN_ACCEPT_TIMEOUT_EXCEED = BLE_ERR(HCI, 0x10),
    /// Unsupported Feature Or Parameter Value.
    BLE_HCI_ERR_UNSUPPORTED = BLE_ERR(HCI, 0x11),
    /// Invalid HCI Command Parameters.
    BLE_HCI_ERR_INVALID_HCI_PARAM = BLE_ERR(HCI, 0x12),
    /// Remote User Terminated Connection.
    BLE_HCI_ERR_REMOTE_USER_TERM_CON = BLE_ERR(HCI, 0x13),
    /// Remote Device Terminated Connection due to Low Resources.
    BLE_HCI_ERR_REMOTE_DEV_TERM_LOW_RESOURCES = BLE_ERR(HCI, 0x14),
    /// Remote Device Terminated Connection due to Power Off.
    BLE_HCI_ERR_REMOTE_DEV_POWER_OFF = BLE_ERR(HCI, 0x15),
    /// Connection Terminated By Local Host.
    BLE_HCI_ERR_CON_TERM_BY_LOCAL_HOST = BLE_ERR(HCI, 0x16),
    /// Repeated Attempts.
    BLE_HCI_ERR_REPEATED_ATTEMPTS = BLE_ERR(HCI, 0x17),
    /// Pairing Not Allowed.
    BLE_HCI_ERR_PAIRING_NOT_ALLOWED = BLE_ERR(HCI, 0x18),
    /// Unknown LMP PDU.
    BLE_HCI_ERR_UNKNOWN_LMP_PDU = BLE_ERR(HCI, 0x19),
    /// Unsupported Remote Feature.
    BLE_HCI_ERR_UNSUPPORTED_REMOTE_FEATURE = BLE_ERR(HCI, 0x1A),
    /// SCO Offset Rejected.
    BLE_HCI_ERR_SCO_OFFSET_REJECTED = BLE_ERR(HCI, 0x1B),
    /// SCO Interval Rejected.
    BLE_HCI_ERR_SCO_INTERVAL_REJECTED = BLE_ERR(HCI, 0x1C),
    /// SCO Air Mode Rejected.
    BLE_HCI_ERR_SCO_AIR_MODE_REJECTED = BLE_ERR(HCI, 0x1D),
    /// Invalid LMP Parameters / Invalid LL Parameters.
    BLE_HCI_ERR_INVALID_LMP_PARAM = BLE_ERR(HCI, 0x1E),
    /// Unspecified Error.
    BLE_HCI_ERR_UNSPECIFIED_ERROR = BLE_ERR(HCI, 0x1F),
    /// Unsupported LMP Parameter Value / Unsupported LL Parameter Value.
    BLE_HCI_ERR_UNSUPPORTED_LMP_PARAM_VALUE = BLE_ERR(HCI, 0x20),
    /// Role Change Not Allowed.
    BLE_HCI_ERR_ROLE_CHANGE_NOT_ALLOWED = BLE_ERR(HCI, 0x21),
    /// LMP Response Timeout / LL Response Timeout.
    BLE_HCI_ERR_LMP_RSP_TIMEOUT = BLE_ERR(HCI, 0x22),
    /// LMP Error Transaction Collision / LL Procedure Collision.
    BLE_HCI_ERR_LMP_COLLISION = BLE_ERR(HCI, 0x23),
    /// LMP PDU Not Allowed.
    BLE_HCI_ERR_LMP_PDU_NOT_ALLOWED = BLE_ERR(HCI, 0x24),
    /// Encryption Mode Not Acceptable.
    BLE_HCI_ERR_ENC_MODE_NOT_ACCEPT = BLE_ERR(HCI, 0x25),
    /// Link Key cannot be Changed.
    BLE_HCI_ERR_LINK_KEY_CANT_CHANGE = BLE_ERR(HCI, 0x26),
    /// Requested QoS Not Supported.
    BLE_HCI_ERR_QOS_NOT_SUPPORTED = BLE_ERR(HCI, 0x27),
    /// Instant Passed.
    BLE_HCI_ERR_INSTANT_PASSED = BLE_ERR(HCI, 0x28),
    /// Pairing With Unit Key Not Supported.
    BLE_HCI_ERR_PAIRING_WITH_UNIT_KEY_NOT_SUP = BLE_ERR(HCI, 0x29),
    /// Different Transaction Collision.
    BLE_HCI_ERR_DIFF_TRANSACTION_COLLISION = BLE_ERR(HCI, 0x2A),
    /// QoS Unacceptable Parameter.
    BLE_HCI_ERR_QOS_UNACCEPTABLE_PARAM = BLE_ERR(HCI, 0x2C),
    /// QoS Rejected.
    BLE_HCI_ERR_QOS_REJECTED = BLE_ERR(HCI, 0x2D),
    /// Channel Assessment Not Supported.
    BLE_HCI_ERR_CHANNEL_CLASS_NOT_SUP = BLE_ERR(HCI, 0x2E),
    /// Insufficient Security.
    BLE_HCI_ERR_INSUFFICIENT_SECURITY = BLE_ERR(HCI, 0x2F),
    /// Parameter Out Of Mandatory Range.
    BLE_HCI_ERR_PARAM_OUT_OF_MAND_RANGE = BLE_ERR(HCI, 0x30),
    /// Role Switch Pending.
    BLE_HCI_ERR_ROLE_SWITCH_PEND = BLE_ERR(HCI, 0x32),
    /// Reserved Slot Violation.
    BLE_HCI_ERR_RESERVED_SLOT_VIOLATION = BLE_ERR(HCI, 0x34),
    /// Role Switch Failed.
    BLE_HCI_ERR_ROLE_SWITCH_FAIL = BLE_ERR(HCI, 0x35),
    /// Extended Inquiry Response Too Large.
    BLE_HCI_ERR_EIR_TOO_LARGE = BLE_ERR(HCI, 0x36),
    /// Secure Simple Pairing Not Supported by Host.
    BLE_HCI_ERR_SP_NOT_SUPPORTED_HOST = BLE_ERR(HCI, 0x37),
    /// The Host Busy - Pairing.
    BLE_HCI_ERR_HOST_BUSY_PAIRING = BLE_ERR(HCI, 0x38),
    /// Connection Rejected due to No Suitable Channel Found.
    BLE_HCI_ERR_CONN_REJ_NO_SUITABLE_CHANN_FOUND = BLE_ERR(HCI, 0x39),
    /// Controller Busy.
    BLE_HCI_ERR_CONTROLLER_BUSY = BLE_ERR(HCI, 0x3A),
    /// Unacceptable Connection Parameters.
    BLE_HCI_ERR_UNACCEPTABLE_CONN_PARAM = BLE_ERR(HCI, 0x3B),
    /// Advertising Timeout.
    BLE_HCI_ERR_ADV_TO = BLE_ERR(HCI, 0x3C),
    /// Connection Terminated Due to MIC Failure.
    BLE_HCI_ERR_TERMINATED_MIC_FAILURE = BLE_ERR(HCI, 0x3D),
    /// Connection Failed to be Established / Synchronization Timeout.
    BLE_HCI_ERR_CONN_FAILED_TO_BE_EST = BLE_ERR(HCI, 0x3E),
    /// Coarse Clock Adjustment Rejected but Will Try to Adjust Using Clock
    /// Dragging.
    BLE_HCI_ERR_CCA_REJ_USE_CLOCK_DRAG = BLE_ERR(HCI, 0x40),
    /// Type0 Submap Not Defined.
    BLE_HCI_ERR_TYPE0_SUBMAP_NOT_DEFINED = BLE_ERR(HCI, 0x41),
    /// Unknown Advertising Identifier.
    BLE_HCI_ERR_UNKNOWN_ADVERTISING_ID = BLE_ERR(HCI, 0x42),
    /// Limit Reached.
    BLE_HCI_ERR_LIMIT_REACHED = BLE_ERR(HCI, 0x43),
    /// Operation Cancelled by Host.
    BLE_HCI_ERR_OPERATION_CANCELED_BY_HOST = BLE_ERR(HCI, 0x44),
    /// Undefined error.
    BLE_HCI_ERR_UNDEFINED = BLE_ERR(HCI, 0xFF),

    /// L2C Specific Error
    /// Table 4.3: Reason code descriptions in L2CAP of Core Specification.
    /// Command not understood.
    BLE_L2C_ERR_NOT_UNDERSTOOD = BLE_ERR(L2C, 0x01),
    /// Signaling MTU exceeded.
    BLE_L2C_ERR_INVALID_MTU_EXCEED = BLE_ERR(L2C, 0x02),
    /// Invalid CID in request.
    BLE_L2C_ERR_INVALID_CID = BLE_ERR(L2C, 0x03),
    /// Table 4.14: Result values for the L2CAP_CONNECTION_PARAMETER_UPDATE_RSP
    /// packet.
    /// Connection Parameters rejected.
    BLE_L2C_ERR_CONN_PARA_REJECTED = BLE_ERR(L2C, 0x04),
    /// Table 4.16: Result values for the L2CAP_LE_CREDIT_BASED_CONNECTION_RSP
    /// packet in L2CAP of Core Specification.
    /// Connection refused - SPSM not supported.
    BLE_L2C_ERR_SPSM_NOT_SUPP = BLE_ERR(L2C, 0x05),
    /// Connection refused - no resources available.
    BLE_L2C_ERR_NO_RES_AVAIL = BLE_ERR(L2C, 0x06),
    /// Connection refused - insufficient authentication.
    BLE_L2C_ERR_INSUFF_AUTHEN = BLE_ERR(L2C, 0x07),
    /// Connection refused - insufficient authorization.
    BLE_L2C_ERR_INSUFF_AUTHOR = BLE_ERR(L2C, 0x08),
    /// Connection refused - insufficient encryption key size.
    BLE_L2C_ERR_INSUFF_ENC_KEY_SIZE = BLE_ERR(L2C, 0x09),
    /// Connection refused - insufficient encryption.
    BLE_L2C_ERR_INSUFF_ENC = BLE_ERR(L2C, 0x0A),
    /// Connection refused – invalid Source CID.
    BLE_L2C_ERR_INVALID_SOURCE_CID = BLE_ERR(L2C, 0x0B),
    /// Connection refused – Source CID already allocated.
    BLE_L2C_ERR_CID_ALREADY_ALLOC = BLE_ERR(L2C, 0x0C),
    /// Connection refused – unacceptable parameters.
    BLE_L2C_ERR_UNACCEPT_PARA = BLE_ERR(L2C, 0x0D),
    /// Table 4.17: Result values for the L2CAP_CREDIT_BASED_CONNECTION_RSP
    /// packet in L2CAP of Core Specification.
    /// Connection pending – no further information available.
    BLE_L2C_ERR_NO_FURTHER_INFO_AVAIL = BLE_ERR(L2C, 0x0E),
    /// Connection pending – authentication pending.
    BLE_L2C_ERR_AUTHEN_PEND = BLE_ERR(L2C, 0x0F),
    /// Connection pending – authorization pending.
    BLE_L2C_ERR_AUTHOR_PEND = BLE_ERR(L2C, 0x10),
    /// Local L2CAP Specific Error
    /// Invalid PDU length exceed MPS.
    BLE_L2C_ERR_INVALID_MPS_EXCEED = BLE_ERR(L2C, 0x11),
    /// Invalid PDU.
    BLE_L2C_ERR_INVALID_PDU = BLE_ERR(L2C, 0x12),
    /// Credit error, invalid number of credit received.
    BLE_L2C_ERR_CREDIT_ERROR = BLE_ERR(L2C, 0x13),
    /// Undefined error.
    BLE_L2C_ERR_UNDEFINED = BLE_ERR(L2C, 0xFF),

    /// ATT Specific Error
    /// Refer to Table 3.4: Error codes in ATT of Core Specification.
    /// The attribute handle given was not valid on this server.
    BLE_ATT_ERR_INVALID_HANDLE = BLE_ERR(ATT, 0x01),
    /// The attribute cannot be read.
    BLE_ATT_ERR_READ_NOT_PERMITTED = BLE_ERR(ATT, 0x02),
    /// The attribute cannot be written.
    BLE_ATT_ERR_WRITE_NOT_PERMITTED = BLE_ERR(ATT, 0x03),
    /// The attribute PDU was invalid.
    BLE_ATT_ERR_INVALID_PDU = BLE_ERR(ATT, 0x04),
    /// The attribute requires authentication before it can be read or written.
    BLE_ATT_ERR_INSUFF_AUTHEN = BLE_ERR(ATT, 0x05),
    /// ATT Server does not support the request received from the client.
    BLE_ATT_ERR_REQUEST_NOT_SUPPORTED = BLE_ERR(ATT, 0x06),
    /// Offset specified was past the end of the attribute.
    BLE_ATT_ERR_INVALID_OFFSET = BLE_ERR(ATT, 0x07),
    /// The attribute requires authorization before it can be read or written.
    BLE_ATT_ERR_INSUFF_AUTHOR = BLE_ERR(ATT, 0x08),
    /// Too many prepare writes have been queued.
    BLE_ATT_ERR_PREPARE_QUEUE_FULL = BLE_ERR(ATT, 0x09),
    /// No attribute found within the given attribute handle range.
    BLE_ATT_ERR_ATTRIBUTE_NOT_FOUND = BLE_ERR(ATT, 0x0A),
    /// The attribute cannot be read using the ATT_READ_BLOB_REQ PDU.
    BLE_ATT_ERR_ATTRIBUTE_NOT_LONG = BLE_ERR(ATT, 0x0B),
    /// The Encryption Key Size used for encrypting this link is too short.
    BLE_ATT_ERR_INSUFF_ENC_KEY_SIZE = BLE_ERR(ATT, 0x0C),
    /// The attribute value length is invalid for the operation.
    BLE_ATT_ERR_INVALID_ATTRIBUTE_VAL_LEN = BLE_ERR(ATT, 0x0D),
    /// The attribute request that was requested has encountered an error that
    /// was unlikely, and therefore could not be completed as requested.
    BLE_ATT_ERR_UNLIKELY_ERR = BLE_ERR(ATT, 0x0E),
    /// The attribute requires encryption before it can be read or written.
    BLE_ATT_ERR_INSUFF_ENC = BLE_ERR(ATT, 0x0F),
    /// The attribute type is not a supported grouping attribute as defined by a
    /// higher layer specification.
    BLE_ATT_ERR_UNSUPP_GRP_TYPE = BLE_ERR(ATT, 0x10),
    /// Insufficient Resources to complete the request.
    BLE_ATT_ERR_INSUFF_RESOURCE = BLE_ERR(ATT, 0x11),
    /// The server requests the client to rediscover the database.
    BLE_ATT_ERR_DATABASE_OUT_OF_SYNC = BLE_ERR(ATT, 0x12),
    /// The attribute parameter value was not allowed.
    BLE_ATT_ERR_VALUE_NOT_ALLOWED = BLE_ERR(ATT, 0x13),

    /// Application error code defined by a higher layer specification.
    /// range 0x80 ~ 0x9F
    BLE_ATT_ERR_APP_ERROR = BLE_ERR(ATT, 0x80),
    /// Invalid parameter in request
    BLE_ATT_ERR_APP_INVALID_PARAM = BLE_ERR(ATT, 0x81),
    /// Inexistent handle for sending a read/write characteristic request
    BLE_ATT_ERR_APP_INEXISTENT_HDL = BLE_ERR(ATT, 0x82),
    /// Discovery stopped due to missing attribute according to specification
    BLE_ATT_ERR_APP_STOP_DISC_CHAR_MISSING = BLE_ERR(ATT, 0x83),
    /// Too many SVC instances found -> protocol violation
    BLE_ATT_ERR_APP_MULTIPLE_SVC = BLE_ERR(ATT, 0x84),
    /// Discovery stopped due to found attribute with incorrect properties
    BLE_ATT_ERR_APP_STOP_DISC_WRONG_CHAR_PROP = BLE_ERR(ATT, 0x85),
    /// Too many Char. instances found-> protocol violation
    BLE_ATT_ERR_APP_MULTIPLE_CHAR = BLE_ERR(ATT, 0x86),
    /// Attribute write not allowed
    BLE_ATT_ERR_APP_NOT_WRITABLE = BLE_ERR(ATT, 0x87),
    /// Attribute read not allowed
    BLE_ATT_ERR_APP_NOT_READABLE = BLE_ERR(ATT, 0x88),
    /// Request not allowed
    BLE_ATT_ERR_APP_REQ_DISALLOWED = BLE_ERR(ATT, 0x89),
    /// Notification Not Enabled
    BLE_ATT_ERR_APP_NTF_DISABLED = BLE_ERR(ATT, 0x8A),
    /// Indication Not Enabled
    BLE_ATT_ERR_APP_IND_DISABLED = BLE_ERR(ATT, 0x8B),
    /// Feature not supported by profile
    BLE_ATT_ERR_APP_FEATURE_NOT_SUPPORTED = BLE_ERR(ATT, 0x8C),
    /// Read value has an unexpected length
    BLE_ATT_ERR_APP_UNEXPECTED_LEN = BLE_ERR(ATT, 0x8D),
    /// Disconnection occurs
    BLE_ATT_ERR_APP_DISCONNECTED = BLE_ERR(ATT, 0x8E),
    /// Procedure Timeout
    BLE_ATT_ERR_APP_PROC_TIMEOUT = BLE_ERR(ATT, 0x8F),
    /// Permission set in service/attribute are invalid
    BLE_ATT_ERR_APP_INVALID_PERM = BLE_ERR(ATT, 0x90),

    /// Common profile and service error codes. range 0xE0 ~ 0xFF
    BLE_ATT_ERR_COMM_PRF_AND_SERVICE_ERR = BLE_ERR(ATT, 0xE0),
    /// Write request rejected
    BLE_ATT_ERR_PRF_WRITE_REQ_REJECTED = BLE_ERR(ATT, 0xFC),
    /// Client characteristic configuration improperly configured
    BLE_ATT_ERR_PRF_CCCD_IMPR_CONFIGURED = BLE_ERR(ATT, 0xFD),
    /// Procedure already in progress
    BLE_ATT_ERR_PRF_PROC_IN_PROGRESS = BLE_ERR(ATT, 0xFE),
    /// Out of Range
    BLE_ATT_ERR_PRF_OUT_OF_RANGE = BLE_ERR(ATT, 0xFF),

    /// SMP Specific Error
    /// Refer to Table 3.7: Pairing Failed Reason Codes in SMP of Core
    /// Specification.
    /// The user input of pass key failed, for example, the user canceled the
    /// operation.
    BLE_SMP_ERR_PASSKEY_ENTRY_FAILED = BLE_ERR(SMP, 0x01),
    /// The OOB Data is not available.
    BLE_SMP_ERR_OOB_NOT_AVAILABLE = BLE_ERR(SMP, 0x02),
    /// The pairing procedure cannot be performed as authentication requirements
    /// cannot be met due to IO capabilities of one or both devices.
    BLE_SMP_ERR_AUTH_REQ = BLE_ERR(SMP, 0x03),
    /// The confirm value does not match the calculated confirm value.
    BLE_SMP_ERR_CONF_VAL_FAILED = BLE_ERR(SMP, 0x04),
    /// Pairing is not supported by the device.
    BLE_SMP_ERR_PAIRING_NOT_SUPP = BLE_ERR(SMP, 0x05),
    /// The resultant encryption key size is insufficient for the security
    /// requirements of this device.
    BLE_SMP_ERR_ENC_KEY_SIZE = BLE_ERR(SMP, 0x06),
    /// The SMP command received is not supported on this device.
    BLE_SMP_ERR_CMD_NOT_SUPPORTED = BLE_ERR(SMP, 0x07),
    /// Pairing failed due to an unspecified reason.
    BLE_SMP_ERR_UNSPECIFIED_REASON = BLE_ERR(SMP, 0x08),
    /// Pairing or Authentication procedure is disallowed because too little
    /// time has elapsed since last pairing request or security request.
    BLE_SMP_ERR_REPEATED_ATTEMPTS = BLE_ERR(SMP, 0x09),
    /// The command length is invalid or a parameter is outside of the specified
    /// range.
    BLE_SMP_ERR_INVALID_PARAM = BLE_ERR(SMP, 0x0A),
    /// Indicates to the remote device that the DHKey Check value received
    /// doesn't match the one calculated by the local device.
    BLE_SMP_ERR_DHKEY_CHECK_FAILED = BLE_ERR(SMP, 0x0B),
    /// Indicates that the confirm values in the numeric comparison protocol do
    /// not match.
    BLE_SMP_ERR_NUMERIC_COMPARISON_FAILED = BLE_ERR(SMP, 0x0C),
    /// Indicates that the pairing over the LE transport failed due to a Pairing
    /// Request sent over the BR/EDR transport in process.
    BLE_SMP_ERR_BREDR_PAIRING_IN_PROGRESS = BLE_ERR(SMP, 0x0D),
    /// Indicates that the BR/EDR Link Key generated on the BR/EDR transport
    /// cannot be used to derive and distribute keys for the LE transport.
    BLE_SMP_ERR_CROSS_TRANSPORT_KEY_GENERATION_NOT_ALLOWED = BLE_ERR(SMP, 0x0E),
    /// Indicates that the device chose not to accept a distributed key.
    BLE_SMP_ERR_KEY_REJECTED = BLE_ERR(SMP, 0x0F),
    /// Local SMP Specific Error
    /// The signature verification failed.
    BLE_SMP_ERR_SIGN_VERIF_FAIL = BLE_ERR(SMP, 0x10),
    /// The encryption procedure failed because the slave device didn't find the
    /// LTK needed to start an encryption session.
    BLE_SMP_ERR_ENC_KEY_MISSING = BLE_ERR(SMP, 0x11),
    /// The encryption procedure failed because the slave device doesn't support
    /// the encryption feature.
    BLE_SMP_ERR_ENC_NOT_SUPPORTED = BLE_ERR(SMP, 0x12),
    /// A timeout has occurred during the start encryption session.
    BLE_SMP_ERR_ENC_TIMEOUT = BLE_ERR(SMP, 0x13),
    /// Undefined error.
    BLE_SMP_ERR_UNDEFINED = BLE_ERR(SMP, 0xFF),

    /// GAP Specific Error
    /// Invalid parameters set.
    BLE_GAP_ERR_INVALID_PARAM = BLE_ERR(GAP, 0x01),
    /// Problem with protocol exchange, get unexpected response.
    BLE_GAP_ERR_PROTOCOL_PROBLEM = BLE_ERR(GAP, 0x02),
    /// Request not supported by software configuration.
    BLE_GAP_ERR_NOT_SUPPORTED = BLE_ERR(GAP, 0x03),
    /// Request not allowed in current state.
    BLE_GAP_ERR_COMMAND_DISALLOWED = BLE_ERR(GAP, 0x04),
    /// Requested operation canceled.
    BLE_GAP_ERR_CANCELED = BLE_ERR(GAP, 0x05),
    /// Requested operation timeout.
    BLE_GAP_ERR_TIMEOUT = BLE_ERR(GAP, 0x06),
    /// Link connection lost during operation.
    BLE_GAP_ERR_DISCONNECTED = BLE_ERR(GAP, 0x07),
    /// Search algorithm finished, but no result found.
    BLE_GAP_ERR_NOT_FOUND = BLE_ERR(GAP, 0x08),
    /// Request rejected by peer device.
    BLE_GAP_ERR_REJECTED = BLE_ERR(GAP, 0x09),
    /// Problem with privacy configuration.
    BLE_GAP_ERR_PRIVACY_CFG_PB = BLE_ERR(GAP, 0x0A),
    /// Duplicate or invalid advertising data.
    BLE_GAP_ERR_ADV_DATA_INVALID = BLE_ERR(GAP, 0x0B),
    /// Insufficient resources.
    BLE_GAP_ERR_INSUFF_RESOURCES = BLE_ERR(GAP, 0x0C),
    /// Unexpected error.
    BLE_GAP_ERR_UNEXPECTED = BLE_ERR(GAP, 0x0D),
    /// Undefined error.
    BLE_GAP_ERR_UNDEFINED = BLE_ERR(GAP, 0xFF),
} ble_err_code_t;
