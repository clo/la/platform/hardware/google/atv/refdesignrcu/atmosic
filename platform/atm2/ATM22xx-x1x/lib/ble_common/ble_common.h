/**
 *******************************************************************************
 *
 * @file ble_common.h
 *
 * @brief Common BLE definitions and utility
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */

#pragma once

#include <string.h>

/// Address length
#define BLE_BDADDR_LEN 6

/// BD address type.
typedef enum {
    /// Public address type
    BLE_ADDR_PUBLIC,
    /// Random address type
    BLE_ADDR_RAND,
} ble_addr_type_t;

/// BD address structure
typedef struct {
    /// 6-byte array address value
    uint8_t addr[BLE_BDADDR_LEN];
} ble_bdaddr_t;

/// Address information about a device address
typedef struct {
    /// BD Address of device
    ble_bdaddr_t addr;
    /// Address type of the device
    ble_addr_type_t type;
} ble_gap_bdaddr_t;

/**
 *******************************************************************************
 * @brief Check equivalence of two ble_bdaddr_t type data
 * @param[in] addr1 first address to compare
 * @param[in] addr2 second address to compare
 *
 * @return True if they equal.
 *******************************************************************************
 */
__NONNULL_ALL
__INLINE bool ble_bdaddr_compare(ble_bdaddr_t const *addr1,
    ble_bdaddr_t const *addr2)
{
    return !memcmp(addr1, addr2, BLE_BDADDR_LEN);
}

/**
 *******************************************************************************
 * @brief Check equivalence of two ble_gap_bdaddr_t type data
 *
 * @param[in] addr1 first address to compare
 * @param[in] addr2 second address to compare
 *
 * @return True if they equal.
 *******************************************************************************
 */
__NONNULL_ALL
__INLINE bool ble_gap_addr_compare(ble_gap_bdaddr_t const *addr1,
    ble_gap_bdaddr_t const *addr2)
{
    return (addr1->type == addr2->type) &&
	ble_bdaddr_compare(&addr1->addr, &addr2->addr);
}
