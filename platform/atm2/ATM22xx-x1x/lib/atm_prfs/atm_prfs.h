/**
 *******************************************************************************
 *
 * @file atm_prfs.h
 *
 * @brief Atmosic Profile Server
 *
 * Copyright (C) Atmosic 2020
 *
 *
 *******************************************************************************
 */
#pragma once

/**
********************************************************************************
 * @addtogroup ATM_PRFS Atmosic Profile Server
 * @ingroup ATM_PRF
 * @brief Atmosic Profile Server
 * @{
********************************************************************************
 */

/*
 * INCLUDE FILE
 *******************************************************************************
 */
#include "prf.h"

#ifdef __cplusplus
extern "C" {
#endif

/// Max number of server task instances
#define ATM_PRFS_IDX_MAX (1)

///The atm_prfs environment variable
struct atm_prfs_env_tag {
    /// Profile environment
    prf_env_t prf_env;
    /// Extra context
    void const *ctx;
};

/**
 *******************************************************************************
 * @brief Retrieve Atmosic profile interface
 *
 * @return Atmosic profile interface
 *******************************************************************************
 */
const struct prf_task_cbs *atm_prfs_itf_get(void);

#ifdef __cplusplus
}
#endif

/// @} ATM_PRFS

