/**
 *******************************************************************************
 *
 * @file atm_prfs_task.h
 *
 * @brief Atmosic Profile Server Task
 *
 * Copyright (C) Atmosic 2020
 *
 *
 *******************************************************************************
 */
#pragma once

/**
********************************************************************************
 * @addtogroup ATM_PRFSTASK Atmosic Profile Server Task
 * @ingroup ATM_PRFS
 * @brief Atmosic Profile Server Task
 * @{
********************************************************************************
 */

/*
 * INCLUDE FILES
 *******************************************************************************
 */
#include "ke_task.h"
#include "gattc_task.h"
#include "atm_prfs.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * TYPE DEFINITIONS
 *******************************************************************************
 */
/// Messages for Atmosic profile server (atm_prfs)
enum atm_prfs_msg_id {
    /// Redirect the GATTC_ATT_INFO_REQ_IND message to upper layer
    ATM_PRFS_GATTC_ATT_INFO_REQ_IND =
	TASK_BUILD(TASK_TYPE_GET(GATTC_ATT_INFO_REQ_IND), TASK_ID_ATMPRFS),
    /// Redirect the GATTC_WRITE_REQ_IND message to upper layer
    ATM_PRFS_GATTC_WRITE_REQ_IND =
	TASK_BUILD(TASK_TYPE_GET(GATTC_WRITE_REQ_IND), TASK_ID_ATMPRFS),
    /// Redirect the GATTC_READ_REQ_IND message to upper layer
    ATM_PRFS_GATTC_READ_REQ_IND =
	TASK_BUILD(TASK_TYPE_GET(GATTC_READ_REQ_IND), TASK_ID_ATMPRFS),
    /// Redirect the GATTC_CMP_EVT message to upper layer
    ATM_PRFS_GATTC_CMP_EVT =
	TASK_BUILD(TASK_TYPE_GET(GATTC_CMP_EVT), TASK_ID_ATMPRFS),
};

/**
 *******************************************************************************
 * @brief Initialize atm_prfs task handlers
 *
 * @param[out] task_desc Task descriptor to fill
 *******************************************************************************
 */
void atm_prfs_task_init(struct ke_task_desc *task_desc);

/**
 *******************************************************************************
 * @brief Retrieve profile task after profile creating
 *
 * @return registed profile task
 *******************************************************************************
 */
uint16_t atm_prfs_get_profile_task(void);

#ifdef __cplusplus
}
#endif

/// @} ATM_PRFSTASK

