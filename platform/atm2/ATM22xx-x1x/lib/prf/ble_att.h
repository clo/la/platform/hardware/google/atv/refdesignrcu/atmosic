/**
 *******************************************************************************
 *
 * @file ble_att.h
 *
 * @brief BLE ATT
 *
 * Copyright (C) Atmosic 2020
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_ATT BLE ATT define
 * @ingroup ATM_BTFM_API
 * @brief ATM bluetooth framework ATT defines
 *
 * @{
 *******************************************************************************
 */

/*
 * INCLUDE FILE
 ******************************************************************************
 */
#include "rwip_config.h"
#include "att.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * DEFINE
 ******************************************************************************
 */

/// No security requirements
#define BLE_SEC_PROP_NO_SECURITY (PERM(SVC_AUTH, NO_AUTH))
/// Unauthenticated no MITM protection
#define BLE_SEC_PROP_UNAUTH (PERM(SVC_AUTH, UNAUTH))
/// Authenticated MITM protection
#define BLE_SEC_PROP_AUTH (PERM(SVC_AUTH, AUTH))
/// LE Secure Connections pairing
#define BLE_SEC_PROP_LE_SC (PERM(SVC_AUTH, SEC_CON))

#define BLE_ATT_RD PERM(RD, ENABLE)
/// No security requirements - Read
#define BLE_ATT_READ_NO_SECURITY (BLE_ATT_RD | PERM(RP, NO_AUTH))
/// Unauthenticated no MITM protection - Read
#define BLE_ATT_READ_UNAUTH (BLE_ATT_RD | PERM(RP, UNAUTH))
/// Authenticated MITM protection - Read
#define BLE_ATT_READ_AUTH (BLE_ATT_RD | PERM(RP, AUTH))
/// LE Secure Connections pairing - Read
#define BLE_ATT_READ_LE_SC (BLE_ATT_RD | PERM(RP, SEC_CON))

#define BLE_ATT_WC PERM(WRITE_COMMAND, ENABLE)
/// No security requirements - Write command
#define BLE_ATT_WRITE_COMMAND_NO_SECURITY (BLE_ATT_WC | PERM(WP, NO_AUTH))
/// Unauthenticated no MITM protection - Write command
#define BLE_ATT_WRITE_COMMAND_UNAUTH (BLE_ATT_WC | PERM(WP, UNAUTH))
/// Authenticated MITM protection - Write command
#define BLE_ATT_WRITE_COMMAND_AUTH (BLE_ATT_WC | PERM(WP, AUTH))
/// LE Secure Connections pairing - Write command
#define BLE_ATT_WRITE_COMMAND_LE_SC (BLE_ATT_WC | PERM(WP, SEC_CON))

#define BLE_ATT_WR PERM(WRITE_REQ, ENABLE)
/// No security requirements - Write request
#define BLE_ATT_WRITE_REQ_NO_SECURITY (BLE_ATT_WR | PERM(WP, NO_AUTH))
/// Unauthenticated no MITM protection - Write request
#define BLE_ATT_WRITE_REQ_UNAUTH (BLE_ATT_WR | PERM(WP, UNAUTH))
/// Authenticated MITM protection - Write request
#define BLE_ATT_WRITE_REQ_AUTH (BLE_ATT_WR | PERM(WP, AUTH))
/// LE Secure Connections pairing - Write request
#define BLE_ATT_WRITE_REQ_LE_SC (BLE_ATT_WR | PERM(WP, SEC_CON))

#define BLE_ATT_WS PERM(WRITE_SIGNED, ENABLE)
/// No security requirements - Write signed
#define BLE_ATT_WRITE_SIGNED_NO_SECURITY (BLE_ATT_WS | PERM(WP, NO_AUTH))
/// Unauthenticated no MITM protection - Write signed
#define BLE_ATT_WRITE_SIGNED_UNAUTH (BLE_ATT_WS | PERM(WP, UNAUTH))
/// Authenticated MITM protection - Write signed
#define BLE_ATT_WRITE_SIGNED_AUTH (BLE_ATT_WS | PERM(WP, AUTH))
/// LE Secure Connections pairing - Write signed
#define BLE_ATT_WRITE_SIGNED_LE_SC (BLE_ATT_WS | PERM(WP, SEC_CON))

#define BLE_ATT_NTF PERM(NTF, ENABLE)
/// No security requirements - Notification
#define BLE_ATT_NTF_NO_SECURITY (BLE_ATT_NTF | PERM(NP, NO_AUTH))
/// Unauthenticated no MITM protection - Notification
#define BLE_ATT_NTF_UNAUTH (BLE_ATT_NTF | PERM(NP, UNAUTH))
/// Authenticated MITM protection - Notification
#define BLE_ATT_NTF_AUTH (BLE_ATT_NTF | PERM(NP, AUTH))
/// LE Secure Connections pairing - Notification
#define BLE_ATT_NTF_LE_SC (BLE_ATT_NTF | PERM(NP, SEC_CON))

#define BLE_ATT_IND PERM(IND, ENABLE)
/// No security requirements - Indication
#define BLE_ATT_IND_NO_SECURITY (BLE_ATT_IND | PERM(IP, NO_AUTH))
/// Unauthenticated no MITM protection - Indication
#define BLE_ATT_IND_UNAUTH (BLE_ATT_IND | PERM(IP, UNAUTH))
/// Authenticated MITM protection - Indication
#define BLE_ATT_IND_AUTH (BLE_ATT_IND | PERM(IP, AUTH))
/// LE Secure Connections pairing - Indication
#define BLE_ATT_IND_LE_SC (BLE_ATT_IND | PERM(IP, SEC_CON))

/// Client Characteristic Configuration Codes
typedef enum {
    /// Stop notification
    BLE_ATT_CCCD_STOP_NTF = ATT_CCC_STOP_NTFIND,
    /// Stop indication
    BLE_ATT_CCCD_STOP_IND = ATT_CCC_STOP_NTFIND,
    /// Start notification
    BLE_ATT_CCCD_NTF = ATT_CCC_START_NTF,
    /// Start indication
    BLE_ATT_CCCD_IND = ATT_CCC_START_IND
} ble_att_cccd_val_t;

#ifdef __cplusplus
}
#endif

///@} ATM_BTFM_ATT

