#!/bin/bash

echo ""
echo "Flashing program from directory $1"
echo ""

[ -d $1 ] && cd $1 && make program

