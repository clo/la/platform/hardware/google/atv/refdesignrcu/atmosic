
ifndef __FRAMEWORK_MK__
__FRAMEWORK_MK__ = 1

CFLAGS += -DCFG_FRAMEWORK
LIBRARIES += framework_lib porting_ble ble_common

# Add common framework modules
ifdef FRAMEWORK_MODULES
LIBRARIES += ble_module ble_task $(FRAMEWORK_MODULES)
endif

ifneq (,$(filter at_cmd,$(LIBRARIES)))
include $(COMMON_USER_DIR)/atcmds.mk
endif

include $(COMMON_USER_DIR)/app.mk

endif # __FRAMEWORK_MK__
