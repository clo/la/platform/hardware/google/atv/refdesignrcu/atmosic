
ifndef __ROM_APP_MK__
__ROM_APP_MK__ = 1

include $(ROM_DIR)/rom.mk

# set library defaults
LIBS ?= \
	$(LIB_DIR)/lib_plf$(if $(DEBUG),_dbg).a \

ifdef RUN_IN_RAM
DRIVERS += pseq
INCLUDES += $(DRIVER_DIR)/wurx
CFLAGS += '-Dmain(...)=user_main(__VA_ARGS__)'
endif

ROM_ERRATA_SRC := $(filter-out $(ROM_ERRATA_EXCLUDE),\
    $(notdir $(wildcard $(ROM_DIR)/ROM_errata_*.c)))

comma := ,

ifeq ($(TOOLSET),IAR)
APP_USER_STARTUP ?= $(ROM_DIR)/iccarm/user_startup_CMSDK_CM0-iar.s

ifdef USE_LIB
LIBS := \
	--whole-archive $(LIB_DIR)/app_lib/$(APP)$(if $(DEBUG),_dbg).a \
	$(LIBS)
else
rom.o: rom.elf
	$(IAR_DIR)/isymexport $< $@
endif

LDFLAGS := \
	--config_search $(ROM_DIR)/iccarm \
	--config cmsdk_cm0_user_flash.icf \
	--entry _user_entry \
	$(LDFLAGS)

REDEF += $(ROM_DIR)/iccarm/iar.redef
else ifeq ($(TOOLSET),ARM)
APP_USER_STARTUP ?= $(ROM_DIR)/armcc/Startup.s

ifdef USE_LIB
LIBS := \
	'$(LIB_DIR)/app_lib/$(APP)$(if $(DEBUG),_dbg).a(*)' \
	$(LIBS)
else
rom.o: rom.elf
	$(COMMON_USER_DIR)/gen_symtab.py $< $@
endif

LDFLAGS := \
	--scatter=$(ROM_DIR)/armcc/cmsdk_cm0_user_flash.sct \
	$(LDFLAGS)

REDEF += $(ROM_DIR)/keil.redef
else # TOOLSET
APP_USER_STARTUP ?= $(ROM_DIR)/user_startup_CMSDK_CM0.s

ifdef USE_LIB
comma := ,
LIBS := \
	-Wl$(comma)--whole-archive \
	$(LIB_DIR)/app_lib/$(APP)$(if $(DEBUG),_dbg).a \
	-Wl$(comma)--no-whole-archive \
	$(LIBS)
else
rom.o: rom.elf
	$(COMMON_USER_DIR)/gen_symtab.py $< $@
endif

LDFLAGS := \
	-L$(ROM_DIR) \
	$(if $(RUN_IN_RAM),-Tcmsdk_cm0_user_ram.ld,\
	    $(if $(AVOID_XIP),-Tcmsdk_cm0_user_flash_noxip.ld,\
	    -Tcmsdk_cm0_user_flash.ld)) \
	$(if $(URAM_START),-Wl$(comma)--defsym=URAM_START=$(URAM_START)) \
	$(if $(URAM_SIZE),-Wl$(comma)--defsym=URAM_SIZE=$(URAM_SIZE)) \
	$(LDFLAGS)
endif

ifndef USE_LIB
OBJS += rom.o
endif

REDEF += \
	$(ROM_DIR)/startup.redef \
	$(ROM_DIR)/user.redef \
	$(if $(COVERAGE),$(ROM_DIR)/gcov.redef) \

rom.elf: $(ROM_DIR)/fw.elf
	$(OBJCOPY) $(addprefix --redefine-syms=,$(REDEF)) \
	    --globalize-symbols=$(ROM_DIR)/rom.glob \
	    --add-symbol run_in_ram_hook=0x601d $< $@

GDB_EXTRA += -ex "add-symbol-file rom.elf"

.PHONY: clean
clean::
	rm -f rom.elf

endif # __ROM_APP_MK__
