
ifndef __ROM_MK__
__ROM_MK__ = 1

ROM_CFLAGS := \
	-DCFG_EMB \
	-DCFG_HOST \
	-DCFG_BLE \
	-DCFG_AHITL \
	-DCFG_NVDS \
	-DCFG_DBG_MEM \
	-DCFG_DBG_FLASH \
	-DCFG_DBG_NVDS \
	-DCFG_DBG_STACK_PROF \
	-DCFG_RF_SYDNEY \
	-DCFG_ALLROLES \
	-DCFG_ACT=6 \
	-DCFG_RAL=3 \
	-DCFG_CON=5 \
	-DCFG_ISO_CON=0 \
	-DCFG_SEC_CON \
	-DCFG_EXT_DB \
	-DCFG_ATTC \
	-DCFG_ATTS \
	-DCFG_PRF \
	-DCFG_NB_PRF=12 \
	-DCFG_HCITL \
	-DCFG_AHITL \
	-DCFG_BLE_DTM \

.PHONY: gdb_rom
gdb_rom: $(ROM_DIR)/fw.elf $(GDB)
	$(GDB) -x $(GDB_CFG_DIR)/atmx2.gdb $<

.PHONY: gdb_rom_target
gdb_rom_target: $(ROM_DIR)/fw.elf $(GDB)
	$(GDB) -x $(GDB_CFG_DIR)/atmx2.gdb $< -ex "target remote localhost:3333"

endif # __ROM_MK__
